FROM juliendehos/cicd:mywebpage
RUN nix-env -iA nixpkgs.python3Packages.sphinx
RUN nix-env -iA nixpkgs.python3Packages.sphinx_rtd_theme
RUN nix-env -iA nixpkgs.gnumake

# docker build -t juliendehos/cicd:haskell-cheats .
# docker push juliendehos/cicd:haskell-cheats
 
