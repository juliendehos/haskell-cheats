#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.transformers])"

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe

main :: IO ()
main = do
    res <- runMaybeT converse
    case res of
        Nothing  -> putStrLn "bad conversation"
        Just str -> putStrLn str

converse :: MaybeT IO String
converse = do
    lift $ putStrLn "\nsay something: "
    line1 <- lift getLine
    guard (line1 == "hi!")
    lift $ putStrLn "hello!"
    line2 <- lift getLine
    guard (line2 == "fine?")
    lift $ putStrLn "yeap!"
    return "nice conversation"

