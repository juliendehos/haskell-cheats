
ExceptT TODO
===============================================================================

:Date:
    2019-01-22

:Examples:
   `todo/todo <https://gitlab.com/juliendehos/useful-haskell/source/tools/cabal>`__

.. code-block:: haskell

    import Control.Monad.Except

    main :: IO ()
    main = do

        e1 <- converse1
        case e1 of
            Left  n   -> putStrLn $ "Error - Code: " ++ show n
            Right str -> putStrLn $ "Success - String: " ++ str

        e2 <- runExceptT converse2
        case e2 of
            Left  n   -> putStrLn $ "Error - Code: " ++ show n
            Right str -> putStrLn $ "Success - String: " ++ str


.. code-block:: console

    nix-shell -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [mtl])" --run "runghc converse.hs"


.. code-block:: shell

    #! /usr/bin/env nix-shell
    #! nix-shell -i runghc -p ghc haskellPackages.mtl
    
    # ...


.. literalinclude:: exceptt3.hs
    :language: haskell
    :linenos:
    :lines: 6-
    :emphasize-lines: 2


