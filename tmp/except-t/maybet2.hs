#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkg.stransformers])"

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe

main :: IO ()
main = do
    res <- runMaybeT converse
    case res of
        Nothing  -> putStrLn "bad conversation"
        Just str -> putStrLn str

converse :: MaybeT IO String
converse = do
    lift $ putStrLn "\nsay something: "
    line1 <- lift getLine
    case line1 of 
        "hi!" -> lift $ putStrLn "hello!"
        _ -> mzero
    lin2 <- lift getLine
    case lin2 of
        "fine?" -> lift $ putStrLn "yeap!"
        _ -> mzero
    return "nice conversation"

