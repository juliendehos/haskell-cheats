#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p ghc

main :: IO ()
main = do
    res <- converse
    case res of
        Nothing  -> putStrLn "bad conversation"
        Just str -> putStrLn str

converse :: IO (Maybe String)
converse = do
    putStrLn "\nsay something: "
    line1 <- getLine
    if line1 /= "hi!"
    then return Nothing
    else do
        putStrLn "hello!"
        line2 <- getLine
        if line2 /= "fine?"
        then return Nothing
        else do
            putStrLn "yeap!"
            return $ Just "nice conversation"

