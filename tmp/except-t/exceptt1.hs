#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p ghc

main :: IO ()
main = do
    res <- converse
    case res of
        Left  n   -> putStrLn $ "Error - Code: " ++ show n
        Right str -> putStrLn $ "Success - String: " ++ str

converse :: IO (Either Int String)
converse = do
    putStrLn "\nsay something: "
    line1 <- getLine
    if line1 /= "hi!"
    then return $ Left 1
    else do
        putStrLn "hello!" 
        line2 <- getLine
        if line2 /= "fine?"
        then return $ Left 2
        else do
            putStrLn "yeap!"
            return $ Right "nice conversation"

