#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.transformers])"

import Control.Monad.Except

main :: IO ()
main = do
    res <- runExceptT converse
    case res of
        Left  n   -> putStrLn $ "Error - Code: " ++ show n
        Right str -> putStrLn $ "Success - String: " ++ str

converse :: ExceptT Int IO String
converse = do
    lift $ putStrLn "\nsay something: "
    line1 <- lift getLine
    if line1 /= "hi!" 
    then throwError 1
    else do
        lift $ putStrLn "hello!"
        line2 <- lift getLine
        if line2 /= "fine?"
        then throwError 2
        else do
            lift $ putStrLn "yeap!"
            return "nice conversation"


