{-# language DataKinds #-}
{-# language OverloadedStrings #-}

import OpenCV
import OpenCV.VideoIO.Types
import qualified OpenCV.VideoIO.VideoWriter as WR
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "Specify an input video file, please"
    else do
        let infile = head args
        cap <- newVideoCapture
        exceptErrorIO $ videoCaptureOpen cap $ VideoFileSource infile Nothing
        isOpened <- videoCaptureIsOpened cap
        case isOpened of
            False -> putStrLn $ "Couldn't open " ++ infile
            True -> do
                w <- videoCaptureGetI cap VideoCapPropFrameWidth
                h <- videoCaptureGetI cap VideoCapPropFrameHeight
                fps <- videoCaptureGetD cap VideoCapPropFps
                wr <- WR.videoWriterOpen $ WR.VideoFileSink'
                         (WR.VideoFileSink "out.mp4" "mp4v" fps (w, h))
                isOpened' <- WR.videoWriterIsOpened wr
                case isOpened' of
                    False -> putStrLn $ "Couldn't open out.mp4"
                    True -> do
                        loop cap wr
                        exceptErrorIO $ WR.videoWriterRelease wr
                        return ()

loop :: VideoCapture -> WR.VideoWriter -> IO ()
loop cap wr = do
    _ <- videoCaptureGrab cap
    frame <- videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            exceptErrorIO $ WR.videoWriterWrite wr $ process img
            loop cap wr

process :: Mat ('S ['D, 'D]) 'D 'D
        -> Mat ('S ['D, 'D]) 'D 'D
process img = exceptError $ do
    img1 <- cvtColor bgr gray img
    img2 <- cvtColor gray bgr img1
    coerceMat img2

