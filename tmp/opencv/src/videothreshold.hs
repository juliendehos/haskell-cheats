{-# language DataKinds #-}
{-# language TypeFamilies #-}
{-# language FlexibleContexts #-}

import Data.Char (ord)
import Data.Int
import Linear.V2
import OpenCV
import OpenCV.TypeLevel
import OpenCV.VideoIO.Types
import qualified OpenCV.VideoIO.VideoWriter as WR
import System.Environment (getArgs)

stringToFourCC :: String -> FourCC
stringToFourCC = FourCC . foldr (\x acc -> acc * 256 + fromIntegral (ord x)) 0 

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then putStrLn "Specify an input video file, please"
    else do
        let infile = head args
        cap <- newVideoCapture
        exceptErrorIO $ videoCaptureOpen cap $ VideoFileSource infile Nothing
        isOpened <- videoCaptureIsOpened cap
        case isOpened of
            False -> putStrLn $ "Couldn't open " ++ infile
            True -> do
                w <- videoCaptureGetI cap VideoCapPropFrameWidth
                h <- videoCaptureGetI cap VideoCapPropFrameHeight
                f <- videoCaptureGetD cap VideoCapPropFps
                wr <- WR.videoWriterOpen $ WR.VideoFileSink'
                         (WR.VideoFileSink "out.mp4" (stringToFourCC "mp4v") f (w, h))
                isOpened' <- WR.videoWriterIsOpened wr
                case isOpened' of
                    False -> putStrLn $ "Couldn't open out.mp4"
                    True -> do
                        loop cap wr
                        exceptErrorIO $ WR.videoWriterRelease wr
                        return ()

loop :: VideoCapture -> WR.VideoWriter -> IO ()
loop cap wr = do
    _ <- videoCaptureGrab cap
    frame <- videoCaptureRetrieve cap
    case frame of
        Nothing -> return ()
        Just img -> do
            exceptErrorIO $ WR.videoWriterWrite wr $ process img
            loop cap wr

-- process :: Mat ('S ['D, 'D]) ('S 3) 'D -> Mat ('S ['D, 'D]) ('S 3) 'D
process img = exceptError $ do
    -- img1 <- cvtColor bgr gray img
    -- img2 <- laplacian Nothing Nothing Nothing Nothing img1
    -- img3 <- cvtColor gray bgr img2
    img2 <- blur (V2 9 9) img 
    coerceMat img2
    -- coerceMat img2

{-
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (9, 9), 1.5)
    edges = cv.Canny(blur, 0, 40, 3)
-}


