import Control.Monad ( void )
import qualified OpenCV as CV
import qualified Data.ByteString as B
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    if null args
    then putStrLn "specify an image file, please"
    else do
        let filename = head args
        img <- CV.imdecode CV.ImreadUnchanged <$> B.readFile filename
        CV.withWindow "test" $ \window -> do
            CV.imshow window img
            void $ CV.waitKey 0

