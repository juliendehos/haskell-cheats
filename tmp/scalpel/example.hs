{-# LANGUAGE OverloadedStrings #-}

import Network.HTTP.Client
import Text.HTML.Scalpel
import qualified Data.ByteString.Lazy.Char8 as L

main :: IO ()
main = do
    man <- newManager defaultManagerSettings

    let login = "frigolem"
    let password = "bitusopo"
    -- let login = "ragolem"
    -- let password = "hykapyhu"

    let req1 = urlEncodedBody [ ("login", login) , ("password", password) ] 
            $ "http://littlegolem.net/jsp/login/index.jsp"
                { method = "POST", cookieJar = Just (createCookieJar []) }
    cookieJar1 <- responseCookieJar <$> httpLbs req1 man
    let baseReq = "http://littlegolem.net" { method = "GET", cookieJar = Just cookieJar1 }
 
    data2 <- responseBody <$> httpLbs (baseReq { path = "/jsp/game/index.jsp" }) man
    let Just tds = scrapeStringLike data2 
            (chroot ("div" @: [hasClass "blue-madison"]) $ chroot "tbody" $ texts "td")
    let gid = L.filter (`notElem` ['\n', '\t', '\r', '#']) $ head tds
    let gname = tds !! 4

    let path3 = L.toStrict $ L.concat ["/servlet/sgf/", gid, "/game", gid, ".hsgf"]
    data3 <- responseBody <$> httpLbs (baseReq { path = path3 }) man

    -- TODO multiple games
    -- TODO play move

    L.putStrLn $ L.concat ["playing game #", gid, " (", gname, ")..."]
    print data3

-- http://littlegolem.net/jsp/game/index.jsp
-- http://littlegolem.net/jsp/game/game.jsp?gid=2114649
-- littlegolem.net/servlet/sgf/2114649/game2114649.hsgf
-- http://littlegolem.net/jsp/game/game.jsp?gid=2114649&move=ee
--



