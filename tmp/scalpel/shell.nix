
let

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: rec {
          req = self.callHackage "req" "2.0.1" {};
          retry = self.callHackage "retry" "0.8.0.0" {};
          scalpel = self.callHackage "scalpel" "0.6.0" {};
          scalpel-core = self.callHackage "scalpel-core" "0.6.0" {};
        };
      };
    };
  };
  
  pkgs = import <nixpkgs> { inherit config; };

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    bytestring
    http-client
    scalpel
    scalpel-core
    text
  ]);

in pkgs.stdenv.mkDerivation {
    name = "my-haskell-env-0";
    buildInputs = [ ghc ];
    shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
  }

