{-# LANGUAGE OverloadedStrings #-}

import Text.HTML.Scalpel
import Control.Applicative

exampleHtml :: String
exampleHtml =
    "<html>\
    \  <body>\
    \    <div class='comment'>\
    \      <span class='comment author'>Sally</span>\
    \      <div class='comment text'>Woo hoo!</div>\
    \    </div>\
    \    <div class='comment'>\
    \      <span class='comment author'>Bill</span>\
    \      <img class='comment image' src='http://example.com/cat.gif' />\
    \    </div>\
    \  </body>\
    \</html>"

type Author = String

data Comment
    = TextComment Author String
    | ImageComment Author URL
    deriving (Show)

main :: IO ()
main = case scrapeStringLike exampleHtml commentsS of
    Nothing -> putStrLn "error"
    Just res -> mapM_ print res

commentsS :: Scraper String [Comment]
commentsS = chroots ("div" @: [hasClass "comment"]) commentS

commentS :: Scraper String Comment
commentS = textCommentS <|> imageCommentS

textCommentS :: Scraper String Comment
textCommentS = do
    author      <- authorS
    commentText <- text $ "div"  @: [hasClass "text"]
    return $ TextComment author commentText

imageCommentS :: Scraper String Comment
imageCommentS = do
    author   <- authorS
    imageURL <- attr "src" $ "img"  @: [hasClass "image"]
    return $ ImageComment author imageURL

authorS :: Scraper String Author
authorS = text $ "span" @: [hasClass "author"]

-- see also:
-- https://github.com/fimad/scalpel/blob/master/examples/example-from-documentation/Main.hs

