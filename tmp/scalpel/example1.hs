{-# LANGUAGE OverloadedStrings #-}

import Text.HTML.Scalpel

main :: IO ()
main = do
    let url = "https://www.haskell.org/"
    maybeImages <- scrapeURL url (attrs "src" "img")
    case maybeImages of
        Nothing -> putStrLn "ERROR: Could not scrape the URL!"
        Just images -> mapM_ putStrLn images

-- see also:
-- https://github.com/fimad/scalpel/blob/master/examples/list-all-images/Main.hs

