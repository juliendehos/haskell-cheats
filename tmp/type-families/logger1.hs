#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ ])"

-- https://mmhaskell.com/blog/2019/2/4/why-haskell-v-type-families

class Logger logger where
  prevMessages :: logger -> [String]
  logString :: String -> logger -> logger

newtype ListWrapper = ListWrapper { _msgs :: [String] }

instance Logger ListWrapper where
    prevMessages (ListWrapper msgs) = msgs
    logString s l = ListWrapper (_msgs l ++ [s])

mylogs :: ListWrapper
mylogs = logString "deux" $ logString "un" $ ListWrapper []

main :: IO ()
main = print $ prevMessages mylogs

