#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ mtl ])"

-- https://mmhaskell.com/blog/2019/2/4/why-haskell-v-type-families

import Control.Monad.State

class Logger logger where
  prevMessages :: logger -> [String]
  logString :: String -> StateT logger IO ()

newtype ListWrapper = ListWrapper { _msgs :: [String] }

instance Logger ListWrapper where
    prevMessages (ListWrapper msgs) = msgs
    logString s = do
        l <- get
        lift $ putStrLn s
        put $ l { _msgs = _msgs l ++ [s] }

mylogs1 :: IO ListWrapper
mylogs1 = execStateT (mapM logString ["un", "deux"]) (ListWrapper [])

mylogs2 :: IO ListWrapper
mylogs2 = flip execStateT (ListWrapper []) $ do
    logString "one"
    logString "two"

main :: IO ()
main = do
    l1 <- mylogs1
    print $ prevMessages l1
    l2 <- mylogs2 
    print $ prevMessages l2

