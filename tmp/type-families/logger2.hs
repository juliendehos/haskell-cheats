#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ mtl ])"

-- https://mmhaskell.com/blog/2019/2/4/why-haskell-v-type-families

import Control.Monad.State

class Logger logger where
  prevMessages :: logger -> [String]
  logString :: String -> State logger ()

newtype ListWrapper = ListWrapper { _msgs :: [String] }

instance Logger ListWrapper where
    prevMessages (ListWrapper msgs) = msgs
    logString s = do
        l <- get
        put $ l { _msgs = _msgs l ++ [s] }

mylogs1 :: ListWrapper
mylogs1 = execState (mapM logString ["un", "deux"]) (ListWrapper [])

mylogs2 :: ListWrapper
mylogs2 = flip execState (ListWrapper []) $ do
    logString "one"
    logString "two"

main :: IO ()
main = do
    print $ prevMessages mylogs1
    print $ prevMessages mylogs2

