#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ mtl ])"

-- https://wiki.haskell.org/GHC/Type_families

{-# LANGUAGE TypeFamilies #-}

import qualified Data.IntMap

class GMapKey k where
    data GMap k :: * -> *
    myempty       :: GMap k v
    mylookup      :: k -> GMap k v -> Maybe v
    myinsert      :: k -> v -> GMap k v -> GMap k v

instance GMapKey Int where
    data GMap Int v          = GMapInt (Data.IntMap.IntMap v)
    myempty                  = GMapInt Data.IntMap.empty
    mylookup k   (GMapInt m) = Data.IntMap.lookup k m
    myinsert k v (GMapInt m) = GMapInt (Data.IntMap.insert k v m)

instance GMapKey () where
    data GMap () v             = GMapUnit (Maybe v)
    myempty                    = GMapUnit Nothing
    mylookup () (GMapUnit v)   = v
    myinsert () v (GMapUnit _) = GMapUnit $ Just v

instance (GMapKey a, GMapKey b) => GMapKey (a, b) where
    data GMap (a, b) v              = GMapPair (GMap a (GMap b v))
    myempty                         = GMapPair myempty
    mylookup (a, b) (GMapPair gm)   = mylookup a gm >>= mylookup b 
    myinsert (a, b) v (GMapPair gm) = GMapPair $ case mylookup a gm of
        Nothing  -> myinsert a (myinsert b v myempty) gm
        Just gm2 -> myinsert a (myinsert b v gm2  ) gm

instance (GMapKey a, GMapKey b) => GMapKey (Either a b) where
    data GMap (Either a b) v                  = GMapEither (GMap a v) (GMap b v)
    myempty                                   = GMapEither myempty myempty
    mylookup (Left  a) (GMapEither gm1  _gm2) = mylookup a gm1
    mylookup (Right b) (GMapEither _gm1 gm2 ) = mylookup b gm2
    myinsert (Left  a) v (GMapEither gm1 gm2) = GMapEither (myinsert a v gm1) gm2
    myinsert (Right b) v (GMapEither gm1 gm2) = GMapEither gm1 (myinsert b v gm2)

main :: IO ()
main = do
    print $ map (`mylookup` m1) [1,2,3]
    print $ map (`mylookup` m2) [(),(),()]
    print $ map (`mylookup` m3) [(1,2),(0,1),(0,2)]
    print $ map (`mylookup` m4) [Right 1, Left 2, Right 0]
    where m1 = myinsert (2::Int) "deux" $ myinsert 1 "un" myempty
          m2 = myinsert () "deux" $ myinsert () "un" myempty
          m3 = myinsert (1::Int,2::Int) "un-deux" $ myinsert (0,1) "zero-un" myempty
          m4 = myinsert (Right (1::Int)) "r-un" $ myinsert (Left (2::Int)) "l-deux" myempty

