#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ mtl ])"

-- https://mmhaskell.com/blog/2019/2/4/why-haskell-v-type-families

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

import Control.Monad.State

class Logger logger where
    type LoggerM logger :: * -> *
    prevMessages :: logger -> [String]
    logString :: String -> (LoggerM logger) ()

{-
newtype ListWrapper = ListWrapper [String]
instance Logger ListWrapper where
    type (LoggerM ListWrapper) = State ListWrapper
    prevMessages (ListWrapper msgs) = reverse msgs
    logString s = do
        (ListWrapper msgs) <- get
        put $ ListWrapper (s : msgs)
-}

newtype ConsoleLogger = ConsoleLogger [String]
instance Logger ConsoleLogger where
    type (LoggerM ConsoleLogger) = StateT ConsoleLogger IO
    prevMessages (ConsoleLogger msgs) = reverse msgs
    logString s = do
        (ConsoleLogger msgs) <- get
        lift $ putStrLn s
        put $ ConsoleLogger (s : msgs)

-- runComputations :: (Logger logger, Monad (LoggerM logger)) => p -> (LoggerM logger) String
-- runComputations :: (Monad (LoggerM logger1), Logger logger2, Logger logger1, LoggerM logger2 ~ LoggerM logger1) => p -> LoggerM logger1 [Char]
-- runComputations :: (Monad (LoggerM logger1), Logger logger2, Logger logger1, LoggerM logger2 ~ LoggerM logger1) => [Char] -> LoggerM logger1 [Char]
runComputations input = do
    logString "Computation!"
    return $ "input: " ++ input

main :: IO ()
main = do
    -- _ <- runComputations "toto"
    putStrLn "finished"

