with import <nixpkgs> {};
mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      containers
      mtl
      transformers
      lifted-async
      stm
    ]))
  ];
}

