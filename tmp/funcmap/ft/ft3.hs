{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad.Reader
import Data.IORef

-- https://serokell.io/blog/tagless-final
-- https://www.parsonsmatt.org/2018/03/22/three_layer_haskell_cake.html

----------------------------------------------------------------------
-- layer 1: ReaderT Design Pattern
----------------------------------------------------------------------

newtype AppT m a = AppT
    { unAppT :: ReaderT DatabaseConfig m a
    } deriving (Functor, Applicative, Monad, MonadIO, MonadReader DatabaseConfig)

runAppT :: MonadIO m => AppT m a -> DatabaseConfig -> m a
runAppT app = runReaderT (unAppT app)

newtype DatabaseConfig = DatabaseConfig
    { usersRef :: IORef [User]
    }

----------------------------------------------------------------------
-- layer 2: bridge layer
----------------------------------------------------------------------

class Monad m => MonadDatabase m where
    getUser :: Name -> m User

instance MonadIO m => MonadDatabase (AppT m) where
    getUser n = do 
        ref <- asks usersRef
        users <- liftIO $ readIORef ref
        case filter ((== n) . name) users of
            [u] -> return u
            _ -> fail "unknown user"

----------------------------------------------------------------------
-- layer 3: business logic
----------------------------------------------------------------------

newtype Name = Name String
    deriving (Eq, Show)

data User = User
    { name :: Name
    , age :: Int
    } deriving (Eq, Show)

----------------------------------------------------------------------
-- main program
----------------------------------------------------------------------

test :: MonadDatabase m => m User
test = getUser (Name "foobar")

main :: IO ()
main = do
    ref <- newIORef
                [ User (Name "toto") 42
                , User (Name "foobar") 13
                ]
    let cfg = DatabaseConfig ref
    runAppT test cfg >>= print


