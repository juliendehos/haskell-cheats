{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad.Reader
import Data.IORef

newtype Name = Name String
    deriving (Eq, Show)

data User = User
    { name :: Name
    , age :: Int
    } deriving (Eq, Show)

class Monad m => MonadDatabase m where
    getUser    :: Name -> m User

newtype DatabaseConfig = DatabaseConfig
    { usersRef :: IORef [User]
    }

newtype AppM a = AppM
    { unAppM :: ReaderT DatabaseConfig IO a
    } deriving (Functor, Applicative, Monad, MonadIO, MonadReader DatabaseConfig)

instance MonadDatabase AppM where
    getUser n = do 
        ref <- asks usersRef
        users <- liftIO $ readIORef ref
        case filter ((== n) . name) users of
            [u] -> return u
            _ -> fail "unknown user"

runAppM :: AppM a -> DatabaseConfig -> IO a
runAppM app = runReaderT (unAppM app)

test :: MonadDatabase m => m User
test = getUser (Name "foobar")

main :: IO ()
main = do
    ref <- newIORef
                [ User (Name "toto") 42
                , User (Name "foobar") 13
                ]
    let cfg = DatabaseConfig ref
    runAppM test cfg >>= print

