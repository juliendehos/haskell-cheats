import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

askInput :: M.Map String b -> IO String
askInput m = do
    putStrLn $ "\nEnv: " ++ unwords (M.keys m)
    putStr "> "
    hFlush stdout
    getLine

newtype Env m = Env (M.Map String (Int -> StateT (Env m) m Int))

env0 :: Monad m => Env m 
env0 = Env $ M.fromList
    [ ("inc", return . (+1))
    , ("mkIncN", \n -> do
        modify (\(Env m) -> Env (M.insert ("inc" ++ show n) (return . (+n)) m))
        return 0)
    ]

loop :: StateT (Env IO) IO ()
loop = forever $ do
    Env m <- get
    input <- liftIO $ askInput m
    case words input of
        [fName, nStr] ->
            case M.lookup fName m of
                Nothing -> liftIO $ putStrLn $ "don't know: " ++ fName
                Just f -> f (read nStr) >>= liftIO . print
        _ -> liftIO exitSuccess

main :: IO ()
main = evalStateT loop env0

