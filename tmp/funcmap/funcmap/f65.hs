import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

askInput :: M.Map String b -> IO String
askInput m = do
    putStrLn $ "\nEnv: " ++ unwords (M.keys m)
    putStr "> "
    hFlush stdout
    getLine

newtype Env = Env (M.Map String (Int -> Int))

env0 :: Env
env0 = Env $ M.fromList
    [ ("inc", (+1))
    ]

loop :: String -> Env -> (String, Env)
loop input e@(Env m) = 
    case words input of
        ["mkIncN", nStr] -> let n = read nStr
                                fName = "inc" ++ show n
                                f n' = n + n'
                                m' = M.insert fName f m
                            in ("0", Env m')
        [fName, nStr] ->
            case M.lookup fName m of
                Nothing -> ("don't know: " ++ fName, e)
                Just f -> let r = f (read nStr)
                          in (show r, e)
        _ -> ("", e)

main :: IO ()
main = run env0
    where run env@(Env m) = do
            input <- askInput m
            let (r, e) = loop input env
            when (r /= "") $ do
                putStrLn r
                run e

