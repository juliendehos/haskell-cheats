import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

askInput :: M.Map String b -> IO String
askInput m = do
    putStrLn $ "\nEnv: " ++ unwords (M.keys m)
    putStr "> "
    hFlush stdout
    getLine

newtype Env = Env (M.Map String (Int -> State Env Int))

env0 :: Env 
env0 = Env $ M.fromList
    [ ("inc", return . (+1))
    , ("mkIncN", \n -> do
        modify (\(Env m) -> Env (M.insert ("inc" ++ show n) (return . (+n)) m))
        return 0)
    ]

loop :: String -> State Env String
loop input = do
    Env m <- get
    case words input of
        [fName, nStr] ->
            case M.lookup fName m of
                Nothing -> return $ "don't know: " ++ fName
                Just f -> show <$> f (read nStr)
        _ -> return ""

main :: IO ()
main = run env0
    where run env@(Env m) = do
            input <- askInput m
            let (r, e) = runState (loop input) env
            when (r /= "") $ do
                putStrLn r
                run e

