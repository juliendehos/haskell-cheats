import qualified Data.Map.Lazy as M
import Control.Monad.State

newtype Env m a = Env (M.Map String (Int -> StateT (Env m a) m Int))

env0 :: Monad m => Env m a
env0 = Env $ M.fromList
    [ ("add1", return . (+1))
    , ("insert", \n -> do
        modify (\(Env m) -> Env (M.insert "mul2" (return . (*2)) m))
        return n)
    ]

run :: Monad m => String -> Int -> StateT (Env m a) m Int
run fname x = do
    (Env m) <- get
    case M.lookup fname m of
        Nothing -> error $ "don't know: " ++ fname
        Just f -> f x

loop :: StateT (Env IO a) IO ()
loop = do
    input <- liftIO getLine
    case words input of
        [fname, str] -> do
            r <- run fname (read str)
            liftIO $ print r 
            loop
        _ -> error "invalid input"

main :: IO ()
main = evalStateT loop env0

