import qualified Data.Map.Lazy as M
import Control.Monad.State

newtype Env a = Env (M.Map String (Int -> StateT (Env a) IO Int))

env0 :: Env a
env0 = Env $ M.fromList
    [ ("add1", \n -> return (n+1))
    , ("insert", \n -> let f (Env m) = Env (M.insert "mul2" (\x -> return (x*2)) m)
                       in modify f >> return n)
    ]

run :: String -> Int -> StateT (Env a) IO Int
run fname x = do
    (Env m) <- get
    case M.lookup fname m of
        Nothing -> error $ "don't know: " ++ fname
        Just f -> f x

loop :: StateT (Env a) IO ()
loop = do
    input <- liftIO getLine
    case words input of
        [fname, str] -> do
            r <- run fname (read str)
            liftIO $ print r 
            loop
        _ -> error "invalid input"

main :: IO ()
main = evalStateT loop env0

