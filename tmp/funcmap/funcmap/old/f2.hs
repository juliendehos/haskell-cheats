import qualified Data.Map.Lazy as M

newtype Env = Env (M.Map String (Int -> Int))

env0 :: Env
env0 = Env $ M.fromList
    [ ("add1", (+1))
    , ("mul2", (*2))
    ]

run :: String -> Int -> Env -> (Int, Env)
run "insert" x (Env m) = (x, Env $ M.insert "mul3" (*3) m)
run fname x (Env m) = case M.lookup fname m of
    Nothing -> error $ "don't know: " ++ fname
    Just f -> (f x, Env m)

loop :: Env -> IO ()
loop env = do
    input <- getLine
    case words input of
        [fname, str] -> do
            let (r, env') = run fname (read str) env
            print r 
            loop env'
        _ -> error "invalid input"

main :: IO ()
main = loop env0

