import qualified Data.Map.Lazy as M

type Env = M.Map String (Int -> Int)

env0 :: Env
env0 = M.fromList
    [ ("add1", (+1))
    , ("mul2", (*2))
    ]

run :: String -> Int -> Int
run fname x = case M.lookup fname env0 of
    Nothing -> error $ "don't know: " ++ fname
    Just f -> f x

main :: IO ()
main = do
    input <- getLine
    case words input of
        [fname, str] -> do
            print $ run fname (read str)
            main
        _ -> error "invalid input"

