import qualified Data.Map.Lazy as M

newtype Env a = Env (M.Map String (Int -> Env a -> (Int, Env a)))

env0 :: Env a
env0 = Env $ M.fromList
    [ ("add1", \n e -> (n+1, e))
    , ("insert", \n (Env m) -> (n, Env $ M.insert "mul2" (\x e -> (x*2, e)) m))
    ]

run :: String -> Int -> Env a -> (Int, Env a)
run fname x e0@(Env m) = case M.lookup fname m of
    Nothing -> error $ "don't know: " ++ fname
    Just f -> let (r, e1) = f x e0 in (r, e1)

loop :: Env a -> IO ()
loop env = do
    input <- getLine
    case words input of
        [fname, str] -> do
            let (r, env') = run fname (read str) env
            print r 
            loop env'
        _ -> error "invalid input"

main :: IO ()
main = loop env0

