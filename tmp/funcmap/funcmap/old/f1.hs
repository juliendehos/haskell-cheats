import qualified Data.Map.Lazy as M

type Env = M.Map String (Int -> Int)

env0 :: Env
env0 = M.fromList
    [ ("add1", (+1))
    , ("mul2", (*2))
    ]

run :: String -> Int -> Env -> (Int, Env)
run "insert" x env = (x, M.insert "mul3" (*3) env)
run fname x env = case M.lookup fname env of
    Nothing -> error $ "don't know: " ++ fname
    Just f -> (f x, env)

loop :: Env -> IO ()
loop env = do
    input <- getLine
    case words input of
        [fname, str] -> do
            let (r, env') = run fname (read str) env
            print r 
            loop env'
        _ -> error "invalid input"

main :: IO ()
main = loop env0

