import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

askInput :: M.Map String b -> IO String
askInput m = do
    putStrLn $ "\nEnv: " ++ unwords (M.keys m)
    putStr "> "
    hFlush stdout
    getLine

newtype Env m = Env (M.Map String ([Int] -> StateT (Env m) m Int))

env0 :: Monad m => Env m
env0 = Env $ M.fromList
    [ ("inc", return . (+1) . head)
    , ("sum", return . sum)
    , ("mkIncN", \xs -> do
        let n = head xs
        modify (\(Env m) -> Env (M.insert ("inc" ++ show n) (return . (+n) . head) m))
        return 0)
    ]

loop :: StateT (Env IO) IO ()
loop = forever $ do
    Env m <- get
    input <- liftIO $ askInput m
    case words input of
        [] -> liftIO exitSuccess
        (fname : params) ->
            case M.lookup fname m of
                Nothing -> liftIO $ putStrLn $ "don't know: " ++ fname
                Just f -> f (map read params) >>= liftIO . print

main :: IO ()
main = evalStateT loop env0

