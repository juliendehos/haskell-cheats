import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

askInput :: M.Map String b -> IO String
askInput m = do
    putStrLn $ "\nEnv: " ++ unwords (M.keys m)
    putStr "> "
    hFlush stdout
    getLine

type Env = M.Map String (Int -> Int)

env0 :: Env
env0 = M.fromList
    [ ("inc", (+1))
    ]

loop :: String -> Env -> (String, Env)
loop input m = 
    case words input of
        ["mkIncN", nStr] -> let n = read nStr
                                fName = "inc" ++ show n
                                f n' = n + n'
                                m' = M.insert fName f m
                            in ("0", m')
        [fName, nStr] ->
            case M.lookup fName m of
                Nothing -> ("don't know: " ++ fName, m)
                Just f -> let r = f (read nStr)
                          in (show r, m)
        _ -> ("", m)

main :: IO ()
main = run env0
    where run m = do
            input <- askInput m
            let (r, m') = loop input m
            when (r /= "") $ do
                putStrLn r
                run m'

