import Control.Applicative
import Control.Monad
import Data.Char
import Control.Monad.Trans.State

type Parser = StateT String Maybe

runParser :: Parser a -> String -> Maybe (a, String)
runParser = runStateT

itemP :: Parser Char
itemP = do
    c:cs <- get
    put cs
    return c

satisfyP :: (Char -> Bool) -> Parser Char
satisfyP p = do
    c <- itemP
    if p c then return c else fail ""

charP :: Char -> Parser Char
charP c = satisfyP (==c)

digitsP :: Parser String
digitsP = some (satisfyP isDigit)

natP :: Parser Int
natP = read <$> digitsP

mulP :: Parser Int
mulP =  (*) <$> natP <*> (charP '*' *> mulP)
    <|> natP

addP :: Parser Int
addP =  (+) <$> mulP <*> (charP '+' *> addP)
    <|> mulP
 
main :: IO ()
main = do
    input <- getLine
    when (input /= "") $ do
        print $ runParser addP input
        main

