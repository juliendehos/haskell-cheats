{-# LANGUAGE FlexibleInstances #-}

import Control.Applicative
import Control.Monad hiding (fail)
import Data.Char
import Control.Monad.Fail
import Control.Monad.Trans.State
import Prelude hiding (fail)

type Parser = StateT String (Either String)

runParser :: Parser a -> String -> Either String (a, String)
runParser = runStateT

instance Alternative (Either String) where
    empty = Left "empty"
    Left _ <|> e2 = e2
    e1 <|> _ = e1

instance MonadFail (Either String) where
    fail = Left

instance MonadPlus (Either String)

itemP :: Parser Char
itemP = do
    c:cs <- get
    put cs
    return c

satisfyP :: (Char -> Bool) -> Parser Char
satisfyP p = do
    c <- itemP
    if p c then return c else fail "not a digit"

charP :: Char -> Parser Char
charP c = satisfyP (==c)

digitsP :: Parser String
digitsP = some (satisfyP isDigit)

natP :: Parser Int
natP = read <$> digitsP

mulP :: Parser Int
mulP =  (*) <$> natP <*> (charP '*' *> mulP)
    <|> natP

addP :: Parser Int
addP =  (+) <$> mulP <*> (charP '+' *> addP)
    <|> mulP
 
main :: IO ()
main = do
    input <- getLine
    when (input /= "") $ do
        print $ runParser addP input
        main

