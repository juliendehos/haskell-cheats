import           Control.Monad.State
import qualified Data.Map.Lazy as M
import           System.Exit
import           System.IO

import Control.Monad.Trans.Class
import Control.Monad.Trans.State
import qualified Control.Monad.Trans.Reader as R
import Control.Monad.IO.Class

type Store = M.Map String (Int -> Int)

type Env = String

type InterpM = StateT Store (R.ReaderT Env IO)

store0 :: Store
store0 = M.fromList
    [ ("inc", (+1))
    ]

env0 :: Env
env0 = "inc 41"

run :: StateT Env (StateT Store IO) ()

main :: IO ()
main = evalStateT (evalStateT id env0) store0


