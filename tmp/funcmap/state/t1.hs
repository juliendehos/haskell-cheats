type MyState = String

consume1 :: MyState -> (Maybe Char, MyState)
consume1 s0 = 
    case s0 of
        [] -> (Nothing, [])
        (x:xs) -> (Just x, xs)

forget1 :: MyState -> MyState
forget1 s0 = 
    let (_, s1) = consume1 s0
    in s1

myStory :: MyState -> (Maybe Char, MyState)
myStory s0 =
    let s1 = forget1 s0
        s2 = forget1 s1
        (_, s3) = consume1 s2
    in consume1 s3

main :: IO ()
main = do
    let state0 = "foobar"
    print $ forget1 state0
    print $ consume1 state0
    print $ myStory state0

