import Control.Monad.Reader
import Data.IORef

type Env = IORef String

consume1 :: MonadIO m => ReaderT Env m (Maybe Char)
consume1 = do
    ref0 <- ask
    str <- liftIO $ readIORef ref0
    case str of
        [] -> return Nothing
        (x:xs) -> liftIO (writeIORef ref0 xs) >> return (Just x)

forget1 :: MonadIO m => ReaderT Env m ()
forget1 = void consume1

myStory :: MonadIO m => ReaderT Env m (Maybe Char)
myStory = do
    liftIO $ putStrLn "inside myStory"
    forget1
    forget1
    _ <- consume1
    consume1

main :: IO ()
main = do
    env0 <- newIORef "foobar"
    runReaderT myStory env0 >>= print
    readIORef env0 >>= print

