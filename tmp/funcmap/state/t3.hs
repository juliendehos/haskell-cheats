import Control.Monad.State

type MyState = String

consume1 :: Monad m => StateT MyState m (Maybe Char)
consume1 = do
    s0 <- get
    case s0 of
        [] -> return Nothing
        (x:xs) -> put xs >> return (Just x)

forget1 :: Monad m => StateT MyState m ()
forget1 = void consume1

myStory :: MonadIO m => StateT MyState m (Maybe Char)
myStory = do
    liftIO $ putStrLn "inside myStory"
    forget1
    forget1
    _ <- consume1
    consume1

main :: IO ()
main = do
    let state0 = "foobar"
    runStateT forget1 state0 >>= print
    runStateT consume1 state0 >>= print
    runStateT myStory state0 >>= print

