{-# LANGUAGE FlexibleContexts #-}

import Control.Monad.Reader
import Control.Monad.State

type Env = String
type Store = Int

checkN :: MonadReader Env m => Int -> StateT Store m (ReaderT Env m String)
checkN n = do
    env <- ask
    let str = take n env
    modify (+ length str)
    return $ return str

myStory :: (MonadReader Env m, MonadIO m) => StateT Store m (ReaderT Env m Int)
myStory = do
    liftIO $ putStrLn "inside myStory"
    _ <- checkN 2
    _ <- checkN 42
    gets return

main :: IO ()
main = do
    let env0 = "foobar"
        store0 = 0
    (_, s1) <- runReaderT (runStateT myStory store0) env0
    print s1
    putStrLn "done"

