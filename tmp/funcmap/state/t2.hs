import Control.Monad.State

type MyState = String

consume1 :: State MyState (Maybe Char)
consume1 = do
    s0 <- get
    case s0 of
        [] -> return Nothing
        (x:xs) -> put xs >> return (Just x)

forget1 :: State MyState ()
forget1 = void consume1

myStory :: State MyState (Maybe Char)
myStory = do
    forget1
    forget1
    _ <- consume1
    consume1

main :: IO ()
main = do
    let state0 = "foobar"
    print $ runState forget1 state0
    print $ runState consume1 state0
    print $ runState myStory state0

