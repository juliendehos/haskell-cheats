{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude

class Monad m => MonadChat m where
    invite :: Text -> m ()
    here :: m [Text]
    status :: Text -> m ()

instance MonadChat IO where

    invite val = putTextLn ("You invited @" <> val <> "!")

    here = do
        let users = ["@Marge", "@Homer", "@Maggie"]
        putTextLn ("In this channel: " <> unwords users)
        pure users

    status val = putTextLn ("Setting status to '" <> val <> "'.")

example :: MonadChat m => m ()
example = do
    invite "Lisa"
    users <- here
    invite "Bart"
    status "Working from home"

main :: IO ()
main = example

