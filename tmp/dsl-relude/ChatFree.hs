{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Functor
import Control.Monad.Free as Free
import Relude

data Command next 
    = Invite Text next
    | Here ([Text] -> next)
    | Status Text next
    deriving (Functor)

invite :: Text -> Free Command ()
invite val = Free.liftF (Invite val ())

here :: Free Command [Text]
here = Free.liftF (Here identity)

status :: Text -> Free Command ()
status val = Free.liftF (Status val ())

interpret :: Command a -> IO a
interpret = \case
    Invite val next -> do
        putTextLn ("You invited @" <> val <> "!")
        pure next
    Here next -> do
        let users = ["@Marge", "@Homer", "@Maggie"]
        putTextLn ("In this channel: " <> unwords users)
        pure (next users)
    Status val next -> do
        putTextLn ("Setting status to '" <> val <> "'.")
        pure next

example :: Free Command ()
example = do
    invite "Lisa"
    users <- here
    invite "Bart"
    status "Working from home"

main :: IO ()
main = void $ Free.foldFree interpret example

