{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude  

data Command a where
    -- Monad api at a type level
    Pure :: a -> Command a
    Bind :: Command a -> (a -> Command b) -> Command b

    -- Constructors
    Invite :: Text -> Command ()
    Here :: Command [Text]
    Status :: Text -> Command ()

instance Functor Command where
    -- fmap :: (a -> b) -> Command a -> Command b
    fmap f g = Bind g (Pure . f)

instance Applicative Command where
    -- pure :: a -> Command a
    pure = Pure
    -- (<*>) :: Command (a -> b) -> Command a -> Command b
    (<*>) f g = Bind f (<$> g)

instance Monad Command where
    -- (>>=) :: Command a -> (a -> Command b) -> Command b
    (>>=) = Bind

interpret :: Command a -> IO a
interpret = \case 
    Pure a -> pure a
    Bind f g -> interpret f >>= interpret . g
    Invite val -> putTextLn ("You invited @" <> val <> "!")
    Here -> do
        let users = ["@Marge", "@Homer", "@Maggie"]
        putTextLn ("In this channel: " <> unwords users)
        pure users
    Status val -> putTextLn ("Setting status to '" <> val <> "'.")

example :: Command ()
example = do
    Invite "Lisa"
    users <- Here
    Invite "Bart"
    Status "Working from home"

main :: IO ()
main = void $ interpret example

