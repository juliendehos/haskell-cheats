{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (forkIO)
import Control.Monad (forever)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (fromStrict)
import Data.IORef (atomicWriteIORef, IORef, newIORef, readIORef)
import GHC.Int
import qualified Web.Scotty as SC
import System.Environment

import OpenCV
import OpenCV.VideoIO.Types

openCam :: Int32 -> IO (Maybe VideoCapture)
openCam iSource = do
    cap <- newVideoCapture
    exceptErrorIO $ videoCaptureOpen cap $ VideoDeviceSource iSource Nothing
    isOpened <- videoCaptureIsOpened cap
    if isOpened 
    then return Nothing
    else do
        _ <- videoCaptureSetD cap VideoCapPropFps 5 
        return $ Just cap

captureCam :: VideoCapture -> IO (Maybe (Mat ('S ['D, 'D]) 'D 'D))
captureCam cap = videoCaptureGrab cap >> videoCaptureRetrieve cap 

imgToPng :: Mat ('S ['D, 'D]) 'D 'D -> ByteString
imgToPng = exceptError . imencode (OutputPng defaultPngParams)

runServer :: Int -> IORef ByteString -> IO ()
runServer port pngRef = SC.scotty port $ do
    SC.get "/" $ SC.file "index.html"
    SC.get "/out.png" $ do
        SC.setHeader "Content-Type" "image/png"
        img <- SC.liftAndCatchIO (readIORef pngRef) 
        SC.raw $ fromStrict img

runCam :: VideoCapture -> IORef ByteString -> IO ()
runCam cap pngRef = forever $ do
    imgMaybe <- fmap imgToPng <$> captureCam cap
    mapM_ (atomicWriteIORef pngRef) imgMaybe

-- TODO parse args using optparse

main :: IO ()
main = do
    iSource <- read . head <$> getArgs
    capMaybe <- openCam iSource
    case capMaybe of
        Nothing -> putStrLn "couldn't open device"
        Just cap -> do
            Just png0 <- fmap imgToPng <$> captureCam cap
            pngRef <- newIORef png0
            _ <- forkIO $ runCam cap pngRef
            runServer 3000 pngRef

