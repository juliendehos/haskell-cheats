#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <vector>

///////////////////////////////////////////////////////////////////////////////

struct Game {
    virtual bool isTerminated() const = 0;
    virtual int nbLegalActions() const = 0;
    virtual void applyAction(int iAction) = 0;
    virtual ~Game() = default;
    virtual void print() const = 0;
};

struct Bot {
    virtual int selectAction(const Game & game) = 0;
};

///////////////////////////////////////////////////////////////////////////////

struct Action1 {
    int _pos1;
    void print() const {
        std::cout << "Action1 " << _pos1 << std::endl;
    }
};

struct Game1 : Game {
    int _target1;
    Action1 _last1;
    Game1() : _target1(1), _last1({-1}) {}
    bool isTerminated() const override {
        return _target1 == _last1._pos1;
    }
    int nbLegalActions() const override {
        return 5;
    }
    void applyAction(int iAction) override {
        if (iAction >= 0 and iAction < nbLegalActions())
        _last1 = {iAction};
    }
    void print() const override {
        std::cout << "Game1 " << _target1 << " ";
        _last1.print();
        for (int i=0; i<nbLegalActions(); i++)
            std::cout << i << " ";
        std::cout << std::endl;
    }
};

///////////////////////////////////////////////////////////////////////////////

struct Action2 {
    int _pos2;
    void print() const {
        std::cout << "Action2 " << _pos2 << std::endl;
    }
};

struct Game2 : Game {
    int _target2;
    Action2 _last2;
    Game2() : _target2(2), _last2({-1}) {}
    bool isTerminated() const override {
        return _target2 == _last2._pos2;
    }
    int nbLegalActions() const override {
        return 5;
    }
    void applyAction(int iAction) override {
        if (iAction >= 0 and iAction < nbLegalActions())
        _last2 = {iAction};
    }
    void print() const override {
        std::cout << "Game2 " << _target2 << " ";
        _last2.print();
        for (int i=0; i<nbLegalActions(); i++)
            std::cout << i << " ";
        std::cout << std::endl;
    }
};

///////////////////////////////////////////////////////////////////////////////

struct BotHuman : Bot {
    int selectAction(const Game & game) override {
        while (true) {
            try {
                std::string line;
                std::getline(std::cin, line);
                int n = stoi(line);
                if (n < game.nbLegalActions())
                    return n;
                else
                    std::cout << "bad input" << std::endl;
            }
            catch (...) {
                std::cout << "bad index" << std::endl;
            }
        }
    }
};

struct BotRandom : Bot {
    std::mt19937 _engine;
    BotRandom() : _engine(std::random_device{}()) {}
    int selectAction(const Game & game) override {
        int nbActions = game.nbLegalActions();
        std::uniform_int_distribution<int> distribution(0, nbActions);
        return distribution(_engine);
    }
};

///////////////////////////////////////////////////////////////////////////////

std::unique_ptr<Game> gameFactory(const std::string & gameName) {
    if (gameName == "game1") return std::make_unique<Game1>();
    else if (gameName == "game2") return std::make_unique<Game2>();
    else return {};
}

std::unique_ptr<Bot> botFactory(const std::string & botName) {
    if (botName == "botHuman") return std::make_unique<BotHuman>();
    else if (botName == "botRandom") return std::make_unique<BotRandom>();
    else return {};
}

void play(Game & game, Bot & bot) {
    while (not game.isTerminated()) {
        game.print();
        int iAction = bot.selectAction(game);
        std::cout << "-> " << iAction << std::endl;
        game.applyAction(iAction);
    }
}

int main(int argc, char ** argv) {
    if (argc != 3) {
        std::cout << "args: <game> <bot>" << std::endl;
        exit(-1);
    }
    const std::string gameName = argv[1];
    const std::string botName = argv[2];

    auto game = gameFactory(gameName);
    if (not game) {
        std::cout << "unknown game" << std::endl;
        exit(-1);
    }

    auto bot = botFactory(botName);
    if (not bot) {
        std::cout << "unknown bot" << std::endl;
        exit(-1);
    }

    play(*game, *bot);
    game->print();

    return 0;
}

