{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE StandaloneDeriving #-}

import System.Environment (getArgs)
import Text.Read (readMaybe)
import System.Random (randomRIO)

-------------------------------------------------------------------------------

class Game g where
    isTerminated :: g -> Bool
    nbLegalActions :: g -> Int
    applyAction :: Int -> g -> g

data AnyGame = forall g. (Game g, Show g) => AnyGame g 

deriving instance Show AnyGame

instance Game AnyGame where
    isTerminated (AnyGame g) = isTerminated g
    nbLegalActions (AnyGame g) = nbLegalActions g
    applyAction i (AnyGame g) = AnyGame $ applyAction i g

-------------------------------------------------------------------------------

class Bot b where
    selectAction :: Game g => g -> b -> IO (Int, b)

data AnyBot = forall b. Bot b => AnyBot b

instance Bot AnyBot where
    selectAction g (AnyBot b) = do
        (i, b') <- selectAction g b
        return (i, AnyBot b')

-------------------------------------------------------------------------------

newtype Action1 = Action1 { pos1 :: Int } deriving Show

data Game1 = Game1
    { target1 :: Int
    , last1 :: Action1
    } deriving Show

instance Game Game1 where
    isTerminated (Game1 n l) = n == pos1 l
    nbLegalActions _ = 4
    applyAction i g = g { last1 = Action1 i }

newGame1 :: Game1
newGame1 = Game1 1 (Action1 (-1))

-------------------------------------------------------------------------------

newtype Action2 = Action2 { pos2 :: Int } deriving Show

data Game2 = Game2
    { target2 :: Int
    , last2 :: Action2
    } deriving Show

instance Game Game2 where
    isTerminated (Game2 n l) = n == pos2 l
    nbLegalActions _ = 4
    applyAction i g = g { last2 = Action2 i }

newGame2 :: Game2
newGame2 = Game2 2 (Action2 (-1))

-------------------------------------------------------------------------------

data BotHuman = BotHuman

instance Bot BotHuman where
    selectAction g b = do
        line <- getLine
        case readMaybe line of
            Nothing -> putStrLn "bad input" >> selectAction g b
            Just iAction -> if iAction < 0 || iAction >= nbLegalActions g
                            then putStrLn "bad index" >> selectAction g b
                            else return (iAction, b)

-------------------------------------------------------------------------------

data BotRandom = BotRandom

instance Bot BotRandom where
    selectAction g b = do
        iAction <- randomRIO (0, nbLegalActions g - 1)
        return (iAction, b)

-------------------------------------------------------------------------------

gameFactory :: String -> Maybe AnyGame
gameFactory "game1" = Just $ AnyGame newGame1
gameFactory "game2" = Just $ AnyGame newGame2
gameFactory _ = Nothing

botFactory :: String -> Maybe AnyBot
botFactory "botHuman" = Just $ AnyBot BotHuman
botFactory "botRandom" = Just $ AnyBot BotRandom
botFactory _ = Nothing

play :: AnyGame -> AnyBot -> IO AnyGame
play g b = do
    let nbActions = nbLegalActions g
    print g
    putStrLn $ unwords $ show <$> [0 .. nbActions - 1]
    (iAction, b') <- selectAction g b
    putStrLn $ "-> " <> show iAction
    if iAction < 0 || iAction >= nbActions
    then putStrLn "bad index" >> play g b'
    else let g' = applyAction iAction g
         in if isTerminated g' then return g' else play g' b'

main :: IO ()
main = do
    args <- getArgs
    case args of
        [gameName, botName] -> case (gameFactory gameName, botFactory botName) of
            (Just g, Just b) -> play g b >>= print
            _ -> putStrLn "unknown game/bot"
        _ -> putStrLn "args: <game> <bot>"

