
Games & bots platform TODO
===============================================================================

:Date:
    2020-02-10

:Examples:
   `projects/games <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/projects/games>`__

.. literalinclude:: games.cpp
    :caption: games.cpp
    :language: cpp

.. literalinclude:: games.hs
    :caption: games.hs
    :language: haskell

