import qualified Data.Vector as V 
import System.Random (getStdGen, StdGen, randomR)

type Joueur = Char
type Coup = Int

data Tictactoe = Tictactoe 
    { grille           :: V.Vector Joueur
    , coupsPossibles   :: V.Vector Coup
    , joueurCourant    :: Joueur
    , joueurSuivant    :: Joueur
    } deriving (Show)

jeuInitial :: Tictactoe
jeuInitial = Tictactoe (V.replicate 9 '.') (V.fromList [0..8]) 'X' 'O'

jouerCoup :: Tictactoe -> Coup -> Tictactoe
jouerCoup jeu coup = 
    if coup `elem` coupsPossibles jeu then jeu' else error "coup invalide"
    where grille' = grille jeu V.// [(coup, joueurCourant jeu)]
          coups' = V.filter (/= coup) (coupsPossibles jeu) 
          jeu' = Tictactoe grille' coups' (joueurSuivant jeu) (joueurCourant jeu)

calculerFin :: Tictactoe -> (Bool, Joueur)
calculerFin jeu
    | g!0 /= '.' && g!0 == g!1 && g!1 == g!2 = (True, g!0)  -- ligne 1
    | g!3 /= '.' && g!3 == g!4 && g!4 == g!5 = (True, g!3)  -- ligne 2
    | g!6 /= '.' && g!6 == g!7 && g!7 == g!8 = (True, g!6)  -- ligne 3
    | g!0 /= '.' && g!0 == g!3 && g!3 == g!6 = (True, g!0)  -- col 1
    | g!1 /= '.' && g!1 == g!4 && g!4 == g!7 = (True, g!1)  -- col 2
    | g!2 /= '.' && g!2 == g!5 && g!5 == g!8 = (True, g!2)  -- col 3
    | g!0 /= '.' && g!0 == g!4 && g!4 == g!8 = (True, g!0)  -- diag 1
    | g!2 /= '.' && g!2 == g!4 && g!4 == g!6 = (True, g!2)  -- diag 2
    | null (coupsPossibles jeu) = (True, '.')               -- égalité
    | otherwise = (False, '.')                              -- en cours
    where g = grille jeu 
          (!) = (V.!)

type Bot = Tictactoe -> StdGen -> (Coup, StdGen) 

botRandom :: Bot
botRandom jeu rng = (coup, rng') 
    where nbCoups = length $ coupsPossibles jeu
          (iCoup, rng') = randomR (0, nbCoups - 1) rng
          coup = coupsPossibles jeu V.! iCoup

jouerJeu :: Tictactoe -> Bot -> Bot -> StdGen -> (Joueur, Tictactoe, StdGen)
jouerJeu jeu botX botO rng = 
    if estFini then (vainqueur, jeu', rng') else continue
    where bot = if joueurCourant jeu == 'X' then botX else botO
          (coup, rng') = bot jeu rng
          jeu' = jouerCoup jeu coup
          (estFini, vainqueur) = calculerFin jeu'
          continue = jouerJeu jeu' botX botO rng'

evalCoup :: Coup -> Int -> Tictactoe -> StdGen -> (Int, StdGen) 
evalCoup coup nbSims jeu rng = evalCoupAux 0 nbSims rng
    where joueur = joueurCourant jeu
          jeu1 = jouerCoup jeu coup

          evalCoupAux nbVicts 0 rngAux = (nbVicts, rngAux)
          evalCoupAux nbVicts nbSimsAux rngAux = 
            let (vainq, _, rngAux') = jouerJeu jeu1 botRandom botRandom rngAux
                nbVicts' = if vainq == joueur then nbVicts+1 else nbVicts
            in evalCoupAux nbVicts' (nbSimsAux-1) rngAux'

botMc :: Int -> Bot
botMc maxSims jeu rng = botMcAux (nbCoups-1) 0 0 rng
    where nbCoups = length $ coupsPossibles jeu
          nbSimsParCoup = max 1 $ div maxSims nbCoups

          botMcAux (-1) best _ rngAux = (best, rngAux)
          botMcAux iCoup best vBest rngAux =
            let coup = coupsPossibles jeu V.! iCoup
                (vCoup, rng') = evalCoup coup nbSimsParCoup jeu rngAux
                (vBest', best') = max (vCoup, coup) (vBest, best)
            in botMcAux (iCoup - 1) best' vBest' rng'

main :: IO ()
main = do
    (winner, jeu, _) <- jouerJeu jeuInitial botRandom (botMc 100) <$> getStdGen
    print jeu
    putStrLn $ "winner: " ++ show winner

