
Basic AI for Tictactoe TODO
===============================================================================

:Date:
    2019-03-08

:Examples:
   `projects/tictactoe <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/projects/tictactoe>`__

.. literalinclude:: src/TictactoeStateAdt.hs
    :caption: src/TictactoeStateAdt.hs
    :language: haskell

