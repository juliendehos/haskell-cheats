#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.mtl])"

import Control.Monad.State

newtype Logger = Logger { runLogger :: State [String] String }

emptyLogger :: [String]
emptyLogger = [] 

execLogger :: Logger -> [String]
execLogger logger = execState (runLogger logger) emptyLogger

{-
logMsg :: String -> Logger
logMsg msg = do
    str <- get
    return str
-- logMsg msg = modify $ \ s -> msg : s

test :: Logger
test = do
    logMsg "msg 1"
    logMsg "msg 2"
-}

main :: IO ()
main = print 42
-- main = print $ execLogger logMsg

