
data Term
    = TermText String
    | TermElement String [Term]
    deriving Show

p_ :: [Term] -> Term
p_ = TermElement "p"

div_ :: [Term] -> Term
div_ = TermElement "div"

mkText :: String -> Term
mkText = TermText

render :: Term -> String 
render (TermText str) = str
render (TermElement name body)
    = "<" ++ name ++ ">" ++ concatMap render body ++ "</" ++ name ++ ">"

myhtml :: Term
myhtml = div_ 
    [ p_ [mkText "toto"]
    , p_ [mkText "tata"]
    ]

main :: IO ()
main = putStrLn $ render myhtml

-- https://hackage.haskell.org/package/lucid-2.9.12/docs/Lucid-Base.html

