let

  eff-src = fetchTarball {
    url = https://github.com/hasura/eff/archive/bf0520d.tar.gz;
    sha256 = "1a3g013i0l38zd9nk9davf8876bvvqa8hmb3v6jgzrgw5mwhndji";
  };

  config = {
    packageOverrides = pkgs: {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: with pkgs.haskell.lib; {
          eff = self.callCabal2nix "eff" eff-src {};
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in pkgs.mkShell {
  buildInputs = [
    (pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
      eff
    ]))
  ];
}

