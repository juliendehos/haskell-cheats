{-# LANGUAGE 
    BangPatterns, BlockArguments, ConstraintKinds, DataKinds, DefaultSignatures,
    DeriveDataTypeable, DeriveFoldable, DeriveFunctor, DeriveGeneric, DeriveLift,
    DeriveTraversable, DerivingStrategies, DerivingVia, EmptyCase,
    ExistentialQuantification, FlexibleContexts, FlexibleInstances,
    FunctionalDependencies, GADTs, GeneralizedNewtypeDeriving, InstanceSigs,
    KindSignatures, LambdaCase, MultiParamTypeClasses, MultiWayIf, NamedFieldPuns,
    OverloadedStrings, PatternSynonyms, QuantifiedConstraints, RankNTypes,
    RoleAnnotations, ScopedTypeVariables, StandaloneDeriving,
    TupleSections, TypeApplications, TypeFamilies,
    TypeFamilyDependencies, TypeOperators, ViewPatterns
#-}

import qualified System.IO as IO
import Prelude hiding (readFile, writeFile)
import Control.Effect

----------------------------------------------------------------------
-- effect definition
----------------------------------------------------------------------

data FileSystem :: Effect where
    ReadFile :: FilePath -> FileSystem m String
    WriteFile :: FilePath -> String -> FileSystem m ()

readFile :: FileSystem :< effs => FilePath -> Eff effs String
readFile = send . ReadFile

writeFile :: FileSystem :< effs => FilePath -> String -> Eff effs ()
writeFile a b = send $ WriteFile a b

----------------------------------------------------------------------
-- IO handler
----------------------------------------------------------------------

runFileSystemIO :: IOE :< effs => Eff (FileSystem ': effs) a -> Eff effs a
runFileSystemIO = interpret \case
    ReadFile path -> liftIO $ IO.readFile path
    WriteFile path contents -> liftIO $ IO.writeFile path contents

----------------------------------------------------------------------
-- pure handler
----------------------------------------------------------------------

runFileSystemPure :: Error String :< effs => Eff (FileSystem ': effs) a -> Eff effs a
runFileSystemPure = lift
    >>> interpret \case
          ReadFile path -> do
            fileSystem <- get
            case lookup path fileSystem of
              Just contents -> pure contents
              Nothing       -> throw ("readFile: no such file " <> path)
          WriteFile path contents -> do
            fileSystem <- get
            -- add the new file and remove an old file with the same name, if it exists
            put ((path, contents) : filter ((/= path) . fst) fileSystem)
    >>> evalState @[(FilePath, String)] []

