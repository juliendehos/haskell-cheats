#!/bin/sh

ALLTESTS=$(find source -name "ci-test.sh" -exec realpath {} \;)

for onetest in ${ALLTESTS} ; do
    echo ""
    echo "**********************************************************************"
    echo ${onetest}
    echo "**********************************************************************"
    cd $(dirname ${onetest})
    ./$(basename ${onetest}) || exit 1
done

