import Game

import qualified Graphics.Gloss as G
import qualified Graphics.Gloss.Data.Bitmap as G (loadBMP)
import qualified Graphics.Gloss.Interface.IO.Interact as G

type MyGame = Game G.Picture

main :: IO ()
main = do
    bobPicture <- G.loadBMP "bob.bmp"
    let myGame = newGame bobPicture
    let myWindow = G.InWindow "movebob" (gameWidth, gameHeight) (0, 0)
    G.play myWindow G.white 30 myGame displayH eventH idleH

displayH :: MyGame -> G.Picture
displayH g = G.Pictures [bob, txt]
    where bob = G.Translate (_x g) (_y g) (_assets g)
          txt = G.Color G.black 
                    $ G.Translate (10 - gameWidth2) (10 - gameHeight2)
                    $ G.Scale 0.15 0.15
                    $ G.Text "use left/right arrows to move Bob"

eventH :: G.Event -> MyGame -> MyGame 
eventH (G.EventKey (G.SpecialKey G.KeyLeft) G.Down _ _)  g = g { _inputLeft = True }
eventH (G.EventKey (G.SpecialKey G.KeyLeft) G.Up _ _)    g = g { _inputLeft = False }
eventH (G.EventKey (G.SpecialKey G.KeyRight) G.Down _ _) g = g { _inputRight = True }
eventH (G.EventKey (G.SpecialKey G.KeyRight) G.Up _ _)   g = g { _inputRight = False }
eventH _ g = g

idleH :: Float -> MyGame -> MyGame
idleH = stepGame

