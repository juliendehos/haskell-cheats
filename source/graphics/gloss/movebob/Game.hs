module Game where

gameWidth, gameHeight :: Int
gameWidth = 400
gameHeight = 200

gameWidth2, gameHeight2 :: Float
gameWidth2 = fromIntegral (gameWidth `div` 2)
gameHeight2 = fromIntegral (gameHeight `div` 2)

gameVelocity :: Float
gameVelocity = 200

data Game a = Game
    { _inputLeft :: Bool
    , _inputRight :: Bool
    , _x :: Float
    , _y :: Float
    , _assets :: a
    }

newGame :: a -> Game a
newGame = Game False False 0 0

stepGame :: Float -> Game a -> Game a
stepGame dt g = g { _x = x2 }
    where dtLeft = if _inputLeft g then dt else 0
          dtRight = if _inputRight g then dt else 0
          x1 = _x g + gameVelocity * (dtRight - dtLeft)
          x2 | x1 > gameWidth2 = -gameWidth2
             | x1 < (-gameWidth2) = gameWidth2
             | otherwise = x1

