
Gloss
===============================================================================

:Date:
    2020-01-12

:Examples:
   `graphics/gloss <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/graphics/gloss>`__


Overview
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Gloss is a library for implementing graphical applications: vector/bitmap
graphics, animation, user interaction... It is simple to use, with a callback
model similar to 
`the OpenGL Utility Toolkit <https://en.wikipedia.org/wiki/OpenGL_Utility_Toolkit>`_ 

See also:
    - `Gloss documentation <https://hackage.haskell.org/package/gloss>`_
    - `Gloss repository <https://github.com/benl23x5/gloss>`_


Example: movebob
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Objective
-------------------------------------------------------------------------------

We want to implement a simple application that displays a bitmap; this bitmap
can be moved using the keyboard. 

.. only:: latex

   .. figure:: movebob.png
      :width: 6cm

      The movebob application, using Gloss. 

.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/movebob.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>



|



Game logic
-------------------------------------------------------------------------------

We define the game logic in a ``Game`` module, independently from Gloss.
First, some parameters:

.. literalinclude:: movebob/Game.hs
   :caption: movebob/Game.hs
   :language: haskell
   :lines: -12

Then, we define a polymorphic type ``Game``, to model the state of the game
(state of the keyboard, position of the bitmap, assets).  In this type, the
assets (here, the bitmap data) are typed using a polymorphic parameter; thus we
can use external types without requiring external dependency.

.. literalinclude:: movebob/Game.hs
   :caption: movebob/Game.hs
   :language: haskell
   :lines: 14-20

Finally, we define some functions to run the game (create a new game, update
the game using a given duration).

.. literalinclude:: movebob/Game.hs
   :caption: movebob/Game.hs
   :language: haskell
   :lines: 22-


Main application
-------------------------------------------------------------------------------

The main application uses Gloss to implement display and user events.  First,
we define a type alias representing a game where the assets are a picture. We
also define the main function to load the assets, create a window and run the
application.

.. literalinclude:: movebob/Main.hs
   :caption: movebob/Main.hs
   :language: haskell
   :lines: -14

Finally, we define the callback functions, to handle display requests, user
events and idle steps.

.. literalinclude:: movebob/Main.hs
   :caption: movebob/Main.hs
   :language: haskell
   :lines: 15-


