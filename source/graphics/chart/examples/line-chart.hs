import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

myData :: [(Double,Double)]
myData = [ (1, 2)
         , (2, 4)
         , (3, 2)
         , (4, 3) ]

main :: IO ()
main = toFile def "line-chart.png" $ do
    layout_title .= "line-chart"
    setColors [opaque blue]
    plot (line "my data" [myData])

