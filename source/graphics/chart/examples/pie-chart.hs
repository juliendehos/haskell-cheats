import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

myDef :: FileOptions
myDef = def { _fo_size = (400, 300) }

myColors :: [AlphaColour Double]
myColors = [ opaque red, opaque green, opaque blue ]

myData :: [(String, Double)]
myData = [ ("t1", 20)
         , ("t2", 50)
         , ("t3", 30) ] 

main :: IO ()
main = toFile myDef "pie-chart.png" $ do
    pie_title .= "pie-chart"
    pie_title_style . font_weight .= FontWeightNormal
    pie_background .= solidFillStyle transparent
    pie_plot . pie_colors .= myColors
    pie_plot . pie_data .= map myPitem myData
    where myPitem (s,v) = pitem_value .~ v $ pitem_label .~ s $ def

