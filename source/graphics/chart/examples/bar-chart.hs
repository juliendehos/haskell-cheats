import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

myTics :: [String]
myTics = [ "2017", "2018", "2019" ]

myValues :: [[Double]]
myValues = [ [13, 37], [7, 42], [24, 24] ]

myLabels :: [String]
myLabels = ["foo", "bar"]

main :: IO ()
main = toFile def "bar-chart.png" $ do
    layout_title .= "bar-chart"
    layout_title_style . font_weight .= FontWeightNormal
    layout_background .= solidFillStyle transparent
    layout_x_axis . laxis_generate .= autoIndexAxis myTics
    plot $ plotBars <$> bars myLabels (addIndexes myValues)

