
Chart
===============================================================================

:Date:
    2019-12-03

:Examples:
   `graphics/chart <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/graphics/chart>`__

Chart is a Haskell library for drawing 2D charts. It supports several backends
(Cairo, Diagrams) and many plot types. Chart has also a simplified "easy" API.

See also:
   - `the Chart package <http://hackage.haskell.org/package/Chart>`_
   - `the Chart wiki <http://hackage.haskell.org/package/Chart>`_


Line chart
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/line-chart.hs
   :caption: examples/line-chart.hs
   :language: haskell

.. figure:: line-chart.png
   :align: center
   :width: 80%

   Line chart.


Bar chart
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/bar-chart.hs
   :caption: examples/bar-chart.hs
   :language: haskell

.. figure:: bar-chart.png
   :align: center
   :width: 80%

   Bar chart.

Pie chart
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. literalinclude:: examples/pie-chart.hs
   :caption: examples/pie-chart.hs
   :language: haskell

.. figure:: pie-chart.png
   :align: center
   :width: 60%

   Pie chart.


