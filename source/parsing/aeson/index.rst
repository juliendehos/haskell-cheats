
JSON parsing/encoding (aeson)
===============================================================================

:Date:
    2019-08-13

:Examples:
   `parsing/aeson <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/parsing/aeson>`__

`Aeson <http://hackage.haskell.org/package/aeson>`_
is a classic Haskell library from parsing and encoding JSON data.
It is used in many other libraries, notably for web programming.
Aeson documentation includes a 
`nice introduction <hackage.haskell.org/package/aeson/docs/Data-Aeson.html>`__.


Manual implementation
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A very classic workflow is to implement a Haskell data-type then to instantiate
the ``FromJSON`` type-class (for parsing) and the ``ToJSON`` type-class (for
encoding).  This instances implement a mapping between the Haskell data-type
and the JSON data. Then we can call the ``encode`` and ``decode`` functions,
provided by aeson.

.. literalinclude:: example1.hs
    :caption: example1.hs
    :language: haskell

.. code-block:: text

   $ runghc example1.hs
   {"age":42,"theName":"toto"}
   Just (Person {_name = "toto", _age = 42})
   Nothing
   Nothing


Generics
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A more simple workflow is to automatically instantiate ``FromJSON`` and
``ToJSON`` using the language extension ``DeriveGeneric`` and the module
``GHC.Generics``.  Thus, we just have to make our data-type inherit from the
class ``Generic`` and the compiler will automatically use the field names of
the Haskell record for the JSON data.

.. literalinclude:: example2.hs
    :caption: example2.hs
    :language: haskell


.. code-block:: text

   $ runghc example2.hs
   {"_age":42,"_name":"toto"}
   Just (Person {_name = "toto", _age = 42})
   Just (Person {_name = "toto", _age = 42})
   Nothing
   Nothing


Template Haskell
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can also use Template Haskell to implement JSON encoding/decoding.

.. literalinclude:: example3.hs
    :caption: example3.hs
    :language: haskell


.. code-block:: text

   $ runghc example3.hs
   {"_age":42,"_name":"toto"}
   Just (Person {_name = "toto", _age = 42})
   Just (Person {_name = "toto", _age = 42})
   Nothing
   Nothing

