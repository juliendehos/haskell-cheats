{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}

import           Data.Aeson 
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BSC
import           GHC.Generics (Generic)

data Person = Person
    { _name :: String
    , _age :: Int
    } deriving (Generic, Show)

instance FromJSON Person
instance ToJSON Person

myDecode = print . (decode :: BS.ByteString -> Maybe Person)

main = do
    BSC.putStrLn $ encode $ Person "toto" 42
    myDecode "{\"_name\":\"toto\", \"_age\":42}"
    myDecode "{\"_name\":\"toto\", \"_age\":42, \"_x\":7}"
    myDecode "{\"_name\":\"toto\", \"age\":\"42\"}"
        -- parsing failure: "42" is a string
    myDecode "{\"_name\":\"toto\"}"
        -- parsing failure: no age

