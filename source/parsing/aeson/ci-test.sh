#!/bin/sh

ALLHS=$(find . -name "*.hs")
for hs in ${ALLHS} ; do
    nix-shell --run "FOOBAR=foo runghc ${hs} success" || exit 1
done 

