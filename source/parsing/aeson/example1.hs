{-# LANGUAGE OverloadedStrings          #-}

import           Data.Aeson 
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BSC

data Person = Person
    { _name :: String
    , _age :: Int
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "theName"
        <*> v .: "age"

instance ToJSON Person where
    toJSON (Person name age) =
        object ["theName" .= name, "age" .= age]

myDecode = print . (decode :: BS.ByteString -> Maybe Person)

main = do
    BSC.putStrLn $ encode $ Person "toto" 42
    myDecode "{\"theName\":\"toto\", \"age\":42}"
    myDecode "{\"theName\":\"toto\", \"age\":\"42\"}"
        -- parsing failure: "42" is a string
    myDecode "{\"theName\":\"toto\"}"
        -- parsing failure: no age


