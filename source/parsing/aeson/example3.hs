{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

import           Data.Aeson 
import           Data.Aeson.TH
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BSC

data Person = Person
    { _name :: String
    , _age :: Int
    } deriving (Show)

$(deriveJSON defaultOptions ''Person)

myDecode = print . (decode :: BS.ByteString -> Maybe Person)

main = do
    BSC.putStrLn $ encode $ Person "toto" 42
    myDecode "{\"_name\":\"toto\", \"_age\":42}"
    myDecode "{\"_name\":\"toto\", \"_age\":42, \"_x\":7}"
    myDecode "{\"_name\":\"toto\", \"age\":\"42\"}"
        -- parsing failure: "42" is a string
    myDecode "{\"_name\":\"toto\"}"
        -- parsing failure: no age

