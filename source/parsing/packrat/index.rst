
.. _parsing-packrat:

Packrat
===============================================================================

:Date:
    2019-12-19

:Examples:
   `parsing/packrat <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/parsing/packrat>`__


`Packrat <https://bford.info/pub/lang/packrat-icfp02>`__ is a technique for
writing linear-time parsers, for LL(k) or LR(k) grammars.  It is simple,
elegant and easy to implement using a lazy functional programming language like
Haskell.

Principle
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The main idea is to define a data structure that matches the grammar, and to
write functions that parse the grammar rules using this structure, recursively.
Thanks to laziness, an input is evaluated till a matching expression is found.
Thus, we automatically have an infinite look ahead parser, with memoization,
and we don't have to explicitely implement backtraking.

Example
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Presentation
-------------------------------------------------------------------------------

Let's say we want to implement a basic calculator, with addition,
multiplication and parenthesis:

.. code-block:: text

    $ cabal run arith "(1+2)*3"
    Up to date
    (1+2)*3
     -> Just (ExprMul (ExprAdd (ExprVal 1) (ExprVal 2)) (ExprVal 3))
     -> 9

We can use the following grammar:

.. code-block:: text

    Additive  := Multitive ‘+’ Additive | Multitive
    Multitive := Primary ‘*’ Multitive | Primary
    Primary   := ‘(’ Additive ‘)’ | Decimal
    Decimal   := Int

    Int       := '-' Digits | Digits
    Digits    := Digit | Digit Digits
    Digit     := 0 | 1 | ... | 9

And the following AST:

.. literalinclude:: arith-packrat/src/Syntax.hs
   :caption: arith-packrat/src/Syntax.hs
   :language: haskell
   :lines: 3-7

Packrat parser
-------------------------------------------------------------------------------

To implement our packrat parser, we define the following data structures and
functions:

.. literalinclude:: arith-packrat/src/Parser.hs
   :caption: arith-packrat/src/Parser.hs
   :language: haskell
   :lines: 11-54

``Derivs`` is the main data structure, that matches the grammar. The ``parse``
function recursively and lazily builds a value of ``Derivs``. Thus, to parse
some input data, we just have to ``parse`` for the entry rule of our grammar
(here ``dvAdditive``).

We still have implement the various functions that parses the grammar rules
(``pAdditive``, ``pMultitive``...). But to simplify the implementation, we can
define a monadic parser type.

.. literalinclude:: arith-packrat/src/Parser.hs
   :caption: arith-packrat/src/Parser.hs
   :language: haskell
   :lines: 120-154

Finally, we can implement the parsing functions easily, using monadic style (or
applicative style).

.. literalinclude:: arith-packrat/src/Parser.hs
   :caption: arith-packrat/src/Parser.hs
   :language: haskell
   :lines: 56-114

