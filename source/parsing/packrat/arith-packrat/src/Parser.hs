module Parser where

import Data.Char

import Syntax

-------------------------------------------------------------------------------
-- Result, Derivs, parse
-------------------------------------------------------------------------------

data Result v 
    = Parsed 
        { parsedValue :: v
        , parsedDv :: Derivs
        }
    | NoParse
    deriving (Eq, Show)

data Derivs = Derivs 
    -- grammar
    { dvAdditive :: Result Expr
    , dvMultitive :: Result Expr
    , dvPrimary :: Result Expr
    , dvDecimal :: Result Expr
    -- tokens
    , dvInt :: Result Int
    , dvDigits :: Result String
    , dvDigit :: Result Char
    -- raw input
    , dvChar :: Result Char
    } deriving (Eq, Show)

parse :: String -> Derivs
parse s = d where
    d = Derivs 
        -- grammer
        { dvAdditive = pAdditive d
        , dvMultitive = pMultitive d
        , dvPrimary = pPrimary d
        , dvDecimal = pDecimal d
        -- tokens
        , dvInt = pInt d
        , dvDigits = pDigits d
        , dvDigit = pDigit d
        -- raw input
        , dvChar = case s of
            (x:xs) -> Parsed x (parse xs)
            [] -> NoParse
        }

parseExpr :: String -> Maybe Expr
parseExpr str = case dvAdditive (parse str) of
    Parsed v _rem -> Just v
    _ -> Nothing

-------------------------------------------------------------------------------
-- tokens
-------------------------------------------------------------------------------

symbol :: Char -> Parser Char
symbol c = do
    c' <- Parser dvChar
    if c' == c then return c else fail []

pDigit :: Derivs -> Result Char
Parser pDigit = do
    c <- Parser dvChar
    if isDigit c then return c else fail "isDigit"

pDigits :: Derivs -> Result String
Parser pDigits = do
    c1 <- Parser dvDigit
    s2 <- Parser dvDigits </> return ""
    return $ c1:s2

pInt :: Derivs -> Result Int
Parser pInt = do
    sg <- symbol '-' </> return ' '
    s2 <- Parser dvDigits
    return $ read $ sg:s2

-------------------------------------------------------------------------------
-- grammar
-------------------------------------------------------------------------------

-- Additive  <- Multitive ‘+’ Additive | Multitive
pAdditive :: Derivs -> Result Expr
Parser pAdditive
    =   do e1 <- Parser dvMultitive 
           _ <- symbol '+'
           e2 <- Parser dvAdditive 
           return (ExprAdd e1 e2)
    </> Parser dvMultitive

-- Multitive <- Primary ‘^’ Multitive | Primary
pMultitive :: Derivs -> Result Expr
Parser pMultitive
    =   do e1 <- Parser dvPrimary 
           _ <- symbol '*'
           e2 <- Parser dvMultitive 
           return (ExprMul e1 e2)
    </> Parser dvPrimary

-- Primary   <- ‘(’ Additive ‘)’ | Decimal
pPrimary :: Derivs -> Result Expr
Parser pPrimary
    =   do _ <- symbol '('
           e <- Parser dvAdditive 
           _ <- symbol ')'
           return e
    </> Parser dvDecimal

pDecimal :: Derivs -> Result Expr
Parser pDecimal = ExprVal <$> Parser dvInt

-------------------------------------------------------------------------------
-- Parser
-------------------------------------------------------------------------------

newtype Parser v = Parser (Derivs -> Result v)

instance Functor Parser where

    -- (a -> b) -> f a -> f b
    fmap f1 (Parser p2) = Parser (\d -> post (p2 d))
        where post NoParse = NoParse
              post (Parsed v d') = Parsed (f1 v) d'

instance Applicative Parser where

    -- a -> f a
    pure x = Parser (\d -> Parsed x d)

    -- f (a -> b) -> f a -> f b
    (<*>) (Parser pf1) (Parser p2) = Parser (\d -> post (pf1 d))
        where post NoParse = NoParse
              post (Parsed f1 d1) = case p2 d1 of
                                        NoParse -> NoParse
                                        Parsed v2 d2 -> Parsed (f1 v2) d2

instance Monad Parser where

    -- m a -> (a -> m b) -> m b
    (>>=) (Parser p1) f2 = Parser (\d -> post (p1 d))
        where post NoParse = NoParse
              post (Parsed v d') = let Parser p2 = f2 v in p2 d'

    -- String -> f a
    fail _msg = Parser (\_ -> NoParse)

(</>) :: Parser v -> Parser v -> Parser v
(</>) (Parser p1) (Parser p2) = Parser (\d -> post d (p1 d))
    where post d NoParse = p2 d
          post _ r = r

