module ParserSpec where

import Parser
import Syntax

import Test.Hspec

spec :: Spec
spec = do

    describe "dvDigit" $ do
        it "4" $ parsedValue (dvDigit (parse "4")) `shouldBe` '4'
        it "42" $ parsedValue (dvDigit (parse "42")) `shouldBe` '4'
        it " 2" $ dvDigit (parse " 2") `shouldBe` NoParse

    describe "dvDigits" $ do
        it "4" $ parsedValue (dvDigits (parse "4")) `shouldBe` "4"
        it "42" $ parsedValue (dvDigits (parse "42")) `shouldBe` "42"
        it "42 1" $ parsedValue (dvDigits (parse "42 1")) `shouldBe` "42"
        it "" $ dvDigits (parse "") `shouldBe` NoParse
        it " 2" $ dvDigits (parse " 2") `shouldBe` NoParse
        it "-4" $ dvDigits (parse "-4") `shouldBe` NoParse
        it "-42" $ dvDigits (parse "-42") `shouldBe` NoParse

    describe "dvInt" $ do
        it "4" $ parsedValue (dvInt (parse "4")) `shouldBe` 4
        it "42" $ parsedValue (dvInt (parse "42")) `shouldBe` 42
        it " 2" $ dvInt (parse " 2") `shouldBe` NoParse
        it "-4" $ parsedValue (dvInt (parse "-4")) `shouldBe` (-4)
        it "-42" $ parsedValue (dvInt (parse "-42")) `shouldBe` (-42)

    describe "dvDecimal" $ do
        it "4" $ parsedValue (dvDecimal (parse "4")) `shouldBe` ExprVal 4
        it "42" $ parsedValue (dvDecimal (parse "42")) `shouldBe` ExprVal 42
        it "-4" $ parsedValue (dvDecimal (parse "-4")) `shouldBe` ExprVal (-4)
        it "-42" $ parsedValue (dvDecimal (parse "-42")) `shouldBe` ExprVal (-42)

    describe "dvPrimary" $ do
        it "42" $ parsedValue (dvPrimary (parse "42")) `shouldBe` ExprVal 42
        it "(1+2)" $ parsedValue (dvPrimary (parse "(1+2)")) `shouldBe` 
            ExprAdd (ExprVal 1) (ExprVal 2)

    describe "dvMultitive" $ do
        it "42" $ parsedValue (dvMultitive (parse "42")) `shouldBe` ExprVal 42
        it "21*2" $ parsedValue (dvMultitive (parse "21*2")) `shouldBe` 
            ExprMul (ExprVal 21) (ExprVal 2)

    describe "dvAdditive" $ do
        it "42" $ parsedValue (dvAdditive (parse "42")) `shouldBe` ExprVal 42
        it "21*2" $ parsedValue (dvAdditive (parse "21*2")) `shouldBe` 
            ExprMul (ExprVal 21) (ExprVal 2)
        it "40+2" $ parsedValue (dvAdditive (parse "40+2")) `shouldBe` 
            ExprAdd (ExprVal 40) (ExprVal 2)
        it "40+-2" $ parsedValue (dvAdditive (parse "40+-2")) `shouldBe` 
            ExprAdd (ExprVal 40) (ExprVal (-2))
        it "40+(-2)" $ parsedValue (dvAdditive (parse "40+(-2)")) `shouldBe` 
            ExprAdd (ExprVal 40) (ExprVal (-2))

    describe "parseExpr" $ do
        it "4" $ parseExpr "4" `shouldBe` Just (ExprVal 4)
        it "42" $ parseExpr "42" `shouldBe` Just (ExprVal 42)
        it " 2" $ parseExpr " 2" `shouldBe` Nothing
        it "21*2" $ parseExpr "21*2" `shouldBe` 
            Just (ExprMul (ExprVal 21) (ExprVal 2))
        it "4+3*2" $ parseExpr "4+3*2" `shouldBe` 
            Just (ExprAdd (ExprVal 4) (ExprMul (ExprVal 3) (ExprVal 2)))
        it "(4+3)*2" $ parseExpr "(4+3)*2" `shouldBe` 
            Just (ExprMul (ExprAdd (ExprVal 4) (ExprVal 3)) (ExprVal 2))

