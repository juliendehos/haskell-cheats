
{
module Parser where
import Lexer
import Syntax
}

%name arithParse
%tokentype { Token }
%error { parseError }

%token 
    int           { TokenVal $$ }
    '+'           { TokenAdd }
    '*'           { TokenMul }
    '('           { TokenOB }
    ')'           { TokenCB }

%%

Exp     : Exp1              { Exp1 $1 }

Exp1    : Exp1 '+' Term     { TAdd $1 $3 }
        | Term              { Term $1 }

Term    : Term '*' Factor   { TMul $1 $3 }
        | Factor            { Factor $1 }

Factor  : int               { TVal $1 }
        | '(' Exp ')'       { TBrack $2 }

{
parseError :: [Token] -> a
parseError _ = error "Parse error"

-------------------------------------------------------------------------------
-- parser types
-------------------------------------------------------------------------------

data Exp
    = Exp1 Exp1
    deriving Show

data Exp1 
    = TAdd Exp1 Term 
    | Term Term
    deriving Show

data Term 
    = TMul Term Factor 
    | Factor Factor
    deriving Show

data Factor 
    = TVal Int
    | TBrack Exp
    deriving Show

-------------------------------------------------------------------------------
-- parser types to AST
-------------------------------------------------------------------------------

class ToExpr a where
    toExpr :: a -> Expr

instance ToExpr Exp where
    toExpr (Exp1 a) = toExpr a

instance ToExpr Exp1 where
    toExpr (TAdd a m) = ExprAdd (toExpr a) (toExpr m)
    toExpr (Term m) = toExpr m

instance ToExpr Term where
    toExpr (TMul m p) = ExprMul (toExpr m) (toExpr p)
    toExpr (Factor p) = toExpr p

instance ToExpr Factor where
    toExpr (TVal v) = ExprVal v
    toExpr (TBrack a) = toExpr a
}

