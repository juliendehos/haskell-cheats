{
module Lexer where
}

%wrapper "basic"

$digit = 0-9

tokens :-

    $digit+     { \s -> TokenVal (read s)}
    \-$digit+   { \s -> TokenVal (read s)}
    \+          { const TokenAdd }
    \*          { const TokenMul }
    \(          { const TokenOB }
    \)          { const TokenCB }

{
data Token
    = TokenVal Int
    | TokenAdd
    | TokenMul
    | TokenOB
    | TokenCB
    deriving Show
}

