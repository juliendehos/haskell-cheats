import Eval
import Lexer
import Parser

import System.IO

main :: IO ()
main = do
    putStr "> "
    hFlush stdout
    input <- getLine
    let tokens = alexScanTokens input
        syntax = arithParse tokens
        ast = toExpr syntax
        res = eval ast
    print tokens
    print syntax
    print ast
    print res

