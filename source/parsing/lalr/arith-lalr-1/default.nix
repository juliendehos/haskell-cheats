with import <nixpkgs> {};
let drv = haskellPackages.callCabal2nix "arith" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

