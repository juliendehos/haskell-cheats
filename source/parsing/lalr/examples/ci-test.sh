#!/bin/sh

nix-shell --run "alex Tokens.x" || exit 1
nix-shell --run "happy Parser.y" || exit 1
nix-shell --run "echo '21*2' | runghc -Wall -Wextra Parser.hs" || exit 1

