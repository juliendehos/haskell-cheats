module Eval where

import Syntax

eval :: [(String,Int)] -> Exp -> Int
eval p (Let var e1 e2) = eval ((var, eval p e1): p) e2
eval p (Exp1 e)        = evalExp1 p e
  where
  evalExp1 p' (Plus  e' t) = evalExp1 p' e' + evalTerm p' t
  evalExp1 p' (Minus e' t) = evalExp1 p' e' + evalTerm p' t
  evalExp1 p' (Term  t)    = evalTerm p' t

  evalTerm p' (Times t f) = evalTerm p' t * evalFactor p' f
  evalTerm p' (Div   t f) = evalTerm p' t `div` evalFactor p' f
  evalTerm p' (Factor f)  = evalFactor p' f

  evalFactor _  (Int i)    = i
  evalFactor p' (Var s)    = case lookup s p' of
                             Nothing -> error "free variable"
                             Just i  -> i
  evalFactor p' (Brack e') = eval p' e'

