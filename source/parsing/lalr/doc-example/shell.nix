with import <nixpkgs> {};
(haskellPackages.callCabal2nix "arith" ./. {}).env

# let
#   pkgs = import <nixpkgs> {};
#   ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
#     alex
#     happy
#     array
#   ]);
# in pkgs.mkShell {
#   buildInputs =  [ ghc ];
#   shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
# }

