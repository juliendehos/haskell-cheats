
Parser generators
===============================================================================

:Date:
    2020-01-16

:Examples:
   `parsing/lalr <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/parsing/lalr>`__


Parser generators, such as Lex/Yacc, are very classic tools for writing
compilers. Haskell also has this kind of tools: Alex/Happy.

See also:
    - `alex <https://www.haskell.org/alex/>`_
    - `happy <https://www.haskell.org/happy/>`_


Overview
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Alex/Happy can generate a LALR parser 
(`Look Ahead Left-to-Right parser <https://en.wikipedia.org/wiki/LALR_parser_generator>`_),
based on an analytic grammar. Alex generates the lexical analyzer, that
analyzes an input string for producing a list of tokens. Happy generates the
parser, that  tries to combine the tokens according to the grammar.

Practically, we write a ``.x`` file for the lexer (using a
`regex-like syntax <https://en.wikipedia.org/wiki/Regular_expression>`_)
and a ``.y`` file for the parser (using a
`BNF-like syntax <https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_form>`_).
Then we call two programs (``alex`` and ``happy``) for generating Haskell code from these files.
Finally, we use the generated Haskell code in a classic Haskell project.

In fact, build tools like Cabal can call ``alex`` and ``happy`` automatically.
So we just have to write the ``.x`` file and the ``.y`` file and let the build
tool generate the corresponding Haskell code transparently. 


Example
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let’s implement a basic calculator (see :ref:`parsing-parsec` and
:ref:`parsing-packrat`).

In a ``.x`` file, we write the rules of the lexical analyzer and a datatype
representing the tokens.

.. literalinclude:: arith-lalr/src/Lexer.x
   :caption: arith-lalr/src/Lexer.x
   :language: haskell

In a ``.y`` file, we write the rules of the grammar and the corresponding
datatypes.  We also implement a type class for converting these types to a more
independent representation (AST).

.. literalinclude:: arith-lalr/src/Parser.y
   :caption: arith-lalr/src/Parser.y
   :language: haskell

Cabal/Alex/Happy will generate the Haskell code corresponding to the previous
files. So we can call the generated functions ``alexScanTokens`` and
``arithParse`` in the rest of the project.

.. literalinclude:: arith-lalr/app/Main.hs
   :caption: arith-lalr/app/Main.hs
   :language: haskell


.. code-block:: text

    $ cabal run

    > 21*2
    [TokenVal 21,TokenMul,TokenVal 2]
    Exp1 (Term (TMul (Factor (TVal 21)) (TVal 2)))
    ExprMul (ExprVal 21) (ExprVal 2)
    42

    > (3+-1)*2
    [TokenOB,TokenVal 3,TokenAdd,TokenVal (-1),TokenCB,TokenMul,TokenVal 2]
    Exp1 (Term (TMul (Factor (TBrack (Exp1 (TAdd (Term (Factor (TVal 3))) (Factor (TVal (-1))))))) (TVal 2)))
    ExprMul (ExprAdd (ExprVal 3) (ExprVal (-1))) (ExprVal 2)
    4

    > 


