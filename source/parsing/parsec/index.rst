
.. _parsing-parsec:

Monadic parser combinators
===============================================================================

:Date:
    2020-01-11

:Examples:
   `parsing/parsec <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/parsing/parsec>`__


Monadic parser combinators is a simple and elegant parsing technique.  It is
very classic in Haskell and has many implementations (ReadP, Parsec,
Megaparsec...).

See also:
    - `a paper about Monadic Parser Combinator <https://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf>`_
    - `parser combinators in wikipedia <https://en.wikipedia.org/wiki/Parser_combinator>`_
    - `ReadP documentation <http://hackage.haskell.org/package/base/docs/Text-ParserCombinators-ReadP.html>`_
    - `ReadP in the Haskell wikibook <https://en.wikibooks.org/wiki/Haskell/ParseExps>`_
    - `Megaparsec documentation <https://hackage.haskell.org/package/megaparsec>`_
    - `Beginner's guide to Megaparsec <http://akashagrawal.me/beginners-guide-to-megaparsec/>`_


Overview
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The main idea is to define parsing functions that can be combined using the
monadic style or the applicative style.


A Parser type
-------------------------------------------------------------------------------

A parser is a function that takes a string and returns the parsed value. If the
parsing does not consume all the input string, we can also return the remaining
input string. Finally, if the input string can be parsed in several ways, we
can return all the possible results. Thus, we can define the following type for
representing parsing functions. 

.. literalinclude:: examples/intro1.hs
   :caption: examples/intro1.hs
   :language: haskell
   :lines: 3

For example, the following parser detects a letter.

.. literalinclude:: examples/intro1.hs
   :caption: examples/intro1.hs
   :language: haskell
   :lines: 5-7

Let's try it with some inputs:

.. literalinclude:: examples/intro1.hs
   :caption: examples/intro1.hs
   :language: haskell
   :lines: 9-15

.. code-block:: text

    $ runghc intro1.hs 
    [('f',"oobar")]
    []


Parser combinators
-------------------------------------------------------------------------------

We can also define functions to combine parsers. For example, the following
``myseq`` combinator runs two parser sequentially and returns a tuple
containing the two parsed values. We can thus create a new parser
``twoLettersP`` that reads two letters and returns them in a tuple.

.. literalinclude:: examples/intro2.hs
   :caption: examples/intro2.hs
   :language: haskell
   :lines: 12-16

.. code-block:: text

    *Main> run twoLettersP "foobar"
    [(('f','o'),"obar")]

The following ``mybind`` combinator also runs two parsers but the resulting
parser applies a function on the result. For example, we can read two letters
and build a string with them.

.. literalinclude:: examples/intro2.hs
   :caption: examples/intro2.hs
   :language: haskell
   :lines: 18-25

.. code-block:: text

    *Main> run twoLettersAsStringP  "foobar"
    [("fo","obar")]



Monadic parser combinators
-------------------------------------------------------------------------------

Parser combinators can be implemented as a Monad. In Haskell, this can be done
by instantiating the classes ``Functor``, ``Applicative`` and ``Monad``.  But,
let's use the ReadP module, instead of re-doing that. The ``letterP``
parser can be implemented with:

.. literalinclude:: examples/intro3.hs
   :caption: examples/intro3.hs
   :language: haskell
   :lines: 1-5

ReadP already implements the ``Monad`` class, so we can now use the Haskell
bind operator ``>>=``:

.. literalinclude:: examples/intro3.hs
   :caption: examples/intro3.hs
   :language: haskell
   :lines: 7-11

Alternatively, we can use the do-notation: 

.. literalinclude:: examples/intro3.hs
   :caption: examples/intro3.hs
   :language: haskell
   :lines: 13-17

Or the applicative style:

.. literalinclude:: examples/intro3.hs
   :caption: examples/intro3.hs
   :language: haskell
   :lines: 19-20

Let's try that in ghci:

.. code-block:: text

    *Main> readP_to_S twoLettersAsStringP'' "foobar"
    [("fo","obar")]

    *Main> readP_to_S twoLettersAsStringP'' "42foo"
    []



ReadP
-------------------------------------------------------------------------------

`ReadP <http://hackage.haskell.org/package/base/docs/Text-ParserCombinators-ReadP.html>`_
is a module of the Haskell base library that implements monadic parser
combinators.

For example, for parsing words:

.. literalinclude:: examples/example1-readp.hs
   :caption: examples/example1-readp.hs
   :language: haskell

.. code-block:: text

    $ runghc example1-readp.hs 
    []
    [('f',"oo;bar")]
    [("f","oo;bar"),("fo","o;bar"),("foo",";bar")]

Or, for parsing positive doubles:

.. literalinclude:: examples/example2-readp.hs
   :caption: examples/example2-readp.hs
   :language: haskell

.. code-block:: text

    $ runghc example2-readp.hs 
    [(13.3,"7"),(13.37,"")]
    []


Megaparsec
-------------------------------------------------------------------------------

`Megaparsec <https://hackage.haskell.org/package/megaparsec>`_ is another
implementation of monadic parser combinators. It aims to be efficient:
optimized text types, user-friendly error messages... 

.. literalinclude:: examples/example1-megaparsec.hs
   :caption: examples/example1-megaparsec.hs
   :language: haskell
   :lines: -17

.. literalinclude:: examples/example2-megaparsec.hs
   :caption: examples/example2-megaparsec.hs
   :language: haskell




Example: arith
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we want to implement a basic calculator, with integers, addition,
multiplication and parenthesis:

.. code-block:: text

    $ cabal run arith "(1+2)*3"
    Up to date
    (1+2)*3
     -> Just (ExprMul (ExprAdd (ExprVal 1) (ExprVal 2)) (ExprVal 3))
     -> 9


We can use the following grammar:

.. code-block:: text

    Additive  := Multitive ‘+’ Additive | Multitive
    Multitive := Primary ‘*’ Multitive | Primary
    Primary   := ‘(’ Additive ‘)’ | Decimal
    Decimal   := Int

    Int       := '-' Digits | Digits
    Digits    := Digit | Digit Digits
    Digit     := 0 | 1 | ... | 9

And the following AST:

.. literalinclude:: arith-readp/src/Syntax.hs
   :caption: arith-readp/src/Syntax.hs
   :language: haskell
   :lines: 3-7


Example: arith (ReadP)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To implement the calculator with ReadP, we can first define some basic parsers:

.. literalinclude:: arith-readp/src/Parser.hs
   :caption: arith-readp/src/Parser.hs
   :language: haskell
   :lines: 12-25

Then we can combine these parsers to directly implement the grammar:

.. literalinclude:: arith-readp/src/Parser.hs
   :caption: arith-readp/src/Parser.hs
   :language: haskell
   :lines: 31-52

Finally, we can write the main parsing functions:

.. literalinclude:: arith-readp/src/Parser.hs
   :caption: arith-readp/src/Parser.hs
   :language: haskell
   :lines: 58-65


Example: arith (Megaparsec)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


To implement the calculator with Megaparsec, we can first define some basic
parsers:

.. literalinclude:: arith-megaparsec/src/Parser.hs
   :caption: arith-megaparsec/src/Parser.hs
   :language: haskell
   :lines: 12-25

Then we can combine these parsers to directly implement the grammar:

.. literalinclude:: arith-megaparsec/src/Parser.hs
   :caption: arith-megaparsec/src/Parser.hs
   :language: haskell
   :lines: 31-52

Finally, we can write the main parsing functions:

.. literalinclude:: arith-megaparsec/src/Parser.hs
   :caption: arith-megaparsec/src/Parser.hs
   :language: haskell
   :lines: 58-65


However, Megaparsec has many other functions that could simplify the previous
implementation. 

