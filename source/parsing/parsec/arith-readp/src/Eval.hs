module Eval where

import Syntax

eval :: Expr -> Int
eval e = case e of
    (ExprVal v) -> v
    (ExprAdd e1 e2) -> eval e1 + eval e2
    (ExprMul e1 e2) -> eval e1 * eval e2

