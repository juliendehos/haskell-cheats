module Parser where

import Text.ParserCombinators.ReadP
import Data.Char

import Syntax

-------------------------------------------------------------------------------
-- tokens
-------------------------------------------------------------------------------

symbolP :: Char -> ReadP Char
symbolP c = satisfy (== c)

digitP :: ReadP Char
digitP = satisfy isDigit

digitsP :: ReadP String
digitsP = many1 digitP

intP :: ReadP Int
intP = do
    sg <- symbolP '-' +++ return ' '
    s2 <- digitsP
    return $ read $ sg:s2

-------------------------------------------------------------------------------
-- grammar
-------------------------------------------------------------------------------

-- Additive  <- Multitive ‘+’ Additive | Multitive
additiveP :: ReadP Expr
additiveP = ma +++ multitiveP
    where ma = do
            e1 <- multitiveP
            _ <- symbolP '+'
            e2 <- additiveP
            return $ ExprAdd e1 e2

-- Multitive <- Primary ‘*’ Multitive | Primary
multitiveP :: ReadP Expr
multitiveP = pm +++ primaryP
    where pm = ExprMul <$> primaryP <*> (symbolP '*' *> multitiveP)

-- Primary   <- ‘(’ Additive ‘)’ | Decimal
primaryP :: ReadP Expr
primaryP = choice [ additive, decimalP ]
    where additive = between (symbolP '(') (symbolP ')') additiveP

-- Decimal
decimalP :: ReadP Expr
decimalP = ExprVal <$> intP

-------------------------------------------------------------------------------
-- parse functions
-------------------------------------------------------------------------------

parseMaybe :: ReadP a -> String -> Maybe a
parseMaybe p str =
    case readP_to_S p str of
        [] -> Nothing
        xs -> Just $ fst $ last xs

parseExpr :: String -> Maybe Expr
parseExpr = parseMaybe additiveP

