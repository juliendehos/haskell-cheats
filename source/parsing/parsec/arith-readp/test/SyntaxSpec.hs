module SyntaxSpec where

import Syntax

import Test.Hspec

spec :: Spec
spec = describe "syntax" $ do

    it "1" $ shouldBe
            (ExprAdd (ExprVal 3) (ExprVal 4))
            (ExprAdd (ExprVal 3) (ExprVal 4))

    it "2" $ shouldBe
             (ExprMul (ExprAdd (ExprVal 5) (ExprVal 5))
                      (ExprMul (ExprAdd (ExprVal (-2)) (ExprVal 1))
                               (ExprVal 3)))
             (ExprMul (ExprAdd (ExprVal 5) (ExprVal 5))
                      (ExprMul (ExprAdd (ExprVal (-2)) (ExprVal 1))
                               (ExprVal 3)))


