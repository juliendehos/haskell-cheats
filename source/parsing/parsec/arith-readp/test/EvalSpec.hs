module EvalSpec where

import Eval
import Syntax

import Test.Hspec

spec :: Spec
spec = describe "eval" $ do

    it "1" $ shouldBe
             (eval (ExprAdd (ExprVal 3) (ExprVal 4)))
             7

    it "2" $ shouldBe
             (eval (ExprMul (ExprAdd (ExprVal 5) (ExprVal 5))
                            (ExprMul (ExprAdd (ExprVal (-2)) (ExprVal 1))
                                     (ExprVal 3))))
             (-30)

