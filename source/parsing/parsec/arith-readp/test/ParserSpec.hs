module ParserSpec where

import Parser
import Syntax

import Test.Hspec
import Text.ParserCombinators.ReadP

spec :: Spec
spec = do

    describe "digitP" $ do
        it "4" $ parseMaybe digitP "4"  `shouldBe` Just '4'
        it "42" $ parseMaybe digitP "42"  `shouldBe` Just '4'
        it "a" $ parseMaybe digitP "a"  `shouldBe` Nothing
        it " 42" $ parseMaybe digitP " 42"  `shouldBe` Nothing

    describe "digitsP" $ do
        it "4" $ parseMaybe digitsP "4" `shouldBe` Just "4"
        it "42" $ parseMaybe digitsP "42" `shouldBe` Just "42"
        it "42 1" $ parseMaybe digitsP "42 1" `shouldBe` Just "42"
        it " 2" $ parseMaybe digitsP " 2" `shouldBe` Nothing
        it "-4" $ parseMaybe digitsP "-4" `shouldBe` Nothing
        it "-42" $ parseMaybe digitsP "-42" `shouldBe` Nothing

    describe "intP" $ do
        it "4" $ parseMaybe intP "4" `shouldBe` Just 4
        it "42" $ parseMaybe intP "42" `shouldBe` Just 42
        it " 2" $ parseMaybe intP " 2" `shouldBe` Nothing
        it "-4" $ parseMaybe intP "-4" `shouldBe` Just (-4)
        it "-42" $ parseMaybe intP "-42" `shouldBe` Just (-42)

    describe "decimalP" $ do
        it "4" $ parseMaybe decimalP "4" `shouldBe` Just (ExprVal 4)
        it "42" $ parseMaybe decimalP "42" `shouldBe` Just (ExprVal 42)
        it "-4" $ parseMaybe decimalP "-4" `shouldBe` Just (ExprVal (-4))
        it "-42" $ parseMaybe decimalP "-42" `shouldBe` Just (ExprVal (-42))

    describe "primaryP" $ do
        it "42" $ parseMaybe primaryP "42" `shouldBe` Just (ExprVal 42)
        it "(-12)" $ parseMaybe primaryP "(-12)" `shouldBe` Just (ExprVal (-12))
        it "(1+2)" $ parseMaybe primaryP "(1+2)"
            `shouldBe` Just (ExprAdd (ExprVal 1) (ExprVal 2))

    describe "multitiveP" $ do
        it "42" $ parseMaybe multitiveP "42" `shouldBe` Just (ExprVal 42)
        it "21*2" $ parseMaybe multitiveP "21*2" `shouldBe` 
            Just (ExprMul (ExprVal 21) (ExprVal 2))

    describe "additiveP" $ do
        it "42" $ parseMaybe additiveP "42" `shouldBe` Just (ExprVal 42)
        it "21*2" $ parseMaybe additiveP "21*2" `shouldBe` 
            Just (ExprMul (ExprVal 21) (ExprVal 2))
        it "40+2" $ parseMaybe additiveP "40+2" `shouldBe` 
            Just (ExprAdd (ExprVal 40) (ExprVal 2))
        it "40+-2" $ parseMaybe additiveP "40+-2" `shouldBe` 
            Just (ExprAdd (ExprVal 40) (ExprVal (-2)))
        it "40+(-2)" $ parseMaybe additiveP "40+(-2)" `shouldBe` 
            Just (ExprAdd (ExprVal 40) (ExprVal (-2)))

    describe "parseExpr" $ do
        it "4" $ parseExpr "4" `shouldBe` Just (ExprVal 4)
        it "42" $ parseExpr "42" `shouldBe` Just (ExprVal 42)
        it " 2" $ parseExpr " 2" `shouldBe` Nothing
        it "21*2" $ parseExpr "21*2" `shouldBe` 
            Just (ExprMul (ExprVal 21) (ExprVal 2))
        it "4+3*2" $ parseExpr "4+3*2" `shouldBe` 
            Just (ExprAdd (ExprVal 4) (ExprMul (ExprVal 3) (ExprVal 2)))
        it "(4+3)*2" $ parseExpr "(4+3)*2" `shouldBe` 
            Just (ExprMul (ExprAdd (ExprVal 4) (ExprVal 3)) (ExprVal 2))

