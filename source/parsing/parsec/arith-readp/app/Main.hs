import System.Environment

import Eval
import Parser

run :: String -> IO ()
run str = do
    let result = parseExpr str
    putStrLn str
    putStrLn $ " -> " ++ show result
    case result of
        Nothing -> putStrLn " -> no value"
        Just expr -> putStrLn $ " -> " ++ show (eval expr)

main :: IO ()
main = do
    args <- getArgs
    mapM_ run args

