module Parser where

import Data.Char
import Data.Void
import Text.Megaparsec hiding (parseMaybe)
import Syntax

-------------------------------------------------------------------------------
-- tokens
-------------------------------------------------------------------------------

symbolP :: Char -> Parser Char
symbolP c = satisfy (== c)

digitP :: Parser Char
digitP = satisfy isDigit

digitsP :: Parser String
digitsP = some digitP

intP :: Parser Int
intP = do
    sg <- symbolP '-' <|> return ' '
    s2 <- digitsP
    return $ read $ sg:s2

-------------------------------------------------------------------------------
-- grammar
-------------------------------------------------------------------------------

-- Additive  <- Multitive ‘+’ Additive | Multitive
additiveP :: Parser Expr
additiveP = try ma <|> multitiveP
    where ma = do
            e1 <- multitiveP
            _ <- symbolP '+'
            e2 <- additiveP
            return $ ExprAdd e1 e2

-- Additive  <- Multitive ‘+’ Additive | Multitive
multitiveP :: Parser Expr
multitiveP = try pm <|> primaryP
    where pm = ExprMul <$> primaryP <*> (symbolP '*' *> multitiveP)

-- Multitive <- Primary ‘^’ Multitive | Primary
primaryP :: Parser Expr
primaryP = try additive <|> decimalP
    where additive = between (symbolP '(') (symbolP ')') additiveP

-- Primary   <- ‘(’ Additive ‘)’ | Decimal
decimalP :: Parser Expr
decimalP = ExprVal <$> intP

-------------------------------------------------------------------------------
-- parse functions
-------------------------------------------------------------------------------

parseMaybe :: Parser a -> String -> Maybe a
parseMaybe p s =
    case parse p "" s of
        Left _ -> Nothing
        Right r -> Just r

parseExpr :: String -> Maybe Expr
parseExpr = parseMaybe additiveP

type Parser = Parsec Void String

