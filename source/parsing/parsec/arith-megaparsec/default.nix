with import <nixpkgs> {};
let drv = haskellPackages.callCabal2nix "arith" ./. {};
in if lib.inNixShell then drv.env else drv

