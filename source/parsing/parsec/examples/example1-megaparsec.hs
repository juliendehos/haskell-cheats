import Data.Char
import Data.Void
import Text.Megaparsec

type Parser = Parsec Void String

letterP :: Parser Char
letterP = satisfy isLetter

wordP :: Parser String
wordP = some letterP

main :: IO ()
main = do
    print $ parse letterP "" ";foobar"
    print $ parse letterP "" "foo;bar"
    print $ parse wordP "" "foo;bar"

    print $ run letterP ";foobar"
    print $ run letterP "foo;bar"
    print $ run wordP "foo;bar"

run :: Parser a -> String -> Maybe a
run p s = case parse p "" s of
            Left _ -> Nothing
            Right r -> Just r

