import Data.Char
import Text.ParserCombinators.ReadP

letterP :: ReadP Char
letterP = satisfy isLetter

wordP :: ReadP String
wordP = many1 letterP

main :: IO ()
main = do
    print $ readP_to_S letterP ";foobar"
    print $ readP_to_S letterP "foo;bar"
    print $ readP_to_S wordP "foo;bar"

