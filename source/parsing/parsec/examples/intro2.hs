import Data.Char

type Parser a = String -> [(a,String)]

run :: Parser a -> String -> [(a, String)]
run p s = p s

letterP :: Parser Char
letterP [] = []
letterP (x:xs) = [(x, xs) | isLetter x]

myseq :: Parser a -> Parser b -> Parser (a,b)
p `myseq` q = \inp0 -> [((v,w),inp2) | (v,inp1) <- p inp0, (w,inp2) <- q inp1]

twoLettersP :: Parser (Char, Char)
twoLettersP = letterP `myseq` letterP

mybind :: Parser a -> (a -> Parser b) -> Parser b
p `mybind` f = \inp0 -> concat [f v inp1 | (v,inp1) <- p inp0]

twoLettersAsStringP :: Parser String
twoLettersAsStringP = 
    letterP `mybind` \c1 -> 
        letterP `mybind` \c2 ->
            \inp -> [([c1, c2], inp)]

main :: IO ()
main = do
    print $ run twoLettersP "foobar"
    print $ run twoLettersP "42foo"
    print $ run twoLettersAsStringP "foobar"
    print $ run twoLettersAsStringP "42foo"

