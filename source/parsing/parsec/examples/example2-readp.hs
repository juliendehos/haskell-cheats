import Data.Char
import Text.ParserCombinators.ReadP

digitP :: ReadP Char
digitP = satisfy isDigit

digitsP :: ReadP String
digitsP = many1 digitP

doubleP :: ReadP Double
doubleP = do
    s1 <- digitsP
    _ <- satisfy (== '.')
    s2 <- digitsP
    return $ read $ concat [s1, ".", s2]

main :: IO ()
main = do
    print $ readP_to_S doubleP "13.37"
    print $ readP_to_S doubleP "13.foo7"

