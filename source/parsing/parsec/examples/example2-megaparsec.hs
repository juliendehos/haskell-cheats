import Data.Char
import Data.Void
import Text.Megaparsec

type Parser = Parsec Void String

digitP :: Parser Char
digitP = satisfy isDigit

digitsP :: Parser String
digitsP = some digitP

doubleP :: Parser Double
doubleP = do
    s1 <- digitsP
    _ <- satisfy (== '.')
    s2 <- digitsP
    return $ read $ concat [s1, ".", s2]

main :: IO ()
main = do
    print $ parse doubleP "" "13.37"
    print $ parse doubleP "" "13.foo7"

