import Data.Char
import Text.ParserCombinators.ReadP

letterP :: ReadP Char
letterP = satisfy isLetter

twoLettersAsStringP :: ReadP String
twoLettersAsStringP = 
    letterP >>= \c1 ->
        letterP >>= \c2 ->
            return [c1, c2]

twoLettersAsStringP' :: ReadP String
twoLettersAsStringP' = do
    c1 <- letterP
    c2 <- letterP
    return [c1, c2]

twoLettersAsStringP'' :: ReadP String
twoLettersAsStringP'' = (\c1 c2 -> [c1, c2]) <$> letterP <*> letterP

main :: IO ()
main = do
    print $ readP_to_S twoLettersAsStringP "foobar"
    print $ readP_to_S twoLettersAsStringP "42foo"
    print $ readP_to_S twoLettersAsStringP' "foobar"
    print $ readP_to_S twoLettersAsStringP' "42foo"
    print $ readP_to_S twoLettersAsStringP'' "foobar"
    print $ readP_to_S twoLettersAsStringP'' "42foo"

