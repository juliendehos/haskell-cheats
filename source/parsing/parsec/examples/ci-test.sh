#!/bin/sh

ALLHS=$(find . -name "*.hs")
for hs in ${ALLHS} ; do
    nix-shell --run "runghc -Wall -Wextra ${hs}" || exit 1
done 

