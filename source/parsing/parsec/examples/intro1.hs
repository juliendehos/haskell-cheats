import Data.Char

type Parser a = String -> [(a,String)]

letterP :: Parser Char
letterP [] = []
letterP (x:xs) = [(x, xs) | isLetter x]

run :: Parser a -> String -> [(a, String)]
run p s = p s

main :: IO ()
main = do
    print $ run letterP "foobar"
    print $ run letterP "42foo"

