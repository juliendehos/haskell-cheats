
The Read type-class TODO
===============================================================================

:Date:
    2020-02-10

:Examples:
   `parsing/read <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/parsing/read>`__

TODO

.. literalinclude:: example1.hs
    :caption: example1.hs
    :language: haskell

.. code-block:: text

   $ runghc example1.hs
    b5
    c4
    example1.hs: Prelude.read: no parse

