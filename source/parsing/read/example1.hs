
data ChessPosition = ChessPosition Char Int

instance Show ChessPosition where
    show (ChessPosition c i) = c : show i

instance Read ChessPosition where
    readsPrec _ (c:i0:xs) =
        let i = read [i0]
        in [ (ChessPosition c i, xs) | 1<=i, i<=8, 'a'<=c, c<='g' ]
    readsPrec _ _ = []

main :: IO ()
main = do
    print (ChessPosition 'b' 5)
    print (read "c4" :: ChessPosition)
    print (read "b9" :: ChessPosition)

