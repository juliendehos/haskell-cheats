#!/bin/sh

nix-shell --run "runghc -Wall -Wextra open-file.hs" || exit 1
nix-shell --run "runghc -Wall -Wextra with-file.hs" || exit 1
nix-shell --run "runghc -Wall -Wextra write-read-file.hs" || exit 1

