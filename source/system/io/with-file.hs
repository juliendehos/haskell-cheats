import System.IO

myAct :: Handle -> IO ()
myAct h = do
    putStrLn "**** first line ****"
    hGetLine h >>= putStrLn

main :: IO ()
main = do
    withFile "with-file.hs" ReadMode myAct
    putStrLn "**** the end ****"
