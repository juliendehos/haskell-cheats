myFilename :: FilePath
myFilename = "write-read-file.txt" 

main :: IO ()
main = do
    writeFile myFilename "foo\nbar"
    readFile myFilename >>= putStrLn

