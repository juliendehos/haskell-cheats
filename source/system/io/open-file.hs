import System.IO
main :: IO ()
main = do
    handle <- openFile "open-file.hs" ReadMode
    putStrLn "**** first line ****"
    hGetLine handle >>= putStrLn
    putStrLn "**** remaining contents ****"
    hGetContents handle >>= putStrLn
    putStrLn "**** the end ****"
