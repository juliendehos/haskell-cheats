
System IO
===============================================================================

:Date:
    2019-08-14

:Examples:
   `system/io <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/system/io>`__

Haskell has the usual functions for Input/Output.
See the
`System.IO <http://hackage.haskell.org/package/base/docs/System-IO.html>`_ 
module.


Basic IO
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has functions for reading from standard input or writing to standard
output. Note that ``print = putStrLn . show``. In the following example,
``contents`` is output line-by-line due to lazy evaluation.


.. literalinclude:: basic-io.hs
    :caption: basic-io.hs
    :language: haskell

.. code-block:: text

   $ runghc basic-io.hs

   Enter a line
   foobar
   The line is:foobar
   Enter contents
   foo
   foo
   bar
   bar

   the end


``interact :: (String -> String) -> IO ()`` is useful for testing a
string-processing function. 

.. literalinclude:: interact.hs
    :caption: interact.hs
    :language: haskell

.. code-block:: text

    $ runghc interact.hs 
    foo
    FOO
    bar
    BAR
    the end


Files
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The function ``readFile`` reads all the contents of a file, in a string.  The
function ``writeFile`` write a string to a file.

.. literalinclude:: write-read-file.hs
    :caption: write-read-file.hs
    :language: haskell

.. code-block:: text

   $ runghc write-read-file.hs
   foo
   bar


If we don't want to read the whole file in memory, we have the
functions ``openFile``, ``hClose``, ``hGetContents``, etc.

.. literalinclude:: open-file.hs
    :caption: open-file.hs
    :language: haskell

.. code-block:: text

   $ runghc open-file.hs

   **** first line ****
   import System.IO
   **** remaining contents ****
   main :: IO ()
   main = do
       handle <- openFile "open-file.hs" ReadMode
       putStrLn "**** first line ****"
       hGetLine handle >>= putStrLn
       putStrLn "**** remaining contents ****"
       hGetContents handle >>= putStrLn
       putStrLn "**** the end ****"

   **** the end ****


And, if we want not to forget to close the file, we have ``withFile``.

.. literalinclude:: with-file.hs
    :caption: with-file.hs
    :language: haskell

.. code-block:: text

   $ runghc with-file.hs

   **** first line ****
   import System.IO
   **** the end ****


