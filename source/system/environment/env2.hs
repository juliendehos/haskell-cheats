import System.Environment

main :: IO ()
main = do
    lookupEnv "USER" >>= print
    getEnv "USER" >>= print
    lookupEnv "FOOBAR" >>= print
    getEnv "FOOBAR" >>= print
    putStrLn "the end"

