import System.Environment

main :: IO ()
main = do
    myEnv <- getEnvironment
    mapM_ print $ take 3 myEnv

