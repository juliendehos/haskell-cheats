
System environment
===============================================================================

:Date:
    2019-08-14

:Examples:
   `system/environment <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/system/environment>`__


Haskell has a few basics functions for handling command-line arguments, 
environment variables, etc. See the 
`System.Environment <http://hackage.haskell.org/package/base/docs/System-Environment.html>`_ 
module.


Command-line arguments
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can get the program name with ``getProgName :: IO String`` and the list of
arguments with ``getArgs :: IO [String]``.

.. literalinclude:: cli.hs
    :caption: cli.hs
    :language: haskell

.. code-block:: text

   $ runghc cli.hs foobar 42

   this is cli.hs called with ["foobar","42"]



Environment variables
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``getEnvironment :: IO [(String, String)]`` returns the list of environment
variables, with their value.

.. literalinclude:: env1.hs
    :caption: env1.hs
    :language: haskell

.. code-block:: text

   $ runghc env1.hs

   ("LD_LIBRARY_PATH","/run/opengl-driver/lib")
   ("LANG","fr_FR.UTF-8")
   ("GDM_LANG","fr_FR.utf8")



Often, we just want to get the value of a variable so it is simpler to use
``getEnv :: String -> IO String``. If the variable doesn't exist, we get an
error so it can be safer to use ``lookupEnv :: String -> IO (Maybe String)``.

.. literalinclude:: env2.hs
    :caption: env2.hs
    :language: haskell

.. code-block:: text

   $ runghc env2.hs

   Just "dehos"
   "dehos"
   Nothing
   env2.hs: FOOBAR: getEnv: does not exist (no environment variable)



Exit codes
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


The `System.Exit <http://hackage.haskell.org/package/base/docs/System-Exit.html>`_
module provides some useful functions for exiting the program.

.. literalinclude:: exit.hs
    :caption: exit.hs
    :language: haskell

.. code-block:: text

   $ runghc exit.hs success ; echo $?
   0

   $ runghc exit.hs failure ; echo $?
   1

   $ runghc exit.hs ; echo $?
   invalid arguments
   1

