import System.Environment
import System.Exit

main :: IO ()
main = do
    args <- getArgs
    _ <- case args of
        ["success"] -> exitSuccess
        ["failure"] -> exitFailure
        _ -> die "invalid arguments"
    putStrLn "the end"

