import System.Environment

main :: IO ()
main = do
    progName <- getProgName
    args <- getArgs
    putStrLn $ "this is " ++ progName ++ " called with " ++ show args

