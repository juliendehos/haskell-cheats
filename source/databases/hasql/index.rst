PostgreSQL driver (hasql)
===============================================================================

:Date:
    2019-12-02

:Examples:
   `databases/hasql <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/databases/hasql>`__

`Hasql <https://hackage.haskell.org/package/hasql>`_ is a PostgreSQL driver
that aims to be efficient and flexible.  It is composed of several libraries,
bringing connection pools, transactions, Template Haskell...  Here, we only
deal with some basic functions of the hasql base library.

Database used in the examples
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

In the following examples, we use the same database as the one we
use in the :ref:`database-simple` section.

.. literalinclude:: music/music.sql
   :caption: music/music.sql
   :language: sql


Example 0: basic usage
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Basic usage of hasql is straightforward:

  - connect to the database

  - run a session of SQL queries (called "statements")

  - handle errors and return values using the ``Either`` data type

.. literalinclude:: music/app0.hs
   :caption: music/app0.hs
   :language: haskell


Example 1: helper functions
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The basic usage of hasql is quite verbose. It is encouraged to write sessions
and statements in separate files.  Moreover, we can write some helper functions
to run our sessions more easily.

.. literalinclude:: music/MyDb.hs
   :caption: music/MyDb.hs
   :language: haskell
   :lines: 9-24

.. literalinclude:: music/app1.hs
   :caption: music/app1.hs
   :language: haskell
   :lines: 10-13


Example 2: results
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Hasql has several functions to retrieve query results.  ``rowMaybe`` retrieves
a ``Maybe`` (no result or one result).

.. literalinclude:: music/app2.hs
   :caption: music/app2.hs
   :language: haskell
   :lines: 24-31

``rowVector`` retrieves a ``Vector`` (none, one or many results).

.. literalinclude:: music/app2.hs
   :caption: music/app2.hs
   :language: haskell
   :lines: 37-44

Applicative style is well suited for building more complex data from results.

.. literalinclude:: music/app2.hs
   :caption: music/app2.hs
   :language: haskell
   :lines: 50-62

As well as ADT.

.. literalinclude:: music/app2.hs
   :caption: music/app2.hs
   :language: haskell
   :lines: 68-87


Example 3: parameters
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can inject a parameter in a query.

.. literalinclude:: music/app3.hs
   :caption: music/app3.hs
   :language: haskell
   :lines: 26-33

If we have several parameters, we can use ``contrazip``.

.. literalinclude:: music/app3.hs
   :caption: music/app3.hs
   :language: haskell
   :lines: 39-50

Or ``contramap`` (aka ``>$<``). 

.. literalinclude:: music/app3.hs
   :caption: music/app3.hs
   :language: haskell
   :lines: 56-67

Example 4: Template Haskell
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

With the `hasql-th <https://github.com/nikita-volkov/hasql-th>`_ extension, we
can write our queries using a SQL-like syntax.

.. literalinclude:: music/app4.hs
   :caption: music/app4.hs
   :language: haskell
   :lines: 12-19

These queries can be used as usual.

.. literalinclude:: music/app4.hs
   :caption: music/app4.hs
   :language: haskell
   :lines: 21-22

Or using `profunctors <https://hackage.haskell.org/package/profunctors>`_ (``dimap``).

.. literalinclude:: music/app4.hs
   :caption: music/app4.hs
   :language: haskell
   :lines: 24-27

