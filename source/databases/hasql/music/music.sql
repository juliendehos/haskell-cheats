CREATE TABLE artists (
  id INTEGER PRIMARY KEY, 
  name TEXT
);

CREATE TABLE titles (
  id INTEGER PRIMARY KEY, 
  artist INTEGER, 
  name TEXT, 
  FOREIGN KEY(artist) REFERENCES artists(id)
);

INSERT INTO artists VALUES(0, 'Radiohead');
INSERT INTO artists VALUES(1, 'Rage against the machine');
INSERT INTO artists VALUES(2, 'Ibrahim Maalouf');

INSERT INTO titles VALUES(0, 0, 'Paranoid android');
INSERT INTO titles VALUES(1, 0, 'Just');
INSERT INTO titles VALUES(2, 1, 'Take the power back');
INSERT INTO titles VALUES(3, 1, 'How I could just kill a man');
INSERT INTO titles VALUES(4, 2, 'La porte bonheur');

CREATE TABLE user_details (
  id INTEGER PRIMARY KEY, 
  name TEXT,
  email TEXT,
  phone TEXT
);

INSERT INTO user_details VALUES(1, 'toto', 'toto@nimpecom', '18');
INSERT INTO user_details VALUES(2, 'tata', 'tata@nimpecom', '');
INSERT INTO user_details (id, name, email) VALUES(3, 'tutu', 'tattutumpecom');

