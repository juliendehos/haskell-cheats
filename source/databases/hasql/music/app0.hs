{-# LANGUAGE OverloadedStrings #-} 

import Control.Monad.IO.Class (liftIO)
import Hasql.Connection as Connection
import Hasql.Decoders as HD
import Hasql.Encoders as HE
import Hasql.Session as Session
import Hasql.Statement as Statement

mySettings :: Settings
mySettings = Connection.settings "localhost" 5432 "music" "toto" "music"

updateArtist1 :: Session ()
updateArtist1 = statement () stUpdateArtist1

stUpdateArtist1 :: Statement () ()
stUpdateArtist1 = Statement req encoder decoder True where 
    req = "UPDATE artists SET name='Rage Against The Machine' WHERE id=1 ; "
    encoder = HE.noParams
    decoder = HD.noResult

main :: IO ()
main = do
    connE <- Connection.acquire mySettings
    case connE of
        Left e -> fail $ show e
        Right conn -> do
            resE <- liftIO $ Session.run updateArtist1 conn
            case resE of
                Left e -> fail (show e)
                Right res -> print res

