{-# LANGUAGE OverloadedStrings #-} 

import qualified Data.Text as T
import qualified Data.Vector as V
import Hasql.Decoders as HD
import Hasql.Encoders as HE
import Hasql.Session as Session
import Hasql.Statement as Statement

import MyDb

main :: IO ()
main = do
    conn <- connectOrFail
    putStrLn "\nartist 1:" >> runOrFail conn getArtist1 >>= print
    putStrLn "\nall artists:" >> runOrFail conn getAllArtists >>= mapM_ print
    putStrLn "\nall titles:" >> runOrFail conn getAllTitles >>= mapM_ print
    putStrLn "\nall titles adt:" >> runOrFail conn getAllTitlesAdt >>= mapM_ print

-------------------------------------------------------------------------------
-- artist 1
-------------------------------------------------------------------------------

getArtist1 :: Session (Maybe T.Text)
getArtist1 = statement () stGetArtist1

stGetArtist1 :: Statement () (Maybe T.Text)
stGetArtist1 = Statement req encoder decoder True where 
    req = "SELECT name FROM artists WHERE id=1;"
    encoder = HE.noParams
    decoder = HD.rowMaybe (HD.column (HD.nonNullable HD.text))

-------------------------------------------------------------------------------
-- all artists
-------------------------------------------------------------------------------

getAllArtists :: Session (V.Vector T.Text)
getAllArtists = statement () stGetAllArtists

stGetAllArtists :: Statement () (V.Vector T.Text)
stGetAllArtists = Statement req encoder decoder True where 
    req = "SELECT name FROM artists ORDER BY name;"
    encoder = HE.noParams
    decoder = HD.rowVector (HD.column (HD.nonNullable HD.text))

-------------------------------------------------------------------------------
-- all titles
-------------------------------------------------------------------------------

getAllTitles :: Session (V.Vector (T.Text, T.Text))
getAllTitles = statement () stGetAllTitles

stGetAllTitles :: Statement () (V.Vector (T.Text, T.Text))
stGetAllTitles = Statement req encoder decoder True where 
    req = " SELECT artists.name, titles.name \
          \ FROM titles \
          \ INNER JOIN artists \
          \ ON titles.artist = artists.id ;"
    encoder = HE.noParams
    decoder = HD.rowVector 
                ((,) <$> HD.column (HD.nonNullable HD.text)
                     <*> HD.column (HD.nonNullable HD.text))

-------------------------------------------------------------------------------
-- all titles adt
-------------------------------------------------------------------------------

data Title = Title
    { name :: T.Text
    , artist :: T.Text
    } deriving Show

titleRow :: HD.Row Title
titleRow = Title <$> HD.column (HD.nonNullable HD.text)
                 <*> HD.column (HD.nonNullable HD.text)

getAllTitlesAdt :: Session (V.Vector Title)
getAllTitlesAdt = statement () stGetAllTitlesAdt

stGetAllTitlesAdt :: Statement () (V.Vector Title)
stGetAllTitlesAdt = Statement req encoder decoder True where 
    req = " SELECT artists.name, titles.name \
          \ FROM titles \
          \ INNER JOIN artists \
          \ ON titles.artist = artists.id ;"
    encoder = HE.noParams
    decoder = HD.rowVector titleRow

