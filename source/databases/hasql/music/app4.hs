{-# LANGUAGE OverloadedStrings, TemplateHaskell, QuasiQuotes #-} 

import MyDb

import Hasql.TH
import Data.Profunctor
import qualified Data.Text as T
import qualified Data.Vector as V
import Hasql.Session as Session
import Hasql.Statement as Statement

stGetArtistsPrefix :: Statement T.Text (V.Vector T.Text)
stGetArtistsPrefix = 
    [vectorStatement|
        SELECT name :: text
        FROM "artists"
        WHERE LOWER(name) LIKE $1 :: text
        ORDER BY name
        |] 

getArtistsPrefix :: Session (V.Vector T.Text)
getArtistsPrefix = statement "ra%" stGetArtistsPrefix 

newtype Artist = Artist { _name :: T.Text } deriving (Show)

getArtistsPrefix' :: Session (V.Vector Artist)
getArtistsPrefix' = statement "ra%" $ dimap id (fmap Artist) stGetArtistsPrefix

main :: IO ()
main = do
    conn <- connectOrFail
    runOrFail conn getArtistsPrefix >>= mapM_ print
    runOrFail conn getArtistsPrefix' >>= mapM_ print

