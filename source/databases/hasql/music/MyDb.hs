{-# LANGUAGE OverloadedStrings #-} 

module MyDb where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Hasql.Session as Session
import Hasql.Connection as Connection

mySettings :: Settings
mySettings = Connection.settings "localhost" 5432 "music" "toto" "music"

connectOrFail :: IO Connection
connectOrFail = do
    connE <- Connection.acquire mySettings
    case connE of
        Left e -> fail $ show e
        Right conn -> return conn

runOrFail :: MonadIO m => Connection -> Session a -> m a
runOrFail conn req = do
    resE <- liftIO $ Session.run req conn
    case resE of
        Left e -> fail (show e)
        Right res -> return res

