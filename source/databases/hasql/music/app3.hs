{-# LANGUAGE OverloadedStrings #-} 

import Contravariant.Extras.Contrazip
import Data.Functor.Contravariant
import Data.Int
import qualified Data.Text as T
import qualified Data.Vector as V
import qualified Hasql.Decoders as HD
import qualified Hasql.Encoders as HE
import Hasql.Session as Session
import Hasql.Statement as Statement

import MyDb

main :: IO ()
main = do
    conn <- connectOrFail
    putStrLn "\nartist:" >> runOrFail conn (getArtistById 1) >>= print
    putStrLn "\nnames1:" >> runOrFail conn (getNames1 3 "rA%") >>= mapM_ print
    putStrLn "\nnames2:" >> runOrFail conn (getNames2 3 "rA%") >>= mapM_ print

-------------------------------------------------------------------------------
-- artist by id
-------------------------------------------------------------------------------

getArtistById :: Int64 -> Session (Maybe T.Text)
getArtistById i = statement i stGetArtistById

stGetArtistById :: Statement Int64 (Maybe T.Text)
stGetArtistById = Statement req encoder decoder True where 
    req = "SELECT name FROM artists WHERE id = $1 ;"
    encoder = HE.param (HE.nonNullable HE.int8)
    decoder = HD.rowMaybe (HD.column (HD.nonNullable HD.text))

-------------------------------------------------------------------------------
--  names (1)
-------------------------------------------------------------------------------

getNames1 :: Int64 -> T.Text -> Session (V.Vector T.Text)
getNames1 nb pattern = statement (nb, pattern) stGetNames1

stGetNames1 :: Statement (Int64, T.Text) (V.Vector T.Text)
stGetNames1 = Statement req encoder decoder True where 
    req = " SELECT name \
          \ FROM artists \
          \ WHERE LOWER(name) LIKE LOWER($2) \
          \ LIMIT $1 ;"
    encoder = contrazip2 (HE.param (HE.nonNullable HE.int8)) 
                         (HE.param (HE.nonNullable HE.text))
    decoder = HD.rowVector (HD.column (HD.nonNullable HD.text))

-------------------------------------------------------------------------------
--  names (2)
-------------------------------------------------------------------------------

getNames2 :: Int64 -> String -> Session (V.Vector String)
getNames2 nb pattern = statement (nb, pattern) stGetNames2

stGetNames2 :: Statement (Int64, String) (V.Vector String)
stGetNames2 = Statement req encoder decoder True where 
    req = " SELECT name \
          \ FROM artists \
          \ WHERE LOWER(name) LIKE LOWER($2) \
          \ LIMIT $1 ;"
    encoder =  (fst >$< HE.param (HE.nonNullable HE.int8)) 
            <> (T.pack . snd >$< HE.param (HE.nonNullable HE.text))
    decoder = HD.rowVector (HD.column (HD.nonNullable (T.unpack <$> HD.text)))

