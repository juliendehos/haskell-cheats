{-# LANGUAGE OverloadedStrings #-} 

import qualified Hasql.Decoders as HD
import qualified Hasql.Encoders as HE
import Hasql.Session as Session
import Hasql.Statement as Statement

import MyDb

main :: IO ()
main = do
    conn <- connectOrFail
    runOrFail conn updateArtist1

updateArtist1 :: Session ()
updateArtist1 = statement () stUpdateArtist1

stUpdateArtist1 :: Statement () ()
stUpdateArtist1 = Statement req encoder decoder True where 
    req = "UPDATE artists SET name='Rage Against The Machine' WHERE id=1 ; "
    encoder = HE.noParams
    decoder = HD.noResult

