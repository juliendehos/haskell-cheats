{-# LANGUAGE OverloadedStrings, TemplateHaskell, QuasiQuotes #-} 

import MyDb

import Hasql.TH
import Data.Int
import Data.Profunctor
import Data.Text
import Data.Tuple.Curry
import Hasql.Session as Session
import Hasql.Statement as Statement

data User = User
    { _name :: Text
    , _email :: Text
    , _phone :: Maybe Text
    } deriving Show

selectUserById :: Int -> Session (Maybe User)
selectUserById userId =
    statement userId $ dimap fromIntegral (uncurryN User <$>) selectUserDetails

selectUserDetails :: Statement Int32 (Maybe (Text, Text, Maybe Text))
selectUserDetails =
  [maybeStatement|
    select name :: text, email :: text, phone :: text?
    from "user_details"
    where id = $1 :: int4
    |]

main :: IO ()
main = do
    conn <- connectOrFail
    runOrFail conn (selectUserById 1) >>= print
    runOrFail conn (selectUserById 2) >>= print
    runOrFail conn (selectUserById 3) >>= print

