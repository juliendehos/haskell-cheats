-- create database:
-- psql -U postgres -f music-pg.sql

-- test database:
-- psql -h localhost -d music -U music
-- SELECT * FROM artists;

-- delete database:
-- sudo -u postgres dropdb music

CREATE DATABASE music;
\connect music

\ir music.sql

DROP ROLE IF EXISTS music;
CREATE ROLE music WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE titles TO music;
GRANT SELECT ON TABLE artists TO music;
GRANT UPDATE ON TABLE artists TO music;
GRANT SELECT ON TABLE user_details TO music;

