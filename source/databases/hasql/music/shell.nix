with import <nixpkgs> {};

mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      contravariant-extras
      hasql-th
      hasql
      text
      tuple
      vector
    ]))
  ];
}

