with import <nixpkgs> {};

mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      postgresql-simple
      sqlite-simple
      text
    ]))
    sqlite
  ];
}

