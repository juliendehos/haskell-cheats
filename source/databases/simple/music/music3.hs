{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import GHC.Generics (Generic)

data Music = Music 
    { _artist :: Text
    , _title :: Text
    } deriving (Generic, Show)

instance FromRow Music where
    fromRow = Music <$> field <*> field 

main :: IO ()
main = do
    conn <- connectPostgreSQL 
                "host='localhost' port=5432 \
                \dbname=music_db user=music_user password='toto'"
    res <- withTransaction conn $ query_ conn 
                "SELECT artists.name, titles.name FROM artists \
                \INNER JOIN titles ON artists.id = titles.artist"
                :: IO [Music]
    mapM_ print res
    close conn

