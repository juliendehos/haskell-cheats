-- create database:
-- psql -U postgres -f music-pg.sql

-- test database:
-- psql "host='localhost' port=5432 dbname=music_db user=music_user"
-- SELECT * FROM artists;

-- delete database:
-- sudo -u postgres dropdb music_db

CREATE DATABASE music_db;
\connect music_db

SELECT current_database();
SELECT current_user;

\ir music.sql

DROP ROLE IF EXISTS music_user;
CREATE ROLE music_user WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE titles TO music_user;
GRANT SELECT ON TABLE artists TO music_user;

