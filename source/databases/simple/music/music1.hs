{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL

main :: IO ()
main = do
    conn <- SQL.open "music.db"

    SQL.execute conn
        "INSERT INTO artists VALUES (?, ?)"
        (3::Int, "Bach" :: T.Text)

    res1 <- SQL.query_ conn
                "SELECT name FROM artists"
                :: IO [SQL.Only T.Text]
    putStrLn "\nartists:"
    mapM_ print res1

    SQL.execute_ conn
        "DELETE FROM artists WHERE id = 3"

    res2 <- SQL.query_ conn 
                "SELECT artists.name, titles.name FROM artists \
                \INNER JOIN titles ON artists.id = titles.artist"
                :: IO [(T.Text, T.Text)]
    putStrLn "\nartists-titles:"
    mapM_ print res2

    let pattern = "%just%" :: T.Text
    res3 <- SQL.query conn
                "SELECT name FROM titles \
                \WHERE LOWER(name) LIKE ?" (SQL.Only pattern)
                :: IO [SQL.Only T.Text]
    putStrLn "\ntitles containing 'just':"
    mapM_ print res3

    SQL.close conn

