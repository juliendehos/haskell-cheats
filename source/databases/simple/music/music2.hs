{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Music = Music 
    { _artist :: Text
    , _title :: Text
    } deriving (Generic, Show)

instance FromRow Music where
    fromRow = Music <$> field <*> field 

main :: IO ()
main = do
    res <- withConnection "music.db" ( \conn -> query_ conn 
        "SELECT artists.name, titles.name FROM artists \
        \INNER JOIN titles ON artists.id = titles.artist"
        :: IO [Music])
    mapM_ print res

