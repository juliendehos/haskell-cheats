
.. _database-simple:


Simple SQL connectors 
===============================================================================

:Date:
    2019-09-26

:Examples:
   `databases/simple <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/databases/simple>`__

Haskell has SQL connectors for 
`Sqlite <https://hackage.haskell.org/package/sqlite-simple>`_,
`Postgresql <https://hackage.haskell.org/package/postgresql-simple>`_,
Mysql...  These libraries are "mid-level" because we still have to write SQL
queries but the libraries handle database connection, value injection, etc.

Usage
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

These SQL connectors enable us to connect to a database, build and run SQL
queries and receive their results.  The data sent/received to/from a SQL query
is type-checked by the compiler but the query itself is not validated at
compile-time.

Main functions for running SQL queries:
   - ``query_`` : the query has no parameters and provides results
   - ``query`` : the query has parameters and provides results
   - ``execute_`` : the query has no parameters and provides no results
   - ``execute`` : the query has parameters and provides no results

These functions should be called in a IO monad. Parameters are injected in the
query string using the token "?". For single-value parameters/results, we have
to use the wrapper ``Only``.


Example 1: sqlite-simple
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We have a simple music database composed of 2 tables:

.. literalinclude:: music/music.sql
   :caption: music/music.sql
   :language: sql

Here is how to run some queries, using sqlite-simple:

.. literalinclude:: music/music1.hs
   :caption: music/music1.hs
   :language: haskell

.. code-block:: haskell

   artists:
   Only {fromOnly = "Radiohead"}
   Only {fromOnly = "Rage against the machine"}
   Only {fromOnly = "Ibrahim Maalouf"}
   Only {fromOnly = "Bach"}

   artists-titles:
   ("Radiohead","Paranoid android")
   ("Radiohead","Just")
   ("Rage against the machine","Take the power back")
   ("Rage against the machine","How I could just kill a man")
   ("Ibrahim Maalouf","La porte bonheur")

   titles containing 'just':
   Only {fromOnly = "Just"}
   Only {fromOnly = "How I could just kill a man"}


Example 2: using ADT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can send/receive data to/from the database more straightfully, by
instanciating ToRow/FromRow for the corresponding data type. 


.. literalinclude:: music/music2.hs
   :caption: music/music2.hs
   :language: haskell

.. code-block:: haskell

   Music {_artist = "Radiohead", _title = "Paranoid android"}
   Music {_artist = "Radiohead", _title = "Just"}
   Music {_artist = "Rage against the machine", _title = "Take the power back"}
   Music {_artist = "Rage against the machine", _title = "How I could just kill a man"}
   Music {_artist = "Ibrahim Maalouf", _title = "La porte bonheur"}


Example 3: postgresql-simple
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Postgresql needs some supplementary initializations:

.. literalinclude:: music/music-pg.sql
   :caption: music/music-pg.sql
   :language: text

The postgresql-simple connector is very similar to sqlite-simple. We may notice
that Postgresql can handle transactions.

.. literalinclude:: music/music3.hs
   :caption: music/music3.hs
   :language: haskell


