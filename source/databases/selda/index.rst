
.. _database-selda:


Selda
===============================================================================

:Date:
    2020-05-21

:Examples:
   `databases/selda <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/databases/selda>`__


Selda is a DSL for SQL databases (Postgresql, Sqlite). It can create, query and
modify a database. And it is type-safe and monadic.

See also:
    - `Selda website <https://selda.link/>`_
    - `Selda documentaion on hackage <https://hackage.haskell.org/package/selda>`_


Define a database and the corresponding datatypes
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we have a database defined in a SQL file:

.. literalinclude:: country/country.sql
    :caption: country/country.sql
    :language: sql

Then, we can define corresponding datatypes:

.. literalinclude:: country/Country.hs
    :caption: country/Country.hs
    :language: haskell


Simple queries
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

With these datatypes, we can write type-safe queries, and use the do-notation.

.. literalinclude:: country/example1.hs
    :caption: country/example1.hs
    :language: haskell


.. code-block:: text

    Country {country_id = 1, country_name = "BRETAGNE"}
    Country {country_id = 2, country_name = "france"}
    Country {country_id = 3, country_name = "Uruguay"}
    "BRETAGNE" :*: 1
    "france" :*: 2
    "Uruguay" :*: 3


Querying multiple tables
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can write product-queries:

.. literalinclude:: country/example2.hs
    :caption: country/example2.hs
    :language: haskell


.. code-block:: text

    City {city_id = 1, city_country = 3, city_name = "Montevideo"} :*: Country {country_id = 3, country_name = "Uruguay"}
    City {city_id = 2, city_country = 1, city_name = "Rennes"} :*: Country {country_id = 1, country_name = "BRETAGNE"}
    City {city_id = 3, city_country = 2, city_name = "Blavozy"} :*: Country {country_id = 2, country_name = "france"}



Creating a database from datatypes
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Finally, we can create the database from the datatypes directly (instead of
using a SQL file):

.. literalinclude:: country/example3.hs
    :caption: country/example3.hs
    :language: haskell

.. code-block:: text

    Country {country_id = 1, country_name = "Uruguay"}

