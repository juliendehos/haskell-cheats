{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

import Country

selectAllData :: SeldaT SQLite IO [City :*: Country]
selectAllData = query $ do
    cities <- select city_table
    countries <- select country_table
    restrict (cities ! #city_country .== countries ! #country_id)
    return (cities :*: countries)

main :: IO ()
main = withSQLite "country.db" selectAllData >>= mapM_ print

