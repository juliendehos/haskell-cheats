#!/bin/sh

rm -f *.db
nix-shell --run "sqlite3 country.db < country.sql" || exit 1

ALLHS=$(find . -name "example*.hs")
for hs in ${ALLHS} ; do
    nix-shell --run "runghc -Wall -Wextra ${hs}" || exit 1
done 

