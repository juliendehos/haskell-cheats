{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda 
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (SQLite, sqliteOpen, seldaClose)

import Country

dbInit :: SeldaT SQLite IO ()
dbInit = do
    createTable country_table
    _ <- tryInsert country_table [ Country def "Uruguay" ]
    createTable city_table
    _ <- tryInsert city_table [ City def (toId 1) "Montevideo" ]
    return ()

main :: IO ()
main = do
    conn <- sqliteOpen "countries2.db"
    runSeldaT dbInit conn
    runSeldaT (query $ select country_table) conn >>= mapM_ print 
    seldaClose conn

