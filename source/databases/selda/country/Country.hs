{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}

module Country where

import Database.Selda

data Country = Country
  { country_id   :: ID Country
  , country_name :: Text
  } deriving (Generic, Show)

instance SqlRow Country

country_table :: Table Country
country_table = table "country" [#country_id :- autoPrimary]

data City = City
  { city_id      :: ID City
  , city_country :: ID Country
  , city_name    :: Text
  } deriving (Generic, Show)

instance SqlRow City

city_table :: Table City
city_table = table "city"
    [ #city_id :- autoPrimary
    , #city_country :- foreignKey country_table #country_id ]

