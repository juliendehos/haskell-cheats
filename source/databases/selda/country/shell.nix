with import <nixpkgs> {};

mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      selda
      selda-sqlite
      text
      text-show
    ]))
    sqlite
  ];
}

