{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

import Country

selectAllCountries :: SeldaT SQLite IO [Country]
selectAllCountries = query $ select country_table

selectAllCountries2 :: SeldaT SQLite IO [Text :*: ID Country]
selectAllCountries2 = query $ do
    c <- select country_table
    return (c ! #country_name :*: c ! #country_id)

main :: IO ()
main = do
    withSQLite "country.db" selectAllCountries >>= mapM_ print
    withSQLite "country.db" selectAllCountries2 >>= mapM_ print

