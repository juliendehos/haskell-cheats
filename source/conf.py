# -*- coding: utf-8 -*-

# http://www.sphinx-doc.org/en/master/usage/configuration.html

project = 'Haskell Cheat Sheet'
copyright = '2020, Julien Dehos.'
author = 'Julien Dehos'
version = '' # The short X.Y version
release = '' # The full version, including alpha/beta/rc tags

source_suffix = '.rst'
master_doc = 'index'
language = None
pygments_style = 'sphinx'
extensions = []

import sphinx_rtd_theme
html_theme = 'sphinx_rtd_theme'
html_last_updated_fmt = '%b %d, %Y'
html_show_sourcelink = False
html_show_date = True
html_show_sphinx = False
html_show_copyright = True
html_favicon = '_static/favicon.ico'
html_logo = '_static/haskell-logo-white.png'
html_static_path = ['_static' ]
html_context = {
    "display_gitlab": True,
    "gitlab_host": "gitlab.com",
    "gitlab_user": "juliendehos",
    "gitlab_repo": 'haskell-cheats',
    "gitlab_version": "master",
    "conf_py_path": "/source/",
}

latex_elements = {
    # 'papersize': 'letterpaper',
    # 'pointsize': '10pt',
    # 'preamble': '',
    # 'figure_align': 'htbp',
}
latex_documents = [
    (master_doc, 'haskell-cheats.tex', 'Haskell Cheat Sheet', 'Julien Dehos', 'manual'),
]
latex_logo = '_static/haskell-logo.png'
latex_show_urls = 'footnote'

extensions += ['sphinx.ext.mathjax']
