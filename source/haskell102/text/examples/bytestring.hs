{-# LANGUAGE OverloadedStrings #-}

import Data.Char (toUpper)
import qualified Data.ByteString.Lazy.Char8 as B

first :: B.ByteString -> Char
first = B.head

vowels :: B.ByteString -> B.ByteString
vowels = B.filter (`B.elem` B.pack "aeiouy")

up :: B.ByteString -> B.ByteString
up = B.map toUpper

main :: IO ()
main = do
    print $ first "foobar"
    print $ vowels "foobar"
    print $ up "foobar"
    print $ B.concat $ B.words " foo bar"

