import Data.Char (toUpper)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as TIO

first :: T.Text -> Char
first = T.head

vowels :: T.Text -> T.Text
vowels = T.filter (`elem` "aeiouy")

up :: T.Text -> T.Text
up = T.map toUpper

main :: IO ()
main = do
    print $ first $ T.pack "foobar"
    TIO.putStrLn $ vowels $ T.pack "foobar"
    TIO.putStrLn $ up $ T.pack "foobar"
    TIO.putStrLn $ T.concat $ T.words $ T.pack " foo bar"

