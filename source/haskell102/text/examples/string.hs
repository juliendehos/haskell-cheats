import Data.Char (toUpper)

first :: String -> Char
first = head

vowels :: String -> String
vowels = filter (`elem` "aeiouy")

up :: String -> String
up = map toUpper

main :: IO ()
main = do
    print $ first "foobar"
    putStrLn $ vowels "foobar"
    putStrLn $ up "foobar"
    putStrLn $ concat $ words " foo bar"

