
Text
===============================================================================

:Date:
    2019-08-08

:Examples:
   `haskell102/text <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell102/text>`__


Overview
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has several implementations of strings. The ``String`` type is very simple 
to use. It's implemented as a list of ``Char`` and can be used like any other list.

.. code-block:: haskell

   Prelude> mystring = concat ["foo", "bar"]

   Prelude> mystring
   "foobar"

   Prelude> :type mystring
   mystring :: [Char]


However, ``String`` is not optimized for speed. Haskell has two packages for
high-performance strings: ``text`` and ``bytestring``. All these
implementations have quite similar functionalities.

.. code-block:: haskell

   Prelude> import qualified Data.Text as T

   Prelude T> mytext = T.concat [T.pack "foo", T.pack "bar"]

   Prelude T> mytext
   "foobar"

   Prelude T> :type mytext
   mytext :: T.Text

Moreover, ``text`` and ``bytestring`` also have "lazy" versions, that can
manipulate large streams of data.

.. code-block:: haskell

   Prelude> import qualified Data.Text.Lazy as L

   Prelude L> mylazytext = L.concat [L.pack "foo", L.pack "bar"]

   Prelude L> mylazytext 
   "foobar"

   Prelude L> :type mylazytext 
   mylazytext :: L.Text

Finally, GHC has a ``OverloadedStrings`` language extension that simplifies
the use of ``text`` and ``bytestring``.

.. code-block:: haskell

   Prelude> import qualified Data.Text.Lazy as L

   Prelude L> :set -XOverloadedStrings 

   Prelude L> mylazy = L.concat ["foo", "bar"]

   Prelude L> :type mylazy 
   mylazy :: L.Text


String
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``String`` is a type synonyme of ``[Char]``.

.. code-block:: haskell

   Prelude> :info String
   type String = [Char] 	-- Defined in ‘GHC.Base’


The "double quote" notation is just syntactic sugar.

.. code-block:: haskell

   Prelude> ['f', 'o', 'o', 'b', 'a', 'r']
   "foobar"


Functions on lists also work for ``String``.

.. code-block:: haskell

   Prelude> head "foobar"
   'f'

   Prelude> (x:xs) = "foobar"

   Prelude> x
   'f'

   Prelude> xs
   "oobar"

   Prelude> filter (`elem` "aeiouy") "foobar"
   "ooa"

Haskell also has ``String``-specific functions 
(see `Data.String <https://hackage.haskell.org/package/base/docs/Data-String.html>`_).

.. code-block:: haskell

   Prelude> words "foo bar"
   ["foo","bar"]

   Prelude> :t words
   words :: String -> [String]

   Prelude> putStrLn "foobar"
   foobar

   Prelude> :t putStrLn
   putStrLn :: String -> IO ()


Text
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The `text <http://hackage.haskell.org/package/text>`_ package provides a
high-performance implementation for unicode text.

``Text`` has many of the functions that ``String`` has.

.. code-block:: haskell

   Prelude> import qualified Data.Text as T

   Prelude T> :set -XOverloadedStrings 

   Prelude T> T.head "foobar"
   'f'

   Prelude T> import Data.Char (toUpper)

   Prelude T Data.Char> T.map toUpper "foobar"
   "FOOBAR"

   Prelude T Data.Char> import qualified Data.Text.IO as TIO

   Prelude T Data.Char TIO> TIO.putStrLn "foobar"
   foobar


If needed, we can use ``pack`` and ``unpack`` to convert data between ``Text``
and ``String`` (``OverloadedStrings`` may also help). 

.. code-block:: haskell

   Prelude> import qualified Data.Text as T

   Prelude T> :t T.pack
   T.pack :: String -> T.Text

   Prelude T> :t T.unpack
   T.unpack :: T.Text -> String


``Text`` also has functions for converting from/to ``ByteString``.

.. code-block:: haskell

   Prelude T> import qualified Data.Text.Encoding as E

   Prelude T E> import qualified Data.ByteString as B

   Prelude T E B> :t E.encodeUtf8
   E.encodeUtf8 :: T.Text -> B.ByteString

   Prelude T E B> :t E.decodeUtf8
   E.decodeUtf8 :: B.ByteString -> T.Text



ByteString
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The `bytestring <http://hackage.haskell.org/package/bytestring>`_ 
package provides a high-performance implementation for binary or 8-bit
character data.

Bytestring has a specific module for handling 8-bit strings, with usual functions.

.. code-block:: haskell

   Prelude> import qualified Data.ByteString.Char8 as C

   Prelude C> bs1 = C.pack "foobar"

   Prelude C> :t bs1
   bs1 :: C.ByteString

   Prelude C> C.head bs1
   'f'

   Prelude C> import Data.Char (toUpper)

   Prelude C Data.Char> C.map toUpper bs1
   "FOOBAR"

   Prelude C Data.Char> C.putStrLn bs1
   foobar


Bytestring can also handle more general data, through the ``Builder`` module.

.. code-block:: haskell

   Prelude> import qualified Data.ByteString.Lazy as LB

   Prelude LB> import qualified Data.ByteString.Builder as B

   Prelude LB B> bs2 = B.toLazyByteString $ B.string8 "foobar"

   Prelude LB B> :t bs2
   bs2 :: LB.ByteString

   Prelude LB B> LB.head bs2
   102

   Prelude LB B> import Data.Char (chr)

   Prelude LB B Data.Char> chr $ fromEnum $ LB.head bs2
   'f'

