import Control.Monad.State
import System.Random

data McpiState = McpiState
    { nwins :: Int
    , nsims :: Int
    , gen :: StdGen
    }

random01 :: StdGen -> (Double, StdGen)
random01 = random

iterMcpi :: State McpiState Double
iterMcpi = do
    McpiState nwins0 nsims0 gen0 <- get
    let (x, genx) = random01 gen0
        (y, geny) = random01 genx
        nwins1 = if x**2 + y**2 < 1 then nwins0 + 1 else nwins0
        nsims1 = nsims0 + 1
    put $ McpiState nwins1 nsims1 geny
    return $ 4.0 * fromIntegral nwins1 / fromIntegral nsims1

run3 :: State McpiState [Double]
run3 = do
    pi1 <- iterMcpi
    pi2 <- iterMcpi
    pi3 <- iterMcpi
    return [pi1, pi2, pi3]

runN :: Int -> State McpiState [Double]
runN n = replicateM n iterMcpi

main :: IO ()
main = do
    state0 <- McpiState 0 0 <$> getStdGen
    print $ evalState run3 state0
    print $ evalState (runN 10) state0

