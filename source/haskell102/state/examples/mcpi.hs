import System.Random

data McpiState = McpiState
    { nwins :: Int
    , nsims :: Int
    , gen :: StdGen
    }

random01 :: StdGen -> (Double, StdGen)
random01 = random

iterMcpi :: McpiState -> (Double, McpiState)
iterMcpi (McpiState nwins0 nsims0 gen0) =
    let (x, genx) = random01 gen0
        (y, geny) = random01 genx
        nwins1 = if x**2 + y**2 < 1 then nwins0 + 1 else nwins0
        nsims1 = nsims0 + 1
        pi' = 4.0 * fromIntegral nwins1 / fromIntegral nsims1
    in (pi', McpiState nwins1 nsims1 geny)

run3 :: McpiState -> ([Double], McpiState)
run3 s0 = ([pi1, pi2, pi3], s3)
    where (pi1, s1) = iterMcpi s0
          (pi2, s2) = iterMcpi s1
          (pi3, s3) = iterMcpi s2

runN :: Int -> McpiState -> ([Double], McpiState)
runN 0 s = ([], s)
runN n s = (pis++[pi2], s2)
    where (pis, s1) = runN (n-1) s
          (pi2, s2) = iterMcpi s1

main :: IO ()
main = do
    state0 <- McpiState 0 0 <$> getStdGen
    print $ fst $ run3 state0
    print $ fst $ runN 10 state0

