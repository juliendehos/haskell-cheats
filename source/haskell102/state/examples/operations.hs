import Control.Monad.State

add :: Int -> State Int ()
add n = modify (+n)

sub :: Int -> State Int ()
sub n = modify (flip (-) n)

manyOperations :: State Int ()
manyOperations = do
    add 1
    sub 3
    add 5

main :: IO ()
main = print $ execState manyOperations 5

