import Control.Monad.State

type MyState = Int

count :: State MyState ()
count = modify (+1)

countAndGet :: State MyState Int
countAndGet = count >> get

countAndGetOld :: State MyState Int
countAndGetOld = do
    s <- get
    count
    return s

main :: IO ()
main = do
    let state0 = 41
    print $ runState countAndGet state0
    print $ runState countAndGetOld state0
    print $ evalState countAndGetOld state0
    print $ execState countAndGetOld state0

