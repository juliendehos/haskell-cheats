import Control.Monad.State

add :: Int -> StateT Int IO ()
add n = StateT $ \s -> do
    print (s+n)
    return ((),s+n)

sub :: Int -> StateT Int IO ()
sub n = do
    s <- get
    lift . print $ s - n
    put $ s - n

manyOperations :: StateT Int IO ()
manyOperations = do
    add 1
    sub 3
    add 5

main :: IO ()
main = evalStateT manyOperations 5

