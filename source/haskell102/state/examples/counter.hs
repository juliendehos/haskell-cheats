type MyState = Int

count :: MyState -> MyState
count s = s + 1

countAndGet :: MyState -> (Int, MyState)
countAndGet s = (s', s')
    where s' = count s

countAndGetOld :: MyState -> (Int, MyState)
countAndGetOld s = (s, s')
    where s' = count s

main :: IO ()
main = do
    let state0 = 41
    print $ countAndGet state0
    print $ countAndGetOld state0
    let (v, s) = countAndGetOld state0
    print v
    print s

