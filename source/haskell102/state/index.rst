
State
===============================================================================

:Date:
    2019-09-25

:Examples:
   `haskell102/state <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell102/state>`__

The State monad enables to implement a stateful program. It stores an updatable
state across computations without having to pass the successive values as
function arguments.

See also:
   - `Haskell wiki <https://wiki.haskell.org/State_Monad>`_
   - `Haskell wikibook <https://en.wikibooks.org/wiki/Haskell/Understanding_monads/State>`_
   - `MTL <http://hackage.haskell.org/package/mtl/docs/Control-Monad-State-Lazy.html>`_
   - `Transformers <http://hackage.haskell.org/package/transformers/docs/Control-Monad-Trans-State-Lazy.html>`_


Motivation & implementation
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Since Haskell is a purely functional language, we can't store a global state in
a global mutable variable. A classic solution is to pass the successive states
using function parameters and return values.

Let's say a computation step is a function from a state of type ``s`` to
another state of type ``s`` while producing a value of type ``a``:

.. code-block:: haskell

   runState :: s -> (a, s)

We can represent such computation steps using a data type:

.. code-block:: haskell

   newtype State s a = State { runState :: s -> (a, s) }

Using this definition, we can run a state, i.e. get the next state and the
resulting value, or just get the next state or the resulting value: 

.. code-block:: haskell

   runState :: State s a -> s -> (a, s)	

   evalState :: State s a -> s -> a	

   execState :: State s a -> s -> s	


Thus, we can store the current state in the monad and update the state
automatically, e.g. get the value of the state, set the value of the state,
apply a function to the state, etc.

.. code-block:: haskell

   get :: State s s

   put :: s -> State s ()

   modify :: (s -> s) -> State s ()



Example 1: counter
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Suppose we want a counter. Here, the state is an ``Int``, e.g. the value of the
counter. We also want some functions, to increment the counter, and get the
new value or the old value of the counter.

.. literalinclude:: examples/counter.hs
   :caption: examples/counter.hs
   :language: haskell

In this implementation, we have to handle the state explicitly, i.e. each
function has to take the old state in its parameters and to return the new
state in its return value.

Using the state monad, we just have to use actions (i.e. monadic functions) and
the state is automatically handled.

.. literalinclude:: examples/counter-state.hs
   :caption: examples/counter-state.hs
   :language: haskell

.. code-block:: text

   $ ./counter.hs

   (42, 42)
   (41, 42)
   41
   42





Example 2: Monte-Carlo Pi
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Here, we want to compute an approximation of :math:`\pi` using the
Monte-Carlo method, i.e. sampling a 2D square and counting the proportion of
samples inside the centered square. The state contains the number of samples
inside the square, the total number of samples and the current random number
generator. A computation step consists in sampling a new 2D point and to
update the estimated value of :math:`\pi`.

.. literalinclude:: examples/mcpi.hs
   :caption: examples/mcpi.hs
   :language: haskell

Here again, we have to handle the successive states manually. For example, we
have to implement the recursive function ``runN`` which call all the
computation steps ``iterMcpi``.

Using a state monad makes the code simpler. In particular, we can repeat the
computation steps more easily using ``replicateM``, a classic function on
monads.

.. literalinclude:: examples/mcpi-state.hs
   :caption: examples/mcpi-state.hs
   :language: haskell


.. code-block:: text

   $ ./mcpi.hs 

   [0.0, 2.0, 2.65]
   [0.0, 2.0, 2.65, 3.0, 3.2, 3.35, 2.85, 2.5, 2.65, 2.8]


Monad transformer (StateT)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can also define ``State`` using a monad transformer ``StateT``:

.. code-block:: haskell

   newtype StateT s m a = StateT { runStateT :: s -> m (a,s) }

   type State s = StateT s Identity

Thus ``get``, ``put`` and ``modify`` become:

.. code-block:: haskell

   get :: Monad m => StateT s m s

   put :: Monad m => s -> StateT s m ()

   modify :: Monad m => (s -> s) -> StateT s m ()

With the monad transformer, we can use (lift) another monad inside the state
monad.

The following example uses ``State`` to store the result of successive computations:

.. literalinclude:: examples/operations.hs
   :caption: examples/operations.hs
   :language: haskell

.. code-block:: text

    $ ./operations.hs 
    8

With ``StateT``, we can use the ``IO`` monad inside the state monad, to print
the successive values of the result:

.. literalinclude:: examples/operationsT.hs
   :caption: examples/operationsT.hs
   :language: haskell

.. code-block:: text

    $ ./operationsT.hs 
    6
    3
    8


