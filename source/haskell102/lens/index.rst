
Lens
===============================================================================

:Date:
    2019-09-26

:Examples:
   `haskell102/lens <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell102/lens>`__

See also:
   - `Lens package <https://hackage.haskell.org/package/lens>`_
   - `Lens tutorial <https://hackage.haskell.org/package/lens-tutorial/docs/Control-Lens-Tutorial.html>`_
   - `Lens page in the wikibook <https://en.wikibooks.org/wiki/Haskell/Lenses_and_functional_references>`_
   - `Optics package <https://hackage.haskell.org/package/optics>`_
   - `A Little Lens Starter Tutorial <https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/a-little-lens-starter-tutorial>`_

Motivation
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Lenses, or functional references, implement a way to access to elements inside
a data type.  It can be convenient for manipulating nested types but it is not
recommended for simple types (since it requires some boilerplate).

Classic usage:
   - define a record data type
   - make lenses
   - use functions/operators on lenses

In the following example, we define a ``Person`` and make lenses for the two
fields: ``name`` and ``age``. Then we use the functions ``view`` (getter),
``set`` (setter) and ``over`` (modifier).

.. literalinclude:: examples/example0.hs
   :caption: examples/example0.hs
   :language: haskell

.. code-block:: haskell

   $ runghc examples/example0.hs 

   42
   Person {_name = "John", _age = 37}
   Person {_name = "John", _age = 44}



Example 1: nested types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Lenses are interesting for nested types, i.e. types in types.

For example, we can define a ``Point`` type and a ``Rectangle`` type that uses
``Point``. If we want to get or modify the field of the points inside a
rectangle, we have to deconstruct and reconstruct the data types.

.. literalinclude:: examples/example1.hs
   :caption: examples/example1.hs
   :language: haskell

With lenses, we can use various functions and operators that simplify the code.

.. literalinclude:: examples/example1-lens.hs
   :caption: examples/example1-lens.hs
   :language: haskell

.. code-block:: haskell

   $ runghc examples/example1-lens.hs 

   Rectangle {_p0 = Point {_x = 1, _y = 2}, _p1 = Point {_x = 4, _y = 3}}
   1
   (1,4)



Example 2: lens and state monad
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Lenses work well with the state monad.

For example, without lenses, we have to deconstruct/reconstruct data.

.. literalinclude:: examples/example2.hs
   :caption: examples/example2.hs
   :language: haskell

With lenses, we can use various operators to "modify" the current data. 

.. literalinclude:: examples/example2-lens.hs
   :caption: examples/example2-lens.hs
   :language: haskell

.. code-block:: haskell

   $ runghc examples/example2-lens.hs 

   Point {_x = 0, _y = 4}


