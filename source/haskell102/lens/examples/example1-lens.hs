{-# LANGUAGE TemplateHaskell #-}

import Control.Lens 

data Point = Point
    { _x :: Int
    , _y :: Int
    } deriving (Show)

data Rectangle = Rectangle
    { _p0 :: Point
    , _p1 :: Point
    } deriving (Show)

makeLenses ''Point
makeLenses ''Rectangle

getXs :: Rectangle -> (Int, Int)
getXs r = (r^.p0.x, r^.p1.x)

main :: IO ()
main = do
    let r1 = Rectangle (Point 1 2) Point { _y = 3, _x = 4 }
    print r1
    print $ r1^.p0.x
    print $ getXs r1

