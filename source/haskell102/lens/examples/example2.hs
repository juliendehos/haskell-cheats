import Control.Monad.State

data Point = Point
    { _x :: Int
    , _y :: Int
    } deriving (Show)

resetX :: State Point ()
resetX = do
    p <- get
    put p { _x = 0 }

mul :: Int -> State Point ()
mul k = do
    Point x y <- get
    put $ Point (x * k) (y * k)

mul2X :: State Point ()
mul2X =  do
    p <- get
    put p { _x = 2 * _x p }

myfunc :: State Point ()
myfunc = do
    resetX
    mul 2
    mul2X

main :: IO ()
main = print $ execState myfunc $ Point 1 2

