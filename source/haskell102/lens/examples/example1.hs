data Point = Point
    { _x :: Int
    , _y :: Int
    } deriving (Show)

data Rectangle = Rectangle
    { _p0 :: Point
    , _p1 :: Point
    } deriving (Show)

getXs :: Rectangle -> (Int, Int)
getXs r = (_x (_p0 r), _x (_p1 r))

main :: IO ()
main = do
    let r1 = Rectangle (Point 1 2) Point { _y = 3, _x = 4 }
    print r1
    print $ _x $ _p0 r1
    print $ getXs r1

