{-# LANGUAGE TemplateHaskell #-}

import Control.Lens 
import Control.Monad.State

data Point = Point
    { _x :: Int
    , _y :: Int
    } deriving (Show)

makeLenses ''Point

resetX :: State Point ()
resetX = x .= 0

mul :: Int -> State Point ()
mul k = do
    x *= k
    y *= k

mul2X :: State Point ()
mul2X = x %= (*2)

myfunc :: State Point ()
myfunc = do
    resetX
    mul 2
    mul2X

main :: IO ()
main = print $ execState myfunc $ Point 1 2

