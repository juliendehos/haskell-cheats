{-# LANGUAGE TemplateHaskell #-}

import Control.Lens 

data Person = Person
    { _name :: String
    , _age :: Int
    } deriving (Show)

makeLenses ''Person

main :: IO ()
main = do
    let p1 = Person "John" 42
    print $ view age p1
    print $ set age 37 p1
    print $ over age (+2) p1

