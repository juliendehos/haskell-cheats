{-# LANGUAGE OverloadedStrings #-}

import Clay
import qualified Data.Text.Lazy.IO as L
import Lucid

myCss :: Css
myCss = do
    h1 ? fontSize (pt 24)
    p ? color red

myHtml :: Html ()
myHtml = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            link_ [rel_ "stylesheet", href_ "foobar.css"]
        body_ $ do
            h1_ "my page"
            p_ "my text"

main :: IO ()
main = do
    renderToFile "foobar.html" myHtml 
    L.writeFile "foobar.css" $ renderWith compact [] myCss

