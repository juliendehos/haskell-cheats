{-# LANGUAGE OverloadedStrings #-}

import Clay
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as L
import Lucid

myCss :: Css
myCss = do
    h1 ? fontSize (pt 24)
    p ? color red

myHtml :: Html ()
myHtml = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ L.toStrict $ renderWith compact [] myCss
        body_ $ do
            h1_ "my page"
            p_ "my text"

main :: IO ()
main = L.putStrLn $ renderText myHtml

