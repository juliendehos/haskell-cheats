{-# LANGUAGE OverloadedStrings #-}

import Clay

main :: IO ()
main = putCss $ do

    (p <> li) |> code ? color "#060"

    ".reveal" ? (h1 <> h2) ? textTransform none

    h2 |> code ? fontSize (pt 24)

    ".reveal" ? pre ? code ? do
        fontSize (pt 18)
        lineHeight (pt 20)

    Clay.div # ".sourceCode" ? margin (em 0.5) 0 0 0

    ".reveal" ? section ? img ? border none 0 black

