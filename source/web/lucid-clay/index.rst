
Generating HTML/CSS (lucid, clay)
===============================================================================

:Date:
    2019-08-15

:Examples:
   `web/lucid-clay <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/web/lucid-clay>`__


Haskell has DSLs (Domain Specific Languages) for generating HTML or CSS code:
we write Haskell code describing the HTML/CSS data we want to generate, then
the compiler checks our code is valid, then the resulting executable generates
the final (hopefully valid) HTML/CSS code.

HTML (lucid)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`lucid <https://hackage.haskell.org/package/lucid>`_ is a Haskell library for
generating HTML. It is a DSL that implements HTML elements:

.. code-block:: haskell

   Prelude> :set -XOverloadedStrings 

   Prelude> import Lucid

   Prelude Lucid> img_ [src_ "foobar.png", width_ "200px"]
   <img width="200px" src="foobar.png">

Lucid is implemented as a monad so we can use the do-notation.

.. literalinclude:: lucid1.hs
    :caption: lucid1.hs
    :language: haskell

.. code-block:: text

   $ runghc lucid1.hs 
   <!DOCTYPE HTML><html><head><meta charset="utf-8"><title>my page</title></head>
   <body><h1>my page</h1><p>toto 42</p></body></html>

We can also reuse HTML elements, write the result to a file...

.. literalinclude:: lucid2.hs
    :caption: lucid2.hs
    :language: haskell

.. code-block:: text

   $ runghc lucid2.hs 

   $ cat my_page.html 
   <!DOCTYPE HTML><html><head><meta charset="utf-8">
   <title>my page</title></head><body><h1>my page</h1>
   <a href="https://www.haskell.org">haskell</a> - 
   <a href="https://www.wikipedia.org">wikipedia</a></body></html>



CSS (clay)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`clay <https://hackage.haskell.org/package/clay>`_
is a Haskell library for generating CSS.


.. literalinclude:: clay1.hs
    :caption: clay1.hs
    :language: haskell

.. code-block:: text

   $ runghc clay1.hs 
   h1
   {
     font-size : 24pt;
   }

   h2
   {
     font-size : 24pt;
     color     : #f0ffff;
   }

   /* Generated with Clay, http://fvisser.nl/clay */


Clay can render minified CSS.

.. literalinclude:: clay2.hs
    :caption: clay2.hs
    :language: haskell

.. code-block:: text

   $ runghc clay2.hs
   h1{font-size:24pt}h2{font-size:24pt;color:#f0ffff}


Clay implements CSS selectors, attributes, combinators...

.. literalinclude:: clay3.hs
    :caption: clay3.hs
    :language: haskell

.. code-block:: text

   $ runghc clay3.hs 

   p > code,
   li > code {
     color : #006600;
   }

   .reveal h1,
   .reveal h2 {
     text-transform : none;
   }

   h2 > code {
     font-size : 24pt;
   }

   .reveal pre code {
     font-size   : 18pt;
     line-height : 20pt;
   }

   div.sourceCode {
     margin : 0.5em 0px 0px 0px;
   }

   .reveal section img {
     border : none 0px #000000;
   }





Combining HTML and CSS
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Clay and lucid can be used to generate a self-contained HTML/CSS document.

.. literalinclude:: lucid-clay1.hs
    :caption: lucid-clay1.hs
    :language: haskell

.. code-block:: text

   $ runghc lucid-clay1.hs 
   <!DOCTYPE HTML><html><head><meta charset="utf-8">
   <style>h1{font-size:24pt}p{color:#ff0000}</style></head>
   <body><h1>my page</h1><p>my text</p></body></html>


Clay and lucid can also render classic linked files. 

.. literalinclude:: lucid-clay2.hs
    :caption: lucid-clay2.hs
    :language: haskell

.. code-block:: text

   $ runghc lucid-clay2.hs 

   $ cat foobar.css 
   h1{font-size:24pt}p{color:#ff0000}

   $ cat foobar.html 
   <!DOCTYPE HTML><html><head><meta charset="utf-8">
   <link href="foobar.css" rel="stylesheet"></head>
   <body><h1>my page</h1><p>my text</p></body></html>


