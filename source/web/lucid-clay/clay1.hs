import Clay

main :: IO ()
main = putCss $ do

    h1 ? fontSize (pt 24)

    h2 ? do
        fontSize (pt 24)
        color azure

