{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy.IO as L
import Lucid

main :: IO ()
main = L.putStrLn $ renderText $ do
    doctype_
    html_ $ do
      head_ $ do
          meta_ [charset_ "utf-8"]
          title_ "my page"
      body_ $ do
          h1_ "my page"
          p_ $ toHtml $ "toto " ++ show (42::Int)

