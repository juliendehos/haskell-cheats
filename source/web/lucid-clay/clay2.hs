import Clay
import qualified Data.Text.Lazy.IO as L

main :: IO ()
main = L.putStr $ renderWith compact [] $ do

    h1 ? fontSize (pt 24)

    h2 ? do
        fontSize (pt 24)
        color azure

