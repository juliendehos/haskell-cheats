{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Lucid

myPage :: Html ()
myPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "my page"
        body_ $ do
            h1_ "my page"
            myA "haskell"
            " - "
            myA "wikipedia"

myA :: T.Text -> Html ()
myA domain = a_ [href_ url] (toHtml domain)
    where url = T.concat ["https://www.", domain, ".org"]

main :: IO ()
main = renderToFile "my_page.html" myPage 

