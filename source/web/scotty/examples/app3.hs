{-# LANGUAGE OverloadedStrings #-}

import Data.Maybe (maybe)
import Lucid
import Network.Wai.Middleware.Gzip (def, gzip, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Web.Scotty

main :: IO ()
main = do
    port <- maybe 3000 read <$> lookupEnv "PORT"
    scotty port $ do
        middleware logStdoutDev
        middleware $ gzip $ def { gzipFiles = GzipCompress }
        middleware $ staticPolicy $ addBase "static"

        get "/" $ html $ renderText $ do
            doctype_
            html_ $ body_ $ do
                h1_ "app3"
                p_  $ img_ [src_ "/bob.png"]

