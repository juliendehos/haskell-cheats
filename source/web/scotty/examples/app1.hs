{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

main :: IO ()
main = scotty 3000 $ do

    get "/" $ text "This is /"

    get "/about" $ text "This is /about"

    get "/api/:word" $ do
        w <- param "word"
        text $ "This is /api with param " <> w

