{-# LANGUAGE OverloadedStrings #-}

import Data.Text.Lazy
import Lucid
import Web.Scotty

main :: IO ()
main = scotty 3000 $ do

    get "/" $ html $ renderPage $ do
        h1_ "this is app2"
        p_ $ a_ [href_ "/page1"] "go to app2/page1"

    get "/page1" $ html $ renderPage $ do
        h1_ "this is app2/page1"
        p_ $ a_ [href_ "/"] "back to app2"

renderPage :: Html () -> Text
renderPage myBody = renderText $ do
    doctype_
    html_ $ do
      head_ $ do
          meta_ [charset_ "utf-8"]
          title_ "app2"
      body_ myBody

