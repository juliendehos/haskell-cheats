{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (newMVar, modifyMVar_, readMVar)
import Control.Monad.Trans (liftIO)
import Lucid
import Web.Scotty

main :: IO ()
main = do
    myVar <- liftIO $ newMVar ["foobar" :: String]

    scotty 3000 $ do

        get "/" $ do
            myList <- liftIO $ readMVar myVar
            html $ renderText $ html_ $ body_ $ do
                form_ [action_ "/", method_ "post"] $ do
                    input_ [name_ "txt", value_ "enter text here"]
                    input_ [type_ "submit"]
                ul_ $ mapM_ (li_ . toHtml) myList

        post "/" $ do
            txt <- param "txt"
            liftIO $ modifyMVar_ myVar (\ v -> return $ v ++ txt)
            redirect "/"

