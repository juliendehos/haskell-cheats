{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson (ToJSON)
import Data.Text (Text)
import GHC.Generics
import Web.Scotty

data User = User
    { userId :: Int
    , userName :: Text
    } deriving (Show, Generic)

instance ToJSON User

myUsers =
    [ User 1 "John Doe"
    , User 2 "Franck Einstein"
    ]

main :: IO ()
main = scotty 3000 $ do
    get "/" $ text "Welcome. Please use the api."
    get "/users" $ json myUsers
    get "/names" $ json $ map userName myUsers

