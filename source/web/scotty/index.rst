
Simple web-framework (scotty)
===============================================================================

:Date:
    2020-01-13

:Examples:
   `web/scotty <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/web/scotty>`__


Scotty is a web framework similar to Ruby's Sinatra, Node.js' Express, etc.
Using Scotty, `Warp <http://hackage.haskell.org/package/warp>`_ and `Wai
<http://hackage.haskell.org/package/wai>`_, it is very simple to implement
RESTful web servers. 

See also:
    - `Scotty documentation <https://hackage.haskell.org/package/scotty>`_
    - `Scotty examples <https://github.com/scotty-web/scotty/tree/master/examples>`_

Basic routing
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A Scotty app is roughly a web server with URL routing.

.. literalinclude:: examples/app1.hs
   :caption: examples/app1.hs
   :language: haskell


.. only:: latex

   .. figure:: web-scotty-app1.png
      :width: 11cm

      Web with Scotty: app1.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/web-scotty-app1.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|




Serving HTML data
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can render and serve HTML, for example using Lucid.

.. literalinclude:: examples/app2a.hs
   :caption: examples/app2a.hs
   :language: haskell

.. only:: latex

   .. figure:: web-scotty-app2a.png
      :width: 11cm

      Web with Scotty: app2a.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/web-scotty-app2a.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|



Serving JSON data
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can render and serve JSON, for example using Aeson.

.. literalinclude:: examples/app2b.hs
   :caption: examples/app2b.hs
   :language: haskell


.. only:: latex

   .. figure:: web-scotty-app2b.png
      :width: 11cm

      Web with Scotty: app2b.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/web-scotty-app2b.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|


Middleware integration (logs, gzip, static files...)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can use Scotty with the WAI middleware. For example, for server logs, on-fly
gzip compression, static files...

.. literalinclude:: examples/app3.hs
   :caption: examples/app3.hs
   :language: haskell

.. only:: latex

   .. figure:: web-scotty-app3.png
      :width: 11cm

      Web with Scotty: app3.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/web-scotty-app3.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|



Implementing forms
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Scotty can handle HTTP methods. For example, for implementing forms using the
POST method.

.. literalinclude:: examples/app4.hs
   :caption: examples/app4.hs
   :language: haskell


.. only:: latex

   .. figure:: web-scotty-app4.png
      :width: 11cm

      Web with Scotty: app4.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/web-scotty-app4.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|



