
Websockets
===============================================================================

:Date:
    2020-05-12

:Examples:
   `web/websockets <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/web/websockets>`__

Haskell has some libraries implementing the WebSocket protocol, such as
`websockets <https://hackage.haskell.org/package/websockets>`_.  This latter
provides some datatypes and functions for implementing WebSocket clients and
servers.


Simple echo app
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To implement an echo client, we run a ``ClientApp`` using ``runClient``. The
``ClientApp`` takes a connection and uses this connection to send or receive
data. 

.. literalinclude:: echo/client1.hs
   :caption: echo/client1.hs
   :language: haskell

To implement an echo server, we use ``runServer`` that runs our ``serverApp``
function asynchronously to handle new client connections. When a new client
connection is accepted, we can then send or receive data.

.. literalinclude:: echo/server1.hs
   :caption: echo/server1.hs
   :language: haskell


JSON encoding/decoding
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The ``websockets`` library can be used with other Haskell libraries, such as
``aeson``.  For example, we can define a ``Model`` datatype that can be
encoded to JSON data or decoded from JSON data:

.. literalinclude:: echo/Model.hs
   :caption: echo/Model.hs
   :language: haskell

Our ``Model`` datatype can then be received from a websocket and decoded:

.. literalinclude:: echo/client2.hs
   :caption: echo/client2.hs
   :language: haskell

Or encoded and sent to a websocket:

.. literalinclude:: echo/server2.hs
   :caption: echo/server2.hs
   :language: haskell


