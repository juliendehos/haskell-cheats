{-# LANGUAGE DeriveGeneric #-}

module Model where

import Data.Aeson 
import GHC.Generics (Generic)

data Model = Model
    { _n :: Int
    , _c :: Char
    } deriving (Generic, Show)

instance FromJSON Model
instance ToJSON Model

