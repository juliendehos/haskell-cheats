import           Control.Monad (forever, when)
import qualified Data.ByteString.Char8 as C
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)
import           System.IO (stdout, hFlush)

main :: IO ()
main = WS.runClient "127.0.0.1" 9000 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = forever $ do
    putStr "> "
    hFlush stdout
    msgToSrv <- getLine
    when (null msgToSrv) exitSuccess
    WS.sendTextData conn (C.pack msgToSrv)
    msgFromSrv <- WS.fromDataMessage <$> WS.receiveDataMessage conn
    C.putStrLn msgFromSrv

