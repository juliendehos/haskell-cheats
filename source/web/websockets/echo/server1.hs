import           Control.Monad (forever)
import qualified Data.ByteString.Char8 as C
import qualified Network.WebSockets as WS

main :: IO ()
main = WS.runServer "0.0.0.0" 9000 serverApp

serverApp :: WS.PendingConnection -> IO ()
serverApp pc = do
    conn <- WS.acceptRequest pc
    forever $ handleConn conn

handleConn :: WS.Connection -> IO ()
handleConn conn = do
    msgFromClt <- WS.fromDataMessage <$> WS.receiveDataMessage conn 
    C.putStrLn msgFromClt
    WS.sendTextData conn msgFromClt

