import           Control.Monad (forever, when)
import           Data.Aeson (decode)
import qualified Data.ByteString.Char8 as C
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)
import           System.IO (stdout, hFlush)

import Model

main :: IO ()
main = WS.runClient "127.0.0.1" 9000 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = forever $ do
    putStr "> "
    hFlush stdout
    msgToSrv <- getLine
    when (null msgToSrv) exitSuccess
    WS.sendTextData conn (C.pack msgToSrv)
    m <- decode . WS.fromLazyByteString <$> WS.receiveData conn
    print (m :: Maybe Model)

