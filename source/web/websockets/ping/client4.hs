import           Control.Monad (forever, when)
import           Data.Aeson (decode, encode)
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)
import           System.IO (stdout, hFlush)
import           WsProtocol

main :: IO ()
main = WS.runClient "127.0.0.1" 9000 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = forever $ do
    putStr "> "
    hFlush stdout
    msgToSrv <- getLine
    when (null msgToSrv) exitSuccess
    WS.sendBinaryData conn (WS.toLazyByteString $ encode newMessage) 
    m <- decode . WS.fromLazyByteString <$> WS.receiveData conn
    print (m :: Maybe Message)

