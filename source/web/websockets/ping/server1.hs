import           Control.Monad (forever)
import qualified Data.ByteString as B
import qualified Network.WebSockets as WS

main :: IO ()
main = do
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 serverApp

serverApp :: WS.PendingConnection -> IO ()
serverApp pc = do
    conn <- WS.acceptRequest pc
    putStrLn "new connection"
    forever $ do
        m <- WS.receiveDataMessage conn 
        WS.sendTextData conn (WS.fromDataMessage m :: B.ByteString)

