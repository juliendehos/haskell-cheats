{-# LANGUAGE TemplateHaskell #-}

import           Control.Concurrent (newMVar, MVar, modifyMVar, modifyMVar_,
                    readMVar, forkIO, threadDelay)
import           Control.Exception (finally)
import           Control.Lens 
import           Control.Monad (forever)
import           Data.Aeson (decode, encode)
import qualified Network.WebSockets as WS
import           WsConn
import           WsProtocol

data WsModel = WsModel
    { _msg :: Message
    , _mgr :: WsConnManager WS.Connection
    } deriving Show

makeLenses ''WsModel
makeLenses ''Message

newWsModel :: WsModel
newWsModel = WsModel newMessage newWsConnManager

main :: IO ()
main = do
    var <- newMVar newWsModel
    _ <-forkIO $ loopMgr var
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: MVar WsModel -> WS.PendingConnection -> IO ()
serverApp var pc = do
    conn <- WS.acceptRequest pc
    iConn <- modifyMVar var $ \model0 -> 
        let (i, mgr1) = addConn conn (model0^.mgr)
        in return (mgr .~ mgr1 $ model0, i)
    finally (handleConn var conn) (endConn var iConn)

handleConn :: MVar WsModel -> WS.Connection -> IO ()
handleConn var conn = forever $ do
    m <- decode . WS.fromLazyByteString <$> WS.receiveData conn
    print (m :: Maybe Message)
    model <- readMVar var
    WS.sendBinaryData conn (WS.toLazyByteString $ encode $ model^.msg) 

endConn :: MVar WsModel -> Int -> IO ()
endConn var iConn = modifyMVar_ var $ \ model0 ->
    return $ mgr %~ rmConn iConn $ model0

loopMgr :: MVar WsModel -> IO ()
loopMgr var = forever $ do
    threadDelay 1000000
    model <- modifyMVar var $ \model0 ->
        let model1 = msg . val +~ 1 $ model0
        in return (model1, model1)
    print model

