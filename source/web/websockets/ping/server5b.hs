{-# LANGUAGE TemplateHaskell #-}

import           Control.Concurrent (newMVar, MVar, modifyMVar, modifyMVar_,
                    forkIO, threadDelay)
import           Control.Exception (finally)
import           Control.Lens 
import           Control.Monad (forever, forM_)
import           Data.Aeson (decode, encode)
import qualified Network.WebSockets as WS
import           WsConn
import           WsProtocol

data WsModel = WsModel
    { _msg :: Message
    , _mgr :: WsConnManager WS.Connection
    } deriving Show

makeLenses ''WsModel
makeLenses ''Message
makeLenses ''WsConn
makeLenses ''WsConnManager

newWsModel :: WsModel
newWsModel = WsModel newMessage newWsConnManager

main :: IO ()
main = do
    var <- newMVar newWsModel
    _ <-forkIO $ loopMgr var
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: MVar WsModel -> WS.PendingConnection -> IO ()
serverApp var pc = do
    c <- WS.acceptRequest pc
    iConn <- modifyMVar var $ \model0 -> 
        let (i, mgr1) = addConn c (model0^.mgr)
        in return (mgr .~ mgr1 $ model0, i)
    finally (handleConn c) (endConn var iConn)

handleConn :: WS.Connection -> IO ()
handleConn c = forever $ do
    m <- decode . WS.fromLazyByteString <$> WS.receiveData c
    print (m :: Maybe Message)

endConn :: MVar WsModel -> Int -> IO ()
endConn var iConn = modifyMVar_ var $ \ model0 ->
    return $ mgr %~ rmConn iConn $ model0

loopMgr :: MVar WsModel -> IO ()
loopMgr var = forever $ do
    threadDelay 5000000
    model <- modifyMVar var $ \model0 ->
        let model1 = msg . val +~ 1 $ model0
        in return (model1, model1)
    print model
    forM_ (model^.mgr.wsConns) $ \ wc ->
        WS.sendBinaryData (wc^.conn) (WS.toLazyByteString $ encode $ model^.msg) 

