import           Control.Concurrent (newMVar, MVar, modifyMVar, modifyMVar_,
                    readMVar)
import           Control.Exception (finally)
import           Control.Monad (forever)
import qualified Data.ByteString as B
import qualified Network.WebSockets as WS
import           WsConn

main :: IO ()
main = do
    var <- newMVar newWsConnManager
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: MVar (WsConnManager WS.Connection) -> WS.PendingConnection -> IO ()
serverApp var pc = do
    conn <- WS.acceptRequest pc
    iConn <- modifyMVar var $ \mgr0 -> do
        let (i, mgr1) = addConn conn mgr0
        return (mgr1, i)
    finally (handleConn var conn) (endConn var iConn)

handleConn :: MVar (WsConnManager WS.Connection) -> WS.Connection -> IO ()
handleConn var conn = forever $ do
    m <- WS.receiveDataMessage conn 
    mgr <- readMVar var
    print mgr
    WS.sendTextData conn (WS.fromDataMessage m :: B.ByteString)

endConn :: MVar (WsConnManager WS.Connection) -> Int -> IO ()
endConn var iConn = modifyMVar_ var (return . rmConn iConn)

