import           Control.Concurrent (newMVar, MVar, modifyMVar, modifyMVar_,
                    readMVar, forkIO, threadDelay)
import           Control.Exception (finally)
import           Control.Monad (forever)
import qualified Data.ByteString as B
import qualified Network.WebSockets as WS
import           WsConn

main :: IO ()
main = do
    var <- newMVar newWsConnManager
    _ <-forkIO $ loopMgr var
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: MVar (WsConnManager WS.Connection) -> WS.PendingConnection -> IO ()
serverApp var pc = do
    conn <- WS.acceptRequest pc
    iConn <- modifyMVar var $ \mgr0 ->
        let (i, mgr1) = addConn conn mgr0
        in return (mgr1, i)
    finally (handleConn var conn) (endConn var iConn)

handleConn :: MVar (WsConnManager WS.Connection) -> WS.Connection -> IO ()
handleConn var conn = forever $ do
    m <- WS.receiveDataMessage conn 
    mgr <- readMVar var
    print mgr
    WS.sendTextData conn (WS.fromDataMessage m :: B.ByteString)

endConn :: MVar (WsConnManager WS.Connection) -> Int -> IO ()
endConn var iConn = modifyMVar_ var (return . rmConn iConn)

loopMgr :: MVar (WsConnManager WS.Connection) -> IO ()
loopMgr var = forever $ do
    threadDelay 1000000
    mgr <- readMVar var
    print mgr

