import           Control.Monad (forever, when)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)
import           System.IO (stdout, hFlush)

main :: IO ()
main = WS.runClient "127.0.0.1" 9000 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = forever $ do
    putStr "> "
    hFlush stdout
    msgToSrv <- getLine
    when (null msgToSrv) exitSuccess
    WS.sendTextData conn (BC.pack msgToSrv)
    WS.receiveDataMessage conn >>= BC.putStrLn . WS.fromDataMessage 

