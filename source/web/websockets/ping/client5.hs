import           Control.Concurrent (forkIO)
import           Control.Monad (forever, when)
import           Data.Aeson (decode)
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)
import           WsProtocol

main :: IO ()
main = WS.runClient "127.0.0.1" 9000 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = do

    _ <- forkIO $ forever $ do
        m <- decode . WS.fromLazyByteString <$> WS.receiveData conn
        print (m :: Maybe Message)

    forever $ do
        msgToSrv <- getLine
        when (null msgToSrv) exitSuccess

