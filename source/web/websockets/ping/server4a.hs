import           Control.Concurrent (newMVar, MVar, modifyMVar, modifyMVar_,
                    readMVar, forkIO, threadDelay)
import           Control.Exception (finally)
import           Control.Monad (forever)
import           Data.Aeson (decode, encode)
import qualified Network.WebSockets as WS
import           WsConn
import           WsProtocol

data WsModel = WsModel
    { _msg :: Message
    , _mgr :: WsConnManager WS.Connection
    } deriving Show

newWsModel :: WsModel
newWsModel = WsModel newMessage newWsConnManager

main :: IO ()
main = do
    var <- newMVar newWsModel
    _ <-forkIO $ loopMgr var
    putStrLn "running server..."
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: MVar WsModel -> WS.PendingConnection -> IO ()
serverApp var pc = do
    conn <- WS.acceptRequest pc
    iConn <- modifyMVar var $ \model0 ->
        let (i, mgr1) = addConn conn (_mgr model0)
            model1 = model0 { _mgr = mgr1 }
        in return (model1, i)
    finally (handleConn var conn) (endConn var iConn)

handleConn :: MVar WsModel -> WS.Connection -> IO ()
handleConn var conn = forever $ do
    m <- decode . WS.fromLazyByteString <$> WS.receiveData conn
    print (m :: Maybe Message)
    model <- readMVar var
    WS.sendBinaryData conn (WS.toLazyByteString $ encode $ _msg model) 

endConn :: MVar WsModel -> Int -> IO ()
endConn var iConn = modifyMVar_ var $ \ model0 ->
    return $ model0 { _mgr = rmConn iConn (_mgr model0) }

loopMgr :: MVar WsModel -> IO ()
loopMgr var = forever $ do
    threadDelay 1000000
    model <- modifyMVar var $ \model0 ->
        let msg0 = _msg model0
            msg1 = msg0 { _val = _val msg0 + 1 }
            model1 = model0 { _msg = msg1 }
        in return (model1, model1)
    print model

