{-# LANGUAGE DeriveGeneric #-}

module WsProtocol where

import           Data.Aeson (FromJSON, ToJSON)
import           GHC.Generics (Generic)

newtype Message = Message { _val :: Int }
    deriving (Eq, Show, Generic)

instance ToJSON Message
instance FromJSON Message

newMessage :: Message
newMessage = Message 0

