module WsConn where

data WsConn a = WsConn
    { _id :: Int
    , _conn :: a
    }

instance Eq (WsConn a) where
    c1 == c2 = _id c1 == _id c2

data WsConnManager a = WsConnManager
    { _nextId :: Int
    , _wsConns :: [WsConn a]
    } deriving Eq

instance Show (WsConnManager a) where
    show (WsConnManager i cs) = "WsConnManager " ++ show i ++ " " ++ show (map _id cs)

newWsConnManager :: WsConnManager a
newWsConnManager = WsConnManager 0 []

addConn :: a -> WsConnManager a -> (Int, WsConnManager a)
addConn conn (WsConnManager id0 cs) =
    (id0, WsConnManager (1+id0) (WsConn id0 conn : cs))

rmConn :: Int -> WsConnManager a -> WsConnManager a
rmConn id0 mgr = mgr { _wsConns = filter ((/=) id0 . _id) (_wsConns mgr) }

