
Advanced typing
===============================================================================

.. toctree::
    :maxdepth: 2

    phantom/index
    datakinds/index

    gadts/index
    monad-trans/index

