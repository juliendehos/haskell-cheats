import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Char

checkInput :: String -> Bool
checkInput [] = False
checkInput (x:_) = isUpper x

getName :: MaybeT IO String
getName = do
    input <- lift getLine
    guard (checkInput input)
    return input

welcome :: ReaderT String (MaybeT IO) ()
welcome = do
        prefix <- ask
        lift $ lift $ putStr "name: "
        name <- lift getName
        lift $ lift $ putStrLn $ prefix ++ name

main :: IO ()
main = void $ runMaybeT $ runReaderT welcome "hello "

