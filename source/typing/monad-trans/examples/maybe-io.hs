import Control.Monad
import Data.Char

type IOMaybe a = IO (Maybe a)

returnIOM :: a -> IOMaybe a
returnIOM = return . Just

bindIOM :: IOMaybe a -> (a -> IOMaybe b) -> IOMaybe b
bindIOM iomx f = do
    mx <- iomx
    case mx of
        Nothing -> return Nothing
        Just x -> f x

liftIOM :: IO a -> IOMaybe a
liftIOM iox = iox >>= returnIOM

checkInput :: String -> Bool
checkInput [] = False
checkInput (x:_) = isUpper x

getName :: IOMaybe String
getName = do
    input <- getLine
    if checkInput input
    then returnIOM input
    else return Nothing

example :: IOMaybe ()
example = 
    putStr "name: "
    >> getName
    `bindIOM` (\name -> liftIOM $ putStrLn $ "hello " ++ name)

main :: IO ()
main = void example 

-- https://www.youtube.com/watch?v=UPb83JiGiIY

