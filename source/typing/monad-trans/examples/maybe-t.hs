import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Class
import Data.Char

newtype MaybeT m a = MaybeT { runMaybeT :: m (Maybe a) }

instance Monad m => Functor (MaybeT m) where
    fmap f =  MaybeT . fmap (fmap f) . runMaybeT

instance Monad m => Applicative (MaybeT m) where
    pure = MaybeT . return . Just

    tmf <*> tmx = MaybeT $ do
        mf <- runMaybeT tmf
        case mf of
            Nothing -> return Nothing
            Just f  -> do
                mx <- runMaybeT tmx
                case mx of
                    Nothing -> return Nothing
                    Just x  -> return $ Just $ f x

instance Monad m => Monad (MaybeT m) where
    x >>= f = MaybeT $ do
        v <- runMaybeT x
        case v of
            Nothing -> return Nothing
            Just y -> runMaybeT $ f y

instance (Functor m, Monad m) => Alternative (MaybeT m) where
    empty = MaybeT (return Nothing)

    x <|> y = MaybeT $ do
        v <- runMaybeT x
        case v of
            Nothing -> runMaybeT y
            Just _  -> return v

instance MonadTrans MaybeT where
    lift = MaybeT . fmap Just

checkInput :: String -> Bool
checkInput [] = False
checkInput (x:_) = isUpper x

getName :: MaybeT IO String
getName = do
    input <- lift getLine
    guard (checkInput input)
    return input

example :: IO (Maybe ())
example = runMaybeT $ do
    lift $ putStr "name: "
    name <- getName
    lift $ putStrLn $ "hello " ++ name

main :: IO ()
main = void example 

-- https://www.youtube.com/watch?v=UPb83JiGiIY
-- https://hackage.haskell.org/package/transformers-0.5.6.2/docs/src/Control.Monad.Trans.Maybe.html

