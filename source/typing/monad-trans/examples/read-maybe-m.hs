{-# LANGUAGE FlexibleContexts #-}

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Reader
import Data.Char

checkInput :: String -> Bool
checkInput [] = False
checkInput (x:_) = isUpper x

getName :: MaybeT IO String
getName = do
    input <- lift getLine
    guard (checkInput input)
    return input

welcome :: (MonadReader String m, MonadIO m) => m ()
welcome = do
        prefix <- ask
        liftIO $ putStr "name: "
        nameMay <- liftIO $ runMaybeT getName
        case nameMay of
            Nothing -> return ()
            Just name -> liftIO $ putStrLn $ prefix ++ name

main :: IO ()
main = runReaderT welcome "hello "

