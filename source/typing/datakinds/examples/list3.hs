{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}

data Nat = Zero | Succ Nat

type One = 'Succ 'Zero
type Two = 'Succ One

data List (n::Nat) a where
    End :: List 'Zero a
    Cons :: a -> List n a -> List ('Succ n) a

deriving instance Show a => Show (List n a)
deriving instance Foldable (List n)
deriving instance Functor (List n)
deriving instance Traversable (List n)

type family Add x y where
    Add 'Zero y = y
    Add ('Succ x) y = 'Succ (Add x y)

-------------------------------------------------------------------------------

safeHead :: List ('Succ n) a -> a
safeHead (Cons x _xs) = x

safeTail :: List ('Succ n) a -> List n a
safeTail (Cons _x xs) = xs

append :: List nx a -> List ny a -> List (Add nx ny) a
append End ys = ys
append (Cons x xs) ys = Cons x (append xs ys)

-------------------------------------------------------------------------------

list0 :: List 'Zero Int 
list0 = End

list1 :: List One Int 
list1 = Cons 42 End

list2 :: List Two Int 
list2 = Cons 13 (Cons 37 End)

main :: IO ()
main = do
    print list0
    print list1
    print list2
    print $ safeHead list1
    print $ safeTail list1
    print $ list0 `append` list1
    print $ list1 `append` list0
    print $ list1 `append` list1
    print $ Cons 13 list1
    print $ null list0
    print $ null list1
    print $ length list0
    print $ length list1
    print $ length list2
    print $ sum list2
    print $ sum list2
    print $ fmap (*2) list2
    mapM_ print list2

    -- doesn't compile :
    -- print $ safeHead list0
    -- print $ safeHead $ safeHead list1

-- http://matija.me/2020/07/04/haskell-type-lvl-programming-intro/
-- https://www.parsonsmatt.org/2017/04/26/basic_type_level_programming_in_haskell.html

