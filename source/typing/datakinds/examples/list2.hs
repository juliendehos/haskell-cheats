{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}

data Nat = Zero | Succ Nat

type One = 'Succ 'Zero

data List (a :: * ) (n :: Nat) where
    End :: List a 'Zero
    Cons :: a -> List a n -> List a ('Succ n)

deriving instance Show a => Show (List a n)

list1 :: List Int 'Zero
list1 = End

list2 :: List Int One
list2 = Cons 42 End

safeHead :: List a ('Succ n) -> a
safeHead (Cons x _xs) = x

safeTail :: List a ('Succ n) -> List a n
safeTail (Cons _x xs) = xs

main :: IO ()
main = do
    print list1
    -- print $ safeHead list1  -- doesn't compile
    print list2
    print $ safeHead list2
    print $ safeTail list2

-- http://matija.me/2020/07/04/haskell-type-lvl-programming-intro/

{-
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

type family Add (x :: Nat) (y :: Nat) :: Nat where
    Add 'Zero n = n
    Add ('Succ n) m = Add n ('Succ m)

append :: List a nx -> List a ny -> List a (Add nx ny)
append End ys = ys
append (Cons x xs) ys = Cons x (append xs ys)

-}

