{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}

-- import GHC.TypeLits

data ListStatus = Empty | NonEmpty deriving Show

data List (a :: * ) (empty :: ListStatus) where
    End :: List a 'Empty 
    Cons :: a -> List a empty -> List a 'NonEmpty

deriving instance Show a => Show (List a e)

list1 :: List Int 'Empty
list1 = End

list2 :: List Int 'NonEmpty
list2 = Cons 42 End

safeHead :: List a 'NonEmpty -> a
safeHead (Cons x _xs) = x

safeTail :: List a 'NonEmpty -> List a e -- FIXME
safeTail (Cons _x _xs) = undefined

main :: IO ()
main = do
    print list1
    -- print $ safeHead list1  -- doesn't compile
    print list2
    print $ safeHead list2
    print $ safeTail list2

-- http://matija.me/2020/07/04/haskell-type-lvl-programming-intro/

