{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}

-- https://github.com/HuwCampbell/grenade/blob/master/src/Grenade/Core/Shape.hs

import GHC.TypeLits

data Dim
    = D1 Nat
    | D2 Nat Nat

data Tensor (d :: Dim) where
    Tensor1
        :: ( KnownNat len )
        => d len
        -> Tensor ('D1 len)

    Tensor2
        :: ( KnownNat rows, KnownNat columns )
        => d rows columns
        -> Tensor ('D2 rows columns)


{-
size :: Tensor d -> Int
size (Tensor1 (D1 n)) = 1
size (Tensor2 xs) = 2

dotProduct :: Tensor (D1 n) -> Int
dotProduct _ = 0

vector :: Tensor ('D1 3)
vector = Tensor []
-}

main :: IO ()
main = do
    putStrLn "TODO"

