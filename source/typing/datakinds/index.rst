
.. _typing-datakinds:


Datakinds TODO
===============================================================================

:Date:
    2020-05-27

:Examples:
   `typing/datakinds <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/typing/datakinds>`__



A first example: distance using datakinds
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

see also :ref:`typing-phantom-distance`

.. literalinclude:: examples/distance-datakinds.hs
    :caption: examples/distance-datakinds.hs
    :language: haskell



TODO
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

https://www.parsonsmatt.org/2017/04/26/basic_type_level_programming_in_haskell.html

