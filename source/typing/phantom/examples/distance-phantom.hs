{-# LANGUAGE GeneralizedNewtypeDeriving #-}

newtype Distance a = Distance Double deriving (Num, Show)

data Kilometer
data Mile

marathonDistance :: Distance Kilometer
marathonDistance = Distance 42.195

distanceKmToMiles :: Distance Kilometer -> Distance Mile
distanceKmToMiles (Distance km) = Distance (0.621371 * km)

marathonDistanceInMiles :: Distance Mile
marathonDistanceInMiles = distanceKmToMiles marathonDistance

main :: IO ()
main = do
    print $ marathonDistance + marathonDistance
    print $ marathonDistanceInMiles + distanceKmToMiles marathonDistance

    -- does not compile:
    -- print $ marathonDistanceInMiles + marathonDistance 
    
-- see also: https://stackoverflow.com/a/28250226

