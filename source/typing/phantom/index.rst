
.. _typing-phantom:

Phantom types 
===============================================================================

:Date:
    2020-05-27

:Examples:
   `typing/phantom <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/typing/phantom>`__

.. _typing-phantom-distance:

A first example: distance using phantom types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A phantom type is a polymorphic type which doesn't use the type parameter in
its value. This parameter is only use at type-level.

In the following example, ``Distance a`` is a phantom type which is then used
for representing a distance in kilometer or a distance in mile, and prevents
comparing uncomparable values.

.. literalinclude:: examples/distance-phantom.hs
    :caption: examples/distance-phantom.hs
    :language: haskell


A more detailled example
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we want to encrypt or decrypt a message. If we implement a message in
a ``String``, we cannot distinguish a plain message and an encrypted message.
For example, the compiler doesn't prevent us from encrypting an already
encrypted message.

.. literalinclude:: crypt/crypt-0.hs
    :caption: crypt/crypt-0.hs
    :language: haskell

We can restrict our ``encrypt`` and ``decrypt`` functions to a 
custom datatype ``Msg``, but it still doesn't solve the problem. 

.. literalinclude:: crypt/crypt-1.hs
    :caption: crypt/crypt-1.hs
    :language: haskell

With a phantom type, we can separate ``Msg PlainStr`` from ``Msg CryptStr``.
Thus, the compiler checks whether we ``encrypt`` or ``decrypt`` a valid data.

.. literalinclude:: crypt/crypt-2.hs
    :caption: crypt/crypt-2.hs
    :language: haskell

Instead of the phantom type, we could use two specific datatypes.

.. literalinclude:: crypt/crypt-3.hs
    :caption: crypt/crypt-3.hs
    :language: haskell

However, a phantom type can simplify the implementation of more advanced
features, such as instancing ``Functor``.

.. literalinclude:: crypt/crypt-4.hs
    :caption: crypt/crypt-4.hs
    :language: haskell

