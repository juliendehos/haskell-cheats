import Rotate

newtype Msg a = Msg String deriving (Show)

data CryptStr
data PlainStr

encrypt :: Msg PlainStr -> Msg CryptStr
encrypt (Msg str) = Msg (rotateStr 1 str)

decrypt :: Msg CryptStr -> Msg PlainStr
decrypt (Msg str) = Msg (rotateStr (-1) str)

main :: IO ()
main = do
    let encrypted = encrypt (Msg "foobar") 
        decrypted = decrypt encrypted
    print encrypted
    print decrypted
    -- print $ encrypt encrypted  -- does not compile :)

