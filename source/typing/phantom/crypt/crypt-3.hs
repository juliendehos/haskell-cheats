import Rotate

newtype CryptStr = CryptStr String deriving Show
newtype PlainStr = PlainStr String deriving Show

encrypt :: PlainStr -> CryptStr
encrypt (PlainStr str) = CryptStr (rotateStr 1 str)

decrypt :: CryptStr -> PlainStr
decrypt (CryptStr str) = PlainStr (rotateStr (-1) str)

main :: IO ()
main = do
    let encrypted = encrypt (PlainStr "foobar") 
        decrypted = decrypt encrypted
    print encrypted
    print decrypted
    -- print $ encrypt encrypted  -- does not compile :)

