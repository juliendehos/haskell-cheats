{-# LANGUAGE DeriveFunctor #-}
import Rotate

newtype Msg e s = Msg s deriving (Eq, Functor, Show)

data CryptStr 
data PlainStr

encrypt :: Msg PlainStr String -> Msg CryptStr String
encrypt (Msg str) = Msg (rotateStr 1 str)

decrypt :: Msg CryptStr String -> Msg PlainStr String
decrypt (Msg str) = Msg (rotateStr (-1) str)

main :: IO ()
main = do
    let encrypted = encrypt (Msg "foobar") 
        decrypted = decrypt encrypted
    print encrypted
    print decrypted

    let nenc0 = length <$> encrypted
        ndec0 = length <$> decrypted
    print nenc0
    print ndec0
    print $ nenc0 == nenc0
    -- print $ nenc0 == ndec0  -- does not compile :)

