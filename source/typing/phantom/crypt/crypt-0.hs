import Rotate

encrypt :: String -> String
encrypt str = rotateStr 1 str

decrypt :: String -> String
decrypt str = rotateStr (-1) str

main :: IO ()
main = do
    let encrypted = encrypt "foobar"
        decrypted = decrypt encrypted
    print encrypted
    print decrypted
    print $ encrypt encrypted  -- this should be forbidden !

