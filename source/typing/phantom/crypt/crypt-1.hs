import Rotate

newtype Msg = Msg String deriving Show

encrypt :: Msg -> Msg
encrypt (Msg str) = Msg (rotateStr 1 str)

decrypt :: Msg -> Msg
decrypt (Msg str) = Msg (rotateStr (-1) str)

main :: IO ()
main = do
    let encrypted = encrypt (Msg "foobar") 
        decrypted = decrypt encrypted
    print encrypted
    print decrypted
    print $ encrypt encrypted  -- this should be forbidden !

