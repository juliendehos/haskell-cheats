module Rotate where

import Data.Char

rotateChar :: Int -> Char -> Char
rotateChar k c
    | isLower c = chr (ord 'a' + ((ord c - ord 'a' + k) `mod` 26))
    | isUpper c = chr (ord 'A' + ((ord c - ord 'A' + k) `mod` 26))
    | otherwise = c

rotateStr :: Int -> String -> String
rotateStr k = map (rotateChar k)

