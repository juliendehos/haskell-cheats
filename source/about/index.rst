
About Haskell Cheat Sheet
===============================================================================


Why this cheat sheet ?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This is my cheat sheet about Haskell. It is a collection of concise and
practical reminders about various subjects: language, build tools, libraries... 


Where to find this cheat sheet ?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `HTML version <https://juliendehos.gitlab.io/haskell-cheats/index.html>`__
- `PDF version <https://juliendehos.gitlab.io/haskell-cheats/haskell-cheats.pdf>`__
- `Source code (document & examples) <https://gitlab.com/juliendehos/haskell-cheats>`__



TODO ?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- typing: gadt, type families, data kinds, functional dependencies, free monad, final tagless
- 102: traversable, foldable, ReaderT/WriterT/ExceptT, prisms, optics, mtl
- web: hakyll
- numerical computing: massiv, hasktorch, grenade, accelerate, ad, easyvision, opencv
- system: config-ini, turtle
- databases : opaleye
- data science ? : cassava, frames, hvega, monad-bayes...
- concurrency ? : ioref, mvar, stm, threads

haskell-gi, juicy-pixels, scalpel,
optparse-applicative, servant, miso
refined (http://nikita-volkov.github.io/refined/)

ci-test: parsing tools 


