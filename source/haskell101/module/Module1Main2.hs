import Module1 (add2, add3)   -- import add2 and add3 only

main = do
    print $ Module1.add3 39     -- ok
    print $ add3 39             -- ok
    -- print $ Module1.add1 41  -- error
    -- print $ add1 41          -- error

