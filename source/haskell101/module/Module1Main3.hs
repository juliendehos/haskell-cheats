import Module1 hiding (add3)   -- hide add3

main = do
    print $ Module1.add1 41     -- ok
    print $ add1 41             -- ok
    -- print $ Module1.add3 39  -- error
    -- print $ add3 39          -- error

