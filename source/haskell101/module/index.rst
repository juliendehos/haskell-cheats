
Modules
===============================================================================

:Date:
    2019-03-23

:Examples:
   `haskell101/module <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell101/module>`__

Haskell has a simple yet practical module system. A module is simply a set of
symbols (functions, data types, types classes...) defined in a file. 
These symbols can then be imported in other modules or files.

Define a module
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- Export all names:

.. literalinclude:: Module1.hs
    :caption: Module1.hs
    :language: haskell

- Specify exported names:

.. literalinclude:: Module2.hs
    :caption: Module2.hs
    :language: haskell

Import a module
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- Import all names:

.. literalinclude:: Module1Main1.hs
    :caption: Module1Main1.hs
    :language: haskell

- Import specific names:

.. literalinclude:: Module1Main2.hs
    :caption: Module1Main2.hs
    :language: haskell

- Hide names:

.. literalinclude:: Module1Main3.hs
    :caption: Module1Main3.hs
    :language: haskell

- Import with qualified:

.. literalinclude:: Module1Main4.hs
    :caption: Module1Main4.hs
    :language: haskell

- Import with qualified and renaming:

.. literalinclude:: Module1Main5.hs
    :caption: Module1Main5.hs
    :language: haskell

