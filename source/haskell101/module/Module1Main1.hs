import Module1  -- import add1, add2 and add3

main = do
    print $ Module1.add1 41     -- ok
    print $ add1 41             -- ok
    print $ Module1.add3 39     -- ok
    print $ add3 39             -- ok

