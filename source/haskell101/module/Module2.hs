module Module2 (sub3) where  -- export sub3 only

sub1 x = x - 1

sub2 = sub1 . sub1

sub3 = sub2 . sub1

