module Module1 where  -- export add1, add2 and add3

add1 x = x + 1

add2 = add1 . add1

add3 = add2 . add1

