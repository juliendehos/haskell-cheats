#!/bin/sh

ALLHS=$(find . -name "*Main*.hs")
for hs in ${ALLHS} ; do
    nix-shell --run "runghc ${hs}" || exit 1
done 

