
The Haskell programming language
===============================================================================

:Date:
    2019-03-07

:Examples:
   `haskell101/haskell <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell101/haskell>`__


Haskell is a general-purpose programming language defined by an open standard
(Haskell 2010). It is statically-typed, purely functional and lazily evaluated.
Haskell has a powerful type system and is well-suited for concurrent
programming (concurrency primitives, green-threads, parallel
garbage-collector...). Haskell is used in academia and industry, and has many
open-source community packages. 

See also: `Haskell (Wikipedia)
<https://en.wikipedia.org/wiki/Haskell_(programming_language)>`__.


Some key features
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Purely functional
-------------------------------------------------------------------------------

In Haskell, every function is pure: for a given input, it always produces the
same output, and does nothing else. In the following example, the function
``repeatStringOk`` takes a string and returns a string (by repeating the input
string).

.. literalinclude:: purely-functional-ok.hs
    :caption: purely-functional-ok.hs
    :language: haskell

If we run this code, we get:

.. code-block:: text

   $ runghc purely-functional-ok.hs 
   inside main...
   toto

Side-effects cannot be introduced in pure functions.  In the following example,
we try to print a message inside the ``repeatStringKo`` function.

.. literalinclude:: purely-functional-ko.hs
    :caption: purely-functional-ko.hs
    :language: haskell

This side-effect is not permitted in a pure function. If we run this
code, the compiler detects an error: 

.. code-block:: text

   $ runghc purely-functional-ko.hs 

   purely-functional-ko.hs:3:6: error:
       • Couldn't match type ‘[]’ with ‘IO’
         Expected type: IO b
           Actual type: [b]
   ...


This is interesting because the error-prone side-effects have to be handled
explicitly, and this is verified by the compiler.



Type system
-------------------------------------------------------------------------------

Haskell has a powerful type-system. Every expression has a type, which is
determined at compile time. In the following example, ``x`` is a ``String``.

.. code-block:: haskell

    Prelude> x = "toto"

    Prelude> :type x
    x :: String

Every function also has a type. Here the ``welcome`` function takes a ``String``
and returns a ``String``.

.. code-block:: haskell

    Prelude> welcome s = "welcome " ++ s ++ "!"

    Prelude> :type welcome 
    welcome :: String -> String

    Prelude> welcome x
    "welcome toto!"

Haskell has polymorphic types and type-classes. The following ``mul2`` function
takes any value of type ``a`` that is in the ``Num`` class, and returns a value
of type ``a``.

.. code-block:: haskell

    Prelude> mul2 n = n * 2

    Prelude> :type mul2 
    mul2 :: Num a => a -> a

    Prelude> mul2 21
    42

Haskell has algebraic data type. The following example defines a polymorphic
recursive type ``List`` and uses this type for creating a list of strings.

.. code-block:: haskell

    Prelude> data List a = Cons a (List a) | Nil

    Prelude> l = Cons "toto" (Cons "tata" Nil)

    Prelude> :type l
    l :: List String


Lazy evaluation
-------------------------------------------------------------------------------

In Haskell, an expression is evaluated only when needed. This enables to
compose functions then to evaluate them efficiently, or to define infinite data
structures. 

For example, the following code defines an infinite list ``xs``. It is then
evaluated in a finite (therefore partial) way when taking the first 5 elements.

.. code-block:: haskell

    Prelude> 1:2:3:[]   -- build a list using the (:) operator and the empty list []
    [1,2,3]

    Prelude> xs = 1 : xs  -- build an infinite list of 1

    Prelude> take 5 xs   -- takes the first 5 elements of the infinite list xs
    [1,1,1,1,1]




Documentation
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Haskell website <https://www.haskell.org>`__
- `Haskell wiki <https://wiki.haskell.org/Haskell>`__
- `Haskell wikibook <https://en.wikibooks.org/wiki/Haskell>`__
- `What I wish I knew when learning Haskell <http://dev.stephendiehl.com/hask/>`__

Compilers
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Ghc <https://www.haskell.org/ghc/>`__: state-of-the-art compiler for Haskell
- `Ghcjs <https://github.com/ghcjs/ghcjs>`__: Haskell to JavaScript compiler
- `Asterius <https://tweag.github.io/asterius/>`__: Haskell to WebAssembly compiler


Packages
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Hackage <https://hackage.haskell.org>`__: community's central package archive
- `Hoogle <https://www.haskell.org/hoogle>`__: Haskell API search engine
- `Gabriel Gonzalez, State of the Haskell ecosystem <https://github.com/Gabriel439/post-rfc/blob/master/sotu.md>`__
- `Big Moon, Haskell Recommendation Libraries <https://github.com/e-bigmoon/awesome-haskell>`__


Community
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Haskell Weekly <https://haskellweekly.news>`__: email newsletter
- `Haskell discourse <https://discourse.haskell.org>`__: web forum
- `Functional programming <https://fpchat-invite.herokuapp.com>`__: Slack chat service
- `IRC <https://wiki.haskell.org/IRC_channel>`__: text chat service
- `Planet haskell <https://planet.haskell.org>`__: blog aggregator
- `Mailing lists <https://wiki.haskell.org/Mailing_lists>`__


Some Blogs
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Michael Snoyman <https://www.snoyman.com/blog>`__
- `Gabriel Gonzalez <http://www.haskellforall.com>`__
- `Matt Parsons <https://www.parsonsmatt.org>`__
- `Sandy Maguire <https://reasonablypolymorphic.com>`__
- `Brent Yorgey <https://byorgey.wordpress.com>`__
- `Bartosz Milewski <https://bartoszmilewski.com>`__
- `Mark Karpov <https://markkarpov.com>`__
- `Tweag.io <https://www.tweag.io/blog>`__
- `Well-Typed <http://www.well-typed.com/blog>`__
- `Monday Morning Haskell <https://mmhaskell.com/blog>`__
- `Lysxia <https://blog.poisson.chat>`__

