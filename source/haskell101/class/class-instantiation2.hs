data Rectangle a = Rectangle a a

instance Show a => Show (Rectangle a) where
    show (Rectangle w h) = "Rectangle " ++ show w ++ " " ++ show h

main :: IO ()
main = putStrLn $ show $ Rectangle 4 (2::Int)
