data Rectangle a = Rectangle a a

instance Show (Rectangle a) where
    show (Rectangle _w _h) = "Rectangle"

main :: IO ()
main = putStrLn $ show $ Rectangle 4 (2::Int)
-- or: main = print $ Rectangle 4 2
