data Rectangle a = Rectangle a a 
    deriving (Show)

main :: IO ()
main = putStrLn $ show $ Rectangle 4 (2::Int)

