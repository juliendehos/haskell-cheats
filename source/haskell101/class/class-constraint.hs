data Rectangle a = Rectangle a a

getArea :: Num a => Rectangle a -> a
getArea (Rectangle w h) = w*h

main :: IO ()
main = do
    print $ getArea $ Rectangle (21::Int) 2
    print $ getArea $ Rectangle (21::Double) 2
