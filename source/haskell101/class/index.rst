
Type-classes
===============================================================================

:Date:
    2019-08-12

:Examples:
   `haskell101/class <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell101/class>`__


- `Wikipedia <https://en.wikipedia.org/wiki/Type_class>`__

- `Haskell wikibook <https://en.wikibooks.org/wiki/Haskell/Classes_and_types>`__


Intuition
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Type-class is a mecanism for adding constraints to polymorphic types.
Basically, a type-class is just a set of function signatures that a polymorphic
type should implement.

An instance of a type-class is a type that implements the functions required by
the type-class.

Type-class can be compared to Java interface.


Predefined type-classes
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has predefined type-classes (``Eq``, ``Ord``...) that data-type may or
may not implement.

+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
|            | Bool               | Char               | String             | Int                | Integer            | Float              | Double             |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Eq         | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Ord        | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Show       | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Read       | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Enum       | :math:`\checkmark` | :math:`\checkmark` |                    | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Bounded    | :math:`\checkmark` | :math:`\checkmark` |                    | :math:`\checkmark` |                    |                    |                    |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Num        |                    |                    |                    | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Integral   |                    |                    |                    | :math:`\checkmark` | :math:`\checkmark` |                    |                    |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+
| Fractional |                    |                    |                    |                    |                    | :math:`\checkmark` | :math:`\checkmark` |
+------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+--------------------+

For example, the type ``Bool`` is of class ``Eq`` (we can check if two booleans
are equal) but not of class ``Num`` (we can't sum two booleans).


Type constraints
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Using type-classes, we can define polymorphic-but-constrained types or
functions.

For example, the following type ``Rectangle a`` is polymorphic, so we can create
a value of type ``Rectangle Int`` or a value of type ``Rectangle String``.
Now, suppose we want a function ``getArea`` that compute the area of a
``Rectangle``.  We thus have to compute the product of the two fields of the
rectangle, which can be done for the type ``Rectangle Int`` but not for the
type ``Rectangle String``. To implement this function, we can add a constraint
on the polymorphic type.

.. literalinclude:: class-constraint.hs
    :caption: class-constraint.hs
    :language: haskell

.. code-block:: text

   $ runghc class-constraint.hs
   42
   42.0

Here, ``getArea`` is a function that takes a ``Rectangle a`` and returns a
``a`` where ``a`` is a type of class ``Num`` (and therefore has the ``(*)``
operator used by ``getArea``).
Thus, we can evaluate ``getArea`` on a ``Rectangle Int`` or on a ``Rectangle
Double`` but not on a ``Rectangle String``. And the compiler ensures that our
code respect the constraints.


Defining a type-class
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can define a type-class using ``class`` and specifying the function
signatures. For example, the class ``Show`` has a function ``show`` that takes
a value of the polymorphic type and returns a ``String``.

.. code-block:: haskell

   class Show a where
       show :: a -> String

Instantiation
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can instantiate a class of a type using ``instance`` and implementing the 
functions required by the class.

For example in the following code, the type ``Rectangle a`` instantiate the
class ``Show``. Here, the function ``show`` defined for a ``Rectangle a`` just
returns the text "Rectangle".

.. literalinclude:: class-instantiation1.hs
    :caption: class-instantiation1.hs
    :language: haskell

.. code-block:: text

   $ runghc class-instantiation1.hs
   Rectangle

Now, suppose we want to also print the fields of the rectangle.  We can get a
string from a value of type ``a`` by calling the function ``show`` on this
value but this requires that the type ``a`` is of class ``Show``.
Haskell can do that: we just have to add the constraint ``Show a`` when
instantiating ``Show (Rectangle a)``. 



.. literalinclude:: class-instantiation2.hs
    :caption: class-instantiation2.hs
    :language: haskell

.. code-block:: text

   $ runghc class-instantiation2.hs
   Rectangle 4 2

In other words: for any type ``a`` of class ``Show``, we instantiate the class
``Show`` for ``Rectangle a`` where ...


Inheritance
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell can automatically instantiate some classes. This is done by using the
``deriving`` keyword when defining the type.

.. literalinclude:: class-derivation.hs
    :caption: class-derivation.hs
    :language: haskell


.. code-block:: text

   $ runghc class-derivation.hs
   Rectangle 4 2


