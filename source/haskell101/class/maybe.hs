
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

safeMul2 :: Double -> Maybe Double
safeMul2 x = Just $ x * 2

main :: IO ()
main = do

    print $ safeSqrt 1764
    print $ safeSqrt (-1)

    print $ safeMul2 21

    print $ safeSqrt 441 >>= safeMul2
    print $ safeSqrt (-1) >>= safeMul2

    print $ safeMul2 882 >>= safeSqrt
    print $ safeMul2 (-882) >>= safeSqrt

    print $ do
        x <- safeSqrt 1024
        y <- safeMul2 5
        return $ x + y

    print $ (+) <$> safeSqrt 1024 <*> safeMul2 5


