data Shape = Disk Double | Rectangle Double Double

getArea :: Shape -> Double
getArea (Disk r) = pi * r**2
getArea (Rectangle w h) = w * h

main :: IO ()
main = do
    print $ getArea $ Disk 2
    print $ getArea $ Rectangle 4 2

