safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

main :: IO ()
main = do
    print $ safeSqrt (-1)
    print $ safeSqrt 1764

