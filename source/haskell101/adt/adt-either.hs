safeSqrt :: Double -> Either String Double
safeSqrt x = if x > 0 then Right (sqrt x) else Left "invalid number"

main :: IO ()
main = do
    print $ safeSqrt (-1)
    print $ safeSqrt 1764


