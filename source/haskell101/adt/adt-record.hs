data Rectangle = Rectangle
    { rectangleWidth :: Double
    , _height        :: Double
    } 

r1, r2, r3 :: Rectangle
r1 = Rectangle 4 2
r2 = Rectangle { _height = 2, rectangleWidth = 4 }
r3 = r1 { rectangleWidth = 42 }

w3 :: Double
w3 = rectangleWidth r3

main :: IO ()
main = print w3
