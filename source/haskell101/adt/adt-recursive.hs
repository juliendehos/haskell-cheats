data Mylist a = Cons a (Mylist a) | Nil deriving (Show)

mul2 :: Num a => Mylist a -> Mylist a
mul2 Nil = Nil
mul2 (Cons x xs) = Cons (2 * x) (mul2 xs)

main :: IO ()
main = print $ mul2 $ Cons 13 (Cons 37 Nil :: Mylist Int)
