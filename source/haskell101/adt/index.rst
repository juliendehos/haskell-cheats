
Algebraic Data Types
===============================================================================

:Date:
    2019-08-12

:Examples:
   `haskell101/adt <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell101/adt>`__


An Algebraic Data Type (ADT) is a composite type formed by combining other
types.

- `Wikipedia <https://en.wikipedia.org/wiki/Algebraic_data_type>`__

- `Haskell wiki <https://wiki.haskell.org/Algebraic_data_type>`__


Sum type
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A sum type is the union of its possible value.  For example, the following
``Color`` type is the union of the values ``Blue``, ``White`` and ``Red``.

.. literalinclude:: adt-sum.hs
    :caption: adt-sum.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-sum.hs
   #FF0000

Here, we have defined a new type and its (new) values: a variable of type
``Color`` can have the value ``Blue``, ``White`` or ``Red``.
We can use this type and its value like any other types and values
(functions, pattern matching...).

This is called a "sum" type because the size of the type (i.e. the number of
all the possible values) is the sum of the size of the union cases, here
:math:`1+1+1=3`.


Product type
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A product type is the cartesian product of the types used in its definition.
For example, the following ``Rectangle`` type is the cartesian product of a
first ``Double`` with a second ``Double``.

.. literalinclude:: adt-product.hs
    :caption: adt-product.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-product.hs
   8.0

This is called a "product" type because the size of the composite type is
the product of the size of the internal types, here :math:`D \times D = D^2`
where :math:`D` is the size of the ``Double`` type.




Algebraic type
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

An ADT is just a sum of product types.

.. literalinclude:: adt-algebraic.hs
    :caption: adt-algebraic.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-algebraic.hs
   12.566370614359172
   8.0

Here, the size of ``Shape`` is :math:`D + D^2` where :math:`D` is the size of
``Double``.


Record
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has a record syntax for ADT. Basically, it's just giving names to
internal fields, which also provides access functions.

.. literalinclude:: adt-record.hs
    :caption: adt-record.hs
    :language: haskell


Polymorphic type
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ADT can be polymorphic. For example, the following type ``Rectangle a`` can have
``Rectangle`` values composed of a first value of type ``a`` and a second value
of type ``a``.


.. literalinclude:: adt-polymorphic.hs
    :caption: adt-polymorphic.hs
    :language: haskell



Example: Maybe
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has many predefined ADT. ``Maybe`` implements optional values.

.. code-block:: haskell

   data Maybe a = Just a | Nothing

Thus, a value of type ``Maybe`` can be ``Nothing`` (no value) or ``Just`` a
value of the polymorphic type ``a``.

.. literalinclude:: adt-maybe.hs
    :caption: adt-maybe.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-maybe.hs
   Nothing
   Just 42.0


Example: Either
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``Either`` implements two-case values.

.. code-block:: haskell

   data Either a b = Left a | Right b

A value of type ``Either`` can be a value ``Left`` with a polymorphic type ``a``
or a value of type ``Right`` with a polymorphic type ``b``.  ``Either`` is
commonly used for handling errors: a ``Left`` value describes a failure and a
``Right`` value describes a success.


.. literalinclude:: adt-either.hs
    :caption: adt-either.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-either.hs
   Left "invalid number"
   Right 42.0


Recursive data-type
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ADT can be recursive (i.e. we can define a data-type by using this data-type). 
This is very convenient for defining lists, trees...

.. literalinclude:: adt-recursive.hs
    :caption: adt-recursive.hs
    :language: haskell

.. code-block:: haskell

   $ runghc adt-recursive.hs
   Cons 26 (Cons 74 Nil)


