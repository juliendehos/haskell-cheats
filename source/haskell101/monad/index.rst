
Functor, Applicative, Monad
===============================================================================

:Date:
    2019-08-12

:Examples:
   `haskell101/monad <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/haskell101/monad>`__


Functor, Applicative and Monad are very common type-classes in Haskell. They 
come from the category theory.

- `Wikipedia <https://en.wikipedia.org/wiki/Monad_(functional_programming)>`__

- `Haskell wikibook <https://en.wikibooks.org/wiki/Haskell/Understanding_monads>`__



The Monad class
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A monad is a design pattern for structuring programs. Monads can compose
actions/computations that may or may not produce a value.

.. code-block:: haskell

   class Monad m where
       return :: a -> m a
       (>>=) :: m a -> (a -> m b) -> m b    -- "bind"

       (>>) :: m a -> m b -> m b   -- "then"
       m >> k = m >>= \_ -> k      -- default implementation of then


The function ``return`` creates an action from a value. The "bind" operator
``(>>=)`` compose an action with a value to another action. The "then" operator
``(>>)`` compose an action but ignore its value.


Example: IO
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

In Haskell, the ``IO`` monad (i.e. type-class) implements input-output
operations. For example, the function ``putStrLn`` takes a ``String`` and
return no value but can do IO (and it does: it writes the parameter to the
standard output).

.. code-block:: haskell

   Prelude> :type putStrLn
   putStrLn :: String -> IO ()

Similarly, the function ``getLine`` takes no parameter, returns a ``String``
and can do IO (it reads a line from the standard input).

.. code-block:: haskell

   Prelude> :type getLine
   getLine :: IO String

In Haskell, the main program is a function that returns no value but can do IO.
For example, the following program displays a prompt to the screen then reads a
line from the keyboard and displays a modification of this line.


.. literalinclude:: monad-io1.hs
    :caption: monad-io1.hs
    :language: haskell

.. code-block:: text

   $ runghc monad-io1.hs
   Name ?
   Julien
   Hello JULIEN

Haskell also provides the "do notation" which is equivalent to the functions
and operators defined in the ``Monad`` class but which looks like imperative
code.  We just have to write the keyword ``do`` followed by the successive
actions.  The left arrow ``<-`` extracts the value from an action (on the
right) to a classic "pure" variable (on the left).

.. literalinclude:: monad-io1-do.hs
    :caption: monad-io1-do.hs
    :language: haskell

We can write monadic functions such as the following ``getName``. 

.. literalinclude:: monad-io2.hs
    :caption: monad-io2.hs
    :language: haskell

Monadic functions can use the do-notation.

.. literalinclude:: monad-io2-do.hs
    :caption: monad-io2-do.hs
    :language: haskell

Only monadic functions can do IO and the ``IO`` monad should appear in their
signature.  This is very useful because it prevents dangerous unwanted action
and this is checked by the compiler.

.. literalinclude:: monad-io3.hs
    :caption: monad-io3.hs
    :language: haskell

Here, ``mul2`` takes an ``Int`` and returns an ``Int`` so it can't do IO (this would
require ``mul2 :: Int -> IO Int``).

.. code-block:: text

   $ runghc monad-io3.hs

   monad-io3.hs:4:5: error:
       • Couldn't match expected type ‘Int’ with actual type ‘IO Int’
       • In a stmt of a 'do' block: dangerousUnwantedAction
         In the expression:
           do dangerousUnwantedAction
              return (2 * x)
         In an equation for ‘mul2’:
             mul2 x
               = do dangerousUnwantedAction
                    return (2 * x)
     |
   4 |     dangerousUnwantedAction
     |     ^^^^^^^^^^^^^^^^^^^^^^^






The Functor class
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A `functor <https://en.wikipedia.org/wiki/Functor>`__ is a type that can be
flat-mapped (i.e. we can apply a pure function on its internal values).

.. code-block:: haskell

   class Functor f where
       fmap :: (a -> b) -> f a -> f b

       (<$>) = fmap

Many Haskell types are functors: ``[]`` (lists), ``Maybe``, ``Either a`` (not
``Either``)...

.. code-block:: haskell

   Prelude> fmap (*2) [13, 37, 42]
   [26,74,84]

   Prelude> (*2) <$> [13, 37, 42]
   [26,74,84]

   Prelude> fmap (*2) (Just 3)
   Just 6

   Prelude> fmap (*2) Nothing
   Nothing

   Prelude> fmap (*2) (Right 3)
   Right 6

   Prelude> fmap (*2) (Left "foobar")
   Left "foobar"



The Applicative class
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`Applicative functor <https://en.wikipedia.org/wiki/Applicative_functor>`__ is
an intermediate structure between functor and monad.

.. code-block:: haskell

   class Functor f => Applicative f where
       pure :: a -> f a    -- equivalent to "return" in Monad
       (<*>) :: f (a -> b) -> f a -> f b       -- "ap"

Applicatives allow to sequence functor applications.  Using applicatives
generally leads to a concise syntax, the "applicative style".

.. code-block:: haskell

   Prelude> (+) <$> [2] <*> [3]
   [5]

   Prelude> (+) <$> [2] <*> [3,4]
   [5,6]

   Prelude> (+) <$> Right 2 <*> Left "foobar"
   Left "foobar"

   Prelude> mulAdd x y z = x * y + z

   Prelude> mulAdd <$> [2] <*> [3] <*> [1]
   [7]




Example: Maybe
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Maybe is a functor, so it supports ``fmap`` (and ``<$>``).

.. literalinclude:: maybe-functor.hs
    :caption: maybe-functor.hs
    :language: haskell

.. code-block:: text

   $ runghc maybe-functor.hs
   Nothing
   Just 42.0
   Nothing
   Just 42.0


Maybe is a also an applicative functor, so it also supports ``pure`` and
``<*>``.

.. literalinclude:: maybe-applicative.hs
    :caption: maybe-applicative.hs
    :language: haskell

.. code-block:: text

   $ runghc maybe-applicative.hs
   Just 42.0
   Just 42.0
   Nothing


Maybe is a also a monad, so it also supports ``return``, ``>>``, ``>>=`` and
the do-notation.

.. literalinclude:: maybe-monad.hs
    :caption: maybe-monad.hs
    :language: haskell

.. code-block:: text

   $ runghc maybe-monad.hs
   Just 42.0
   Just 42.0
   Just 42.0



Applicative vs Monad
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

It is often possible to write a code in different ways. Some programmers like
the applicative style, others prefer the do-notation, others don't care very
much...

.. literalinclude:: applicative-vs-monad.hs
    :caption: applicative-vs-monad.hs
    :language: haskell

.. code-block:: text

   $ runghc applicative-vs-monad.hs
   Just 42.0
   Just 42.0
   Just 42.0



Using classes with functions and types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Classes, functions and types work well together.  For example in the following
code, the ``fmul2`` function is defined using the ``Functor`` class, so we can
apply this function to a list, a ``Maybe``, etc.

Reciprocally, we can define a data-type ``Mylist`` which instantiates the
``Functor`` class, so we can automatically use our data-type with any function
defined on ``Functor``, such as ``fmap``, ``fmul2``, etc.

.. literalinclude:: using-functor.hs
    :caption: using-functor.hs
    :language: haskell


.. code-block:: text

   $ runghc using-functor.hs
   [26,74]
   Just 4
   Cons True (Cons False Nil)
   Cons 26 (Cons 74 Nil)


Type-class inheritance
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A class can inherit from another class.  This means that a data-type which
instantiates the class should also instantiate the inherited class (this is
checked by the compiler).  For example, an ``Applicative`` is also a
``Functor`` and a ``Monad`` is also an ``Applicative``.

.. code-block:: haskell

   class Functor f => Applicative f where
      ...

   class Applicative m => Monad m where
      ...

Thus, the following ``applyBinOp`` function is defined on ``Applicative`` but 
it can also use ``<$>`` (defined in ``Functor``) because any
``Applicative`` is also a ``Functor``.

.. literalinclude:: derivation.hs
    :caption: derivation.hs
    :language: haskell


.. code-block:: text

   $ runghc derivation.hs
   Just 42



Laws, category theory
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The category theory formalizes the notion of functor, applicative functor and
monad. 

A functor should satisfy these laws:

.. code-block:: haskell

   fmap id = id

   fmap (g . f) = fmap g . fmap f


An applicative functor should satisfy these laws:

.. code-block:: haskell

   pure id <*> v = v                            -- identity

   pure f <*> pure x = pure (f x)               -- homomorphism

   u <*> pure y = pure ($ y) <*> u              -- interchange

   pure (.) <*> u <*> v <*> w = u <*> (v <*> w) -- composition


A monad should satisfy these laws:

.. code-block:: haskell

    m >>= return     =  m                        -- right unit

    return x >>= f   =  f x                      -- left unit

    (m >>= f) >>= g  =  m >>= (\x -> f x >>= g)  -- associativity


The Haskell type-classes (``Functor``, ``Applicative``, ``Monad``) implement
the corresponding structures defined in the category theory but the compiler
does not check that the laws are satisfied. So, if we implement a new
``Functor``, ``Applicative`` or ``Monad``, we have to verify manually that our
implementation satisfies the corresponding laws.

There are many other structures and type-classes in the category theory and 
in Haskell (see 
`Category Theory for Programmers <https://github.com/hmemcpy/milewski-ctfp-pdf>`_ 
and `Typeclassopedia <https://wiki.haskell.org/Typeclassopedia>`_).

