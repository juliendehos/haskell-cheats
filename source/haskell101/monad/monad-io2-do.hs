import Data.Char (toUpper)

getName :: IO String
getName = do
    putStrLn "Name ?" 
    line <- getLine
    return $ map toUpper line

main :: IO ()
main = do
    name <- getName
    putStrLn $ "Hello " ++ name
