import Data.Char (toUpper)

main :: IO ()
main = do
    putStrLn "Name ?" 
    name <- getLine
    putStrLn $ "Hello " ++ map toUpper name
