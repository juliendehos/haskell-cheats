
mul2 :: Int -> Int
mul2 x = do
    dangerousUnwantedAction
    return (2*x)

dangerousUnwantedAction :: IO ()
dangerousUnwantedAction = putStrLn "BOOM!"

main = print (mul2 21)

