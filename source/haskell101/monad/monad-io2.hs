import Data.Char (toUpper)

getName :: IO String
getName = putStrLn "Name ?" >> getLine >>= (return . map toUpper)

main :: IO ()
main = getName >>= putStrLn . ("Hello "++)
