fmul2 :: (Functor f, Num a) => f a -> f a
fmul2 = fmap (*2)

data Mylist a = Cons a (Mylist a) | Nil deriving (Show)

instance Functor Mylist where
    fmap f Nil = Nil
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

main = do
    -- fmul2 can be used on any functor
    print $ fmul2 [13, 37]
    print $ fmul2 (Just 2)

    -- Mylist can be used like any functor
    let l = Cons 13 (Cons 37 Nil)
    print $ fmap (<20) l
    print $ fmul2 l

