import Data.Char (toUpper)

main :: IO ()
main = putStrLn "Name ?" >> getLine >>= putStrLn . ("Hello "++) . map toUpper

