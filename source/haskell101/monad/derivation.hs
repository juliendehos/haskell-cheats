
applyBinOp :: Applicative f => f a1 -> f a2 -> (a1 -> a2 -> b) -> f b
applyBinOp x y p = p <$> x <*> y

main = print $ applyBinOp (Just 21) (Just 2) (*)


