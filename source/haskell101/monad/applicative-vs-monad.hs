safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

safeMul2 :: Monad m => Double -> m Double
safeMul2 = return . (*2)

main = do
    -- using applicative functor:
    print $ (+) <$> safeMul2 11 <*> safeSqrt 400

    -- using monad:
    print $ safeMul2 11 >>= (\ x -> safeSqrt 400 >>= return . (x+))

    -- using monad (do notation):
    print $ do
        x <- safeMul2 11
        y <- safeSqrt 400
        return $ x + y
