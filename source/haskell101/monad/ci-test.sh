#!/bin/sh

nix-shell --run "runghc applicative-vs-monad.hs" || exit 1
nix-shell --run "runghc derivation.hs" || exit 1
nix-shell --run "runghc maybe-applicative.hs" || exit 1
nix-shell --run "runghc maybe-functor.hs" || exit 1
nix-shell --run "runghc maybe-monad.hs" || exit 1
nix-shell --run "runghc using-functor.hs" || exit 1

