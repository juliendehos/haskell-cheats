
Functions
===============================================================================

:Date:
    2019-08-09


Since Haskell is a functional programming language, one of its key features
is... function.


Expression
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

An expression is just a valid piece of Haskell code. For example, 
``2 + 0.5*(4-2)`` is an expression but ``2 + 0.5*(4-`` is not.

We can give a name to an expression.

.. code-block:: haskell

   Prelude> r = 2 + 0.5*(4-2)

Since Haskell is purely functional, the symbol ``r`` is equivalent to ``2 +
0.5*(4-2)`` (referential transparency).  As in other language, ``r`` can be
called a "variable" but in Haskell a variable can be defined only once (no
mutation).

An expression can be evaluated, which gives its "value".

.. code-block:: haskell

   Prelude> r
   3.0

Haskell only evaluates an expression when we ask for its value (lazy
evaluation).


Function
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A function is an expression which has unknown values (parameters).
For example, we can define a function called ``lerp`` that takes 3 parameters
``a``, ``b`` and ``x`` and corresponds to the expression ``a + x*(b-a)``.

.. code-block:: haskell

   Prelude> lerp a b x = a + x*(b-a)

We can then evaluate this function for given parameter values. For example, for
``a=2``, ``b=4`` and ``x=0.5``:

.. code-block:: haskell

   Prelude> lerp 2 4 0.5
   3.0

A variable can be considered as a function with no parameter.


Partial application
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can evaluate a function partially, by fixing only some of its parameters.
For example, if we fix the two first parameters of ``lerp`` (e.g. ``a=2`` and
``b=4``), we get a new function with one parameter (corresponding to the
remaining parameter of the initial function).

.. code-block:: haskell

   Prelude> lerp24 x = lerp 2 4 x

We can evaluate the new function ``lerp24``, by giving a value for its parameter:

.. code-block:: haskell

   Prelude> lerp24 0.5
   3.0

Partial evaluation is straightforward in Haskell. For example, the function ``lerp24``
can be defined more simply:

.. code-block:: haskell

   Prelude> lerp24 = lerp 2 4

Indeed, Haskell knows that ``lerp`` has 3 parameters so ``lerp24`` is a
function that still has one parameter. The previous expression is in
"point-free notation" because the function is defined in term of other
functions and not using a point ``x`` of its domain.


Lambda function
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell can define lambda functions, also called anonymous functions. 
For example, we can define a lambda that takes a parameter ``x`` and 
returns ``2 + x*(4-2)``, then evaluate this lambda for ``x=0.5``:

.. code-block:: haskell

   Prelude> (\x -> 2 + x*(4-2)) 0.5
   3.0

We can use a lambda to define the value of a symbol. This is equivalent to the
classic function definition seen previously.

.. code-block:: haskell

   Prelude> lerp24 = \ x -> lerp 2 4 x

Lambda functions can have several parameters.

.. code-block:: haskell

   Prelude> lerp = \ a b x -> a + x*(b-a)


Currying
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

In fact, a Haskell function has only one parameter but it can return a
function.  For example, the ``lerp`` function is, more precisely, a function
which takes a parameter ``a`` and returns a function which takes a parameter
``b`` and returns...

.. code-block:: haskell

   Prelude> lerp = \a -> ( \b -> ( \x -> a + x*(b-a) ) )

The parentheses only specify the priorities. And they are optional here.

.. code-block:: haskell

   Prelude> lerp = \a -> \b -> \x -> a + x*(b-a)

This function definition is called currying. It's an elegant implementation of
multi-parameter functions which automatically supports partial evaluation (for
example, ``lerp 2 4`` just evaluates ``lerp`` for ``a=2`` then its resulting
function for ``b=4`` then returns the function ``\x -> 2 + x*(4-2)``).


Composition
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Functions can be "chained", i.e. the return value of a function is used as 
the parameter value of another function.

.. code-block:: haskell

   Prelude> half x = x / 2

   Prelude> lerp24 (half (half 2))    -- the () define the priorities
   3.0

   Prelude> lerp24 $ half $ half 2    -- $ is the application operator
   3.0

Here, ``half`` takes a number and return a number so we can use its return
value as the parameter value in another evaluation of ``half`` or in an
evaluation of ``lerp24``.

Instead of chaining evaluation, we can also define a new function, by
composing the corresponding initial functions.

.. code-block:: haskell

   Prelude> (lerp24 . half . half) 2
   3.0

   Prelude> lerp24_after_2_half = lerp24 . half . half
   -- or: lerp24_after_2_half x = lerp24 $ half $ half x
   -- or: lerp24_after_2_half = lerp 2 4 . half . half

   Prelude> lerp24_after_2_half 2
   3.0

In Haskell, function composition and partial evaluation provide a simple and
elegant syntax for defining functions.


Operators
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A binary operator is placed between its two parameters (infix notation).

.. code-block:: haskell

   Prelude> 21 * 2
   42

But it can also be used like a classic function (prefix notation), using
parentheses.

.. code-block:: haskell

   Prelude> (*) 21 2
   42

Reciprocally, a function of arity 2 can be used like a binary operator (infix
notation), using backticks.

.. code-block:: haskell

   Prelude> div 21 2
   10

   Prelude> 21 `div` 2
   10


Conditionals
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A function or an expression can be defined according to a condition. Haskell 
has the classic 2-case conditional expression ``if-then-else``:

.. code-block:: haskell

   r = if 42 > 1 then "one" else "many"

Haskell also has pattern matching, for defining a function according to the
value or structure of its parameters:

.. code-block:: haskell

    formatNumber 1 = "one"
    formatNumber _ = "many"

The ``case`` syntax is the equivalent of pattern matching but for defining 
an expression instead of a function:

.. code-block:: haskell

    r = case 42 of
           1 -> "one"
           otherwise -> "many"

Finally, we can use guards to define a function according to complex
conditions on its parameters:

.. code-block:: haskell

    formatNumber x
        | x == 1 = "one"
        | x >= 1 = "many"
        | otherwise = error "this number is not supported"


Local symbols
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can define local symbol using the ``let-in`` syntax:

.. code-block:: haskell

   hypotenuse a b = 
      let a2 = a2 = a*a
               b2 = b*b
      in sqrt (a2 + b2)

Or, equivalently, using the ``where`` syntax:

.. code-block:: haskell

   hypotenuse a b = sqrt (a2 + b2)
      where a2 = a*a
            b2 = b*b

In theses examples, the two variables ``a2`` and ``b2`` are defined locally to
the function ``hypotenuse``.



Recursion
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has no loop but it has recursive functions (i.e. functions that call
themselves). 

.. code-block:: haskell

   factorial 1 = 1
   factorial n = n * factorial (n - 1)

Haskell supports the tail-recursion optimization (i.e. when the recursive
function call is the last computation of the function).

For example, we can rewrite the previous function using a tail-recursive
auxiliary function.

.. code-block:: haskell

   aux 1 acc = acc
   aux n acc = aux (n - 1) (n * acc)

   factorial n = aux n 1

Generally, we don't want to expose this auxiliary function so we can define it
locally to the desired function.

.. code-block:: haskell

   factorial n = aux n 1
      where aux 1 acc = acc
            aux n acc = aux (n - 1) (n * acc)



Closure
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A function can be defined according to its environment:

.. code-block:: haskell

   x = 42

   f y = x + y

Here, the function ``f`` has one parameter ``y`` but its definition uses the
variable ``x``. It is said that ``x`` is captured in the definition of ``f`` or
that ``x`` is in the closure of ``f``.

Since Haskell is purely functional, it can't have any of the classic problems
with closure, such as dynamic binding or unbound variable.

