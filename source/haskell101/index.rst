
Haskell 101
===============================================================================

.. toctree::
    :maxdepth: 2

    haskell/index
    function/index
    type/index
    adt/index
    class/index
    monad/index
    module/index

