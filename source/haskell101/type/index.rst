
Types
===============================================================================

:Date:
    2019-08-10

Haskell has a powerful type system. Every expression has a type, which is
infered statically by the compiler.


Basic types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has several basic types: ``Int``, ``Double``, ``String``, ``Char``,
``Bool``...

.. code-block:: haskell

   Prelude> :type 'c'
   'c' :: Char

   Prelude> :type False
   False :: Bool

Every expression has a type:

.. code-block:: haskell

   Prelude> :type 13 == 37
   13 == 37 :: Bool

   Prelude> :type if 42 > 1 then "one" else "many"
   if 42 > 1 then "one" else "many" :: String


We can specify the type of an expression, for example, to solve ambiguities:

.. code-block:: haskell

   Prelude> 13 :: Int
   13

   Prelude> 13 :: Double
   13.0



Composite types: tuples
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A tuple has a predefined number of (possibly different) types.

.. code-block:: haskell

   Prelude> (13, 37) :: (Int, Int)
   (13,37)

   Prelude> (42, "toto") :: (Int, String)
   (42,"toto")

   Prelude> (1, 2, True) :: (Double, Int, Bool)
   (1.0,2,True)


Haskell has some predefined functions for handling tuples:

.. code-block:: haskell

   Prelude> fst (13, 37)
   13

   Prelude> snd (13, 37)
   37

We can pattern match a tuple:

.. code-block:: haskell

   Prelude> (x, y) = (42, "toto") 

   Prelude> x
   42

   Prelude> y
   "toto"


Composite types: lists
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A list has an unspecified number of values of the same type:

.. code-block:: haskell

   Prelude> [13, 37] :: [Int]
   [13,37]

   Prelude> [True, True, False] :: [Bool]
   [True,True,False]

A ``String`` is a list of character (``[Char]``):

.. code-block:: haskell

   Prelude> ['f', 'o', 'o']
   "foo"

   Prelude> :type "foo"
   "foo" :: [Char]


A list is defined recursively as the empty list ``[]`` or the construction of
an element on a previous list using the operator ``(:)``:

.. code-block:: haskell

   Prelude> 13:37:[]
   [13,37]

We can pattern match a list:

.. code-block:: haskell

   Prelude> (x:xs) = [13, 37, 42]

   Prelude> x
   13

   Prelude> xs
   [37,42]



Haskell has arithmetic lists:

.. code-block:: haskell

   Prelude> [2..5]
   [2,3,4,5]

   Prelude> [2,4..10]
   [2,4,6,8,10]

Haskell has comprehension lists:

.. code-block:: haskell

   Prelude> [a | a<-[1..50], a `mod` 7 == 0]
   [7,14,21,28,35,42,49]

   Prelude> [(a,b,c) | a<-[1..10], b<-[a..10], c<-[b..10], a^2+b^2==c^2]
   [(3,4,5),(6,8,10)]


Haskell has many predefined functions on lists:

.. code-block:: haskell

   Prelude> head [2..5]
   2

   Prelude> tail [2..5]
   [3,4,5]

   Prelude> reverse [2..5]
   [5,4,3,2]

   Prelude> length [2..5]
   4

   Prelude> [2..5] !! 0
   2

   Prelude> take 2 [2..5]
   [2,3]

Haskell has infinite lists:

.. code-block:: haskell

   Prelude> take 10 [2..]
   [2,3,4,5,6,7,8,9,10,11]



Function types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Every function has a type: 

.. code-block:: haskell

   Prelude> half = (/2) :: Double -> Double

   Prelude> half 4
   2.0

``Double -> Double`` means: a function which takes a ``Double`` and returns a
``Double``.

Currying is explicit in function signatures. For example, a function that takes
an ``Int`` and a list of ``Int`` and returns a ``Bool`` has the signature ``Int
-> [Int] -> Bool``. This can also be seen as a function that takes an ``Int``
and returns a function that takes a list of ``Int`` and returns a ``Bool``.

.. code-block:: haskell

   Prelude> :{
   Prelude| isNthNull :: Int -> [Int] -> Bool
   Prelude| isNthNull n l = (l !! n) == 0
   Prelude| :}

   Prelude> isNthNull 0 [2..5]
   False

Partial application is straightforward. For example, if we evaluate a function
``Int -> [Int] -> Bool`` for one parameter, we get a function ``[Int] ->
Bool``.

.. code-block:: haskell

   Prelude> isFstNull = isNthNull 0

   Prelude> :type isFstNull 
   isFstNull :: [Int] -> Bool

   Prelude> isFstNull [2..5]
   False


Polymorphic types
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we want a function that returns the first element of a list. If it's
a list of ``Int``, we get an ``Int``. If it's a list of ``Bool``, we get a
``Bool``.

.. code-block:: haskell

   Prelude> headInt (x:xs) = x :: Int

   Prelude> headInt [2..5]
   2

   Prelude> headBool (x:xs) = x :: Bool

   Prelude> headBool [False, True]
   False

These two functions are ok but they have the same definition, only different
signatures. Instead of fixing the type ``Int`` or ``Bool``, we can say
that the function takes a list of any type ``a`` and returns a value of this
type ``a``. In fact, this is how the function ``head`` is defined.

.. code-block:: haskell

   Prelude> :type head
   head :: [a] -> a

   Prelude> head [2..5] :: Int
   2

   Prelude> head [True, False] :: Bool
   True

``[a]`` is a polymorphic type and ``head`` is a polymorphic function.


Higher-order function
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell support higher-order functions, i.e. functions that take functions as
parameter.  For example, the following function takes a function ``a ->
a``, an ``a`` and returns an ``a``

.. code-block:: haskell

   Prelude> :{
   Prelude| applyTwice :: (a -> a) -> a -> a
   Prelude| applyTwice f x = f (f x)
   Prelude| :}

   Prelude> applyTwice (*2) 1
   4

Higher-order functions are very common in functional languages.

.. code-block:: haskell

   Prelude> map (*2) [2..5]
   [4,6,8,10]

   Prelude> :type map
   map :: (a -> b) -> [a] -> [b]

   Prelude> filter (<4) [2..5]
   [2,3]

   Prelude> :type filter
   filter :: (a -> Bool) -> [a] -> [a]

   Prelude> foldl (+) 0 [2..5]
   14

   Prelude> :type foldl
   foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b







Composition, recursion
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

When we compose functions, the compiler checks that the types are compatible:

.. code-block:: haskell

   Prelude> isF = (=='f')

   Prelude> :type isF
   isF :: Char -> Bool

   Prelude> boolToString p = if p then "yes" else "no"

   Prelude> :type boolToString 
   boolToString :: Bool -> [Char]

   Prelude> yesNoF = boolToString . isF

   Prelude> :type yesNoF 
   yesNoF :: Char -> [Char]

   Prelude> yesNoF 'c'
   "no"

Here, ``isF`` returns a ``Bool`` so it can be composed to ``boolToString``
which takes a ``Bool`` as a parameter.


In the same way, a recursive definition should have compatible types:

.. code-block:: haskell

   Prelude> :{
   Prelude| mylength :: [a] -> Int
   Prelude| mylength l = if null l then 0 else 1 + mylength (tail l)
   Prelude| :}

   Prelude> mylength "foobar"
   6

Here, ``mylength`` returns an ``Int`` so we can add 1 to its return value.



Type inference
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The Haskell type system can infer the type of an expression.

For example, the function ``head :: [a] -> a`` is polymorphic and works for any
type ``a``. We can compose this function to a function ``isF :: Char -> Bool``
but this restricts the final function to ``[Char] -> Bool`` (and not ``[a] ->
Bool``). 

.. code-block:: haskell

   Prelude> :type head
   head :: [a] -> a

   Prelude> isF = (== 'f')

   Prelude> :type isF
   isF :: Char -> Bool

   Prelude> isHeadF = isF . head

   Prelude> :type isHeadF 
   isHeadF :: [Char] -> Bool

   Prelude> isHeadF "foobar"
   True



