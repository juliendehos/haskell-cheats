import System.Environment (getArgs)

sayHello :: Int -> IO ()
sayHello _x = putStrLn "hello"

main :: IO ()
main = do
    args <- getArgs
    let argsStr = if null args then "" else " on " ++ unwords args
    putStrLn $ "running dumb-hello.hs " ++ argsStr
    sayHello 42

