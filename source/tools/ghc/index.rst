
Ghc
===============================================================================

:Date:
    2019-03-08

:Examples:
   `tools/ghc <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/ghc>`__

The Glasgow Haskell Compiler (GHC) is the most commonly-used Haskell compiler. It can compile to native code for
major architectures and operating systems. It includes many libraries,
optimizations and language extensions. It also provides development tools
such as an interactive environment and a profiler. Ghc is open-source and
distributed under a BSD-like license.

See also: `Ghc user's guide
<https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html>`__,
`Ghc wiki <https://wiki.haskell.org/GHC>`__.


Running Haskell programs with runghc
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``runghc`` is a useful program for running a source file. It automatically compiles
and runs the code, without producing any intermediate file. For example,
suppose we have the following source file.

.. literalinclude:: dumb-hello.hs
    :caption: dumb-hello.hs
    :language: haskell

Then we can run this program using the following command. 

.. code-block:: text

    $ runghc dumb-hello.hs 
    running dumb-hello.hs 
    hello

``runghc`` let us specify command-line arguments for our program.

.. code-block:: text

    $ runghc dumb-hello.hs toto 42
    running dumb-hello.hs on toto 42
    hello

``runghc`` can also take compiling options (see :ref:`the-ghc-compiler`).

.. code-block:: text

    $ runghc -Wall dumb-hello.hs toto 42

    dumb-hello.hs:4:10: warning: [-Wunused-matches]
        Defined but not used: ‘x’
      |
    4 | sayHello x = putStrLn "hello"
      |          ^
    running dumb-hello.hs on toto 42
    hello



.. _the-ghc-compiler:

The ghc compiler
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``ghc`` is the main program for compiling Haskell code. Basically, we just
have to call it on our file.

.. code-block:: text

    $ ghc dumb-hello.hs

``ghc`` then builds an executable:

.. code-block:: text

    $ ./dumb-hello 
    running dumb-hello.hs 
    hello

``ghc`` has many options, for example:
    - ``-Wall``: activate all warning
    - ``-O2``: "apply every non-dangerous optimisation"
    - ``-fforce-recomp``: force recompilation (for example, for recompiling the same code but with different options)
    - ``-H100M``: set the minimum heap size to 100M
    - ...


The runtime system
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Sometimes, it is better to adjust some options at runtime. This can be done
using the ``-rtsopts`` option:

.. code-block:: text

    $ ghc -rtsopts dumb-hello.hs

Then, we can set runtime options when calling the executable:

.. code-block:: text

    $ ./dumb-hello +RTS -H100M


The ghc-pkg package manager
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

``ghc-pkg`` can create and manage Haskell packages. Here, we will not
present this tool but just mention a command for listing the packages of the
current ghc environment:

.. code-block:: text

    $ ghc-pkg list

