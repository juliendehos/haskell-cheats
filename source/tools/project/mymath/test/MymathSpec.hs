
module MymathSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Mymath

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "MymathSpec" $ do
        it "TODO" $ property $ \x -> mysqrt (x*x) == abs x
