
.. _tools-project:

Organizing a Haskell project
===============================================================================

:Date:
    2019-03-11

:Examples:
   `tools/project <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/project>`__


Haskell does not impose a particular organization of the project files but
we generally follow some conventions.


Contents
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we start a Haskell project called "mymath".
We typically create the following files.

.. code-block:: text

    mymath/
    ├── app
    │   └── mysqrt1764
    │       └── Main.hs
    ├── ChangeLog.md
    ├── LICENSE
    ├── README.md
    ├── Setup.hs
    ├── src
    │   └── Lib.hs
    ├── test
    │   └── Spec.hs
    └── mymath.cabal


The cabal file (``mymath.cabal``) is the main project configuration (see
:ref:`tools-cabal`). It describes the project, build targets, dependencies...
It can be used by the cabal program or by many other tools (stack, nix, hackage...).

For the Haskell source code of the project, we write common module files in
the ``src`` folder and their corresponding test files in the ``test`` folder.
The ``app`` folder contains program files (i.e. having a ``main`` function).

The ``Setup.hs`` file is a Haskell program for performing the setup tasks. It
is generally as simple as the following example.

.. literalinclude:: mymath/Setup.hs
    :caption: mymath/Setup.hs
    :language: haskell


Finally, we also write some ``README.md``, ``ChangeLog.md`` and ``LICENSE``
files.



Additional files for building the project
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To build the project with Nix, we add a ``default.nix`` file (see :ref:`tools-nix`).

To build the project with Stack, we add a ``stack.yaml`` file. Then we have two possibilities (see :ref:`tools-stack`):
    - reuse the cabal file, or
    - use a ``package.yaml`` file instead of the cabal file.


Helper tools for creating a Haskell project
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Some tools helps creating and initializing a Haskell project:
    - Stack: ``stack new``
    - Cabal: ``cabal init``
    - `Hasker <https://gitlab.com/juliendehos/hasker>`__

