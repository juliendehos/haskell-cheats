
.. _tools-cabal:

Cabal
===============================================================================

:Date:
    2019-05-04

:Examples:
   `tools/cabal <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/cabal>`__

Cabal is a very classic tool of the Haskell ecosystem. It can build and package
Haskell projects, install and manage dependencies... Here, we only presents how
to use Cabal to build a Haskell project.

See also: 
`Cabal User Guide <https://www.haskell.org/cabal/users-guide/index.html>`_, 
`Cabal overview <https://www.haskell.org/cabal>`_,
`the Cabal package <http://hackage.haskell.org/package/Cabal>`_.

Project setup
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Cabal can build complex projects, having multiple build targets (libraries,
executables...) or using multiple compilers (ghc, ghcjs...).

To define a Haskell project, we just have to write a cabal file, as described
below.  However, real projects generally use a classic file structure (see
:ref:`tools-project`). We can use the ``cabal init`` command to create a new
project.

Cabal file
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The cabal file defines the project: project information, build targets,
compilation flags, dependencies...

For example, the cabal file of the ``mymath`` project (see
:ref:`tools-project`) defines two build targets: a ``mymath`` library and a
``mysqrt1764`` executable.

.. literalinclude:: mymath/mymath.cabal
    :caption: mymath/mymath.cabal
    :language: haskell

Basic commands
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- build the project defined in the cabal file : 

.. code-block:: text

   $ cabal build

   Resolving dependencies...
   Configuring mymath-0.1...
   Preprocessing library for mymath-0.1..
   Building library for mymath-0.1..
   [1 of 1] Compiling Mymath           ( src/Mymath.hs, dist/build/Mymath.o )
   Preprocessing executable 'mysqrt1764' for mymath-0.1..
   Building executable 'mysqrt1764' for mymath-0.1..
   [1 of 1] Compiling Main             ( app/mysqrt1764/Main.hs, dist/build/mysqrt1764/mysqrt1764-tmp/Main.o )
   Linking dist/build/mysqrt1764/mysqrt1764 ...

- run the ``mysqrt1764`` executable: 

.. code-block:: text

   $ cabal run mysqrt1764

   Preprocessing library for mymath-0.1..
   Building library for mymath-0.1..
   Preprocessing executable 'mysqrt1764' for mymath-0.1..
   Building executable 'mysqrt1764' for mymath-0.1..
   Running mysqrt1764...
   42.0

- clean the project (``dist`` folder):

.. code-block:: text

   $ cabal clean

   cleaning...

- run an interactive session:

.. code-block:: text

   $ cabal repl mymath

   Warning: The repl command is a part of the legacy v1 style of cabal usage.

   Please switch to using either the new project style and the new-repl command
   or the legacy v1-repl alias as new-style projects will become the default in
   the next version of cabal-install. Please file a bug if you cannot replicate a
   working v1- use case with the new-style commands.

   For more information, see: https://wiki.haskell.org/Cabal/NewBuild

   Preprocessing library for mymath-0.1..
   GHCi, version 8.6.4: http://www.haskell.org/ghc/  :? for help
   [1 of 1] Compiling Mymath           ( src/Mymath.hs, interpreted )
   Ok, one module loaded.
   *Mymath> mysqrt 16
   4.0
   *Mymath> 


Cabal v2
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The version 2 of Cabal has a new interface (``new-build``, ``new-run``...). 
It is very similar to version 1 but it can also manage projects with multiple
packages.

- A multiple package project is composed of cabal projects (e.g.
  ``mymath`` and ``mysqrt1764``) with a ``cabal.project`` file:

.. code-block:: text

   mymath-v2/
   ├── cabal.project
   ├── mymath
   │   ├── mymath.cabal
   │   └── src
   │       └── Mymath.hs
   └── mysqrt1764
       ├── mysqrt1764.cabal
       └── src
           └── Main.hs

- the ``cabal.project`` file defines the packages of the project:

.. literalinclude:: mymath-v2/cabal.project
    :caption: mymath-v2/cabal.project
    :language: haskell

- the packages are classic cabal packages, e.g. ``mymath/mymath.cabal``:

.. literalinclude:: mymath-v2/mymath/mymath.cabal
    :caption: mymath-v2/mymath/mymath.cabal
    :language: haskell

- a package can depend on another package of the project , e.g. ``mysqrt1764/mysqrt1764.cabal``:

.. literalinclude:: mymath-v2/mysqrt1764/mysqrt1764.cabal
    :caption: mymath-v2/mysqrt1764/mysqrt1764.cabal
    :language: haskell

- we can build and run a package of the project, with its dependencies:

.. code-block:: text

   $ cabal new-run mysqrt1764

   Warning: The package list for 'hackage.haskell.org' is 55 days old.
   Run 'cabal update' to get the latest list of available packages.
   Resolving dependencies...
   Build profile: -w ghc-8.6.4 -O1
   In order, the following will be built (use -v for more details):
    - mymath-0.1 (lib) (first run)
    - mysqrt1764-0.1 (exe:mysqrt1764) (first run)
   Configuring library for mymath-0.1..
   Preprocessing library for mymath-0.1..
   Building library for mymath-0.1..
   [1 of 1] Compiling Mymath           ( src/Mymath.hs, /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/cabal/mymath-v2/dist-newstyle/build/x86_64-linux/ghc-8.6.4/mymath-0.1/build/Mymath.o )
   Configuring executable 'mysqrt1764' for mysqrt1764-0.1..
   Preprocessing executable 'mysqrt1764' for mysqrt1764-0.1..
   Building executable 'mysqrt1764' for mysqrt1764-0.1..
   [1 of 1] Compiling Main             ( src/Main.hs, /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/cabal/mymath-v2/dist-newstyle/build/x86_64-linux/ghc-8.6.4/mysqrt1764-0.1/x/mysqrt1764/build/mysqrt1764/mysqrt1764-tmp/Main.o )
   Linking /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/cabal/mymath-v2/dist-newstyle/build/x86_64-linux/ghc-8.6.4/mysqrt1764-0.1/x/mysqrt1764/build/mysqrt1764/mysqrt1764 ...
   42.0

- we can run an interactive session:

.. code-block:: text

   $ cabal new-repl mymath
   
   Build profile: -w ghc-8.6.4 -O1
   In order, the following will be built (use -v for more details):
    - mymath-0.1 (lib) (ephemeral targets)
   Preprocessing library for mymath-0.1..
   GHCi, version 8.6.4: http://www.haskell.org/ghc/  :? for help
   [1 of 1] Compiling Mymath           ( src/Mymath.hs, interpreted )
   Ok, one module loaded.
   *Mymath> mysqrt 16
   4.0
   *Mymath> 

- clean the project:

.. code-block:: text

   $ cabal new-clean


Other features
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Cabal can also:
   - handle fine-grain dependencies (see `hackage <https://hackage.haskell.org/>`_)
   - build and run tests (see :ref:`tools-testing`)
   - generate documentation (see :ref:`tools-doc`)
   - package, sandbox...  (see the `Cabal user guide <https://www.haskell.org/cabal/users-guide/index.html>`_)

