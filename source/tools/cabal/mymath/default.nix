with import <nixpkgs> {};
let drv = haskellPackages.callCabal2nix "mymath" ./. {};
in if lib.inNixShell then drv.env else drv

