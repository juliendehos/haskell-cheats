
Development environment
===============================================================================

:Date:
    2019-03-10


Vim + HLint + ALE
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Pros:
   - linter
   - fast, lightweight and easy to install

Cons: 
   - completion

Installation:
   - install ghc and hlint (system packages), install ale (vim plugin):

   .. code-block:: text

      $ nix-env -iA nixos.ghc nixos.hlint nixos.vimPlugins.ale

   - configure ale in your ``.vimrc``:

   .. code-block:: vim

      " ale linters, for haskell
      let g:ale_linters = {'haskell': ['hlint', 'ghc']}
      let g:ale_haskell_ghc_options = '-fno-code -v0 -isrc -Wall'

      " ale error navigation
      nmap <F8> <Plug>(ale_previous_wrap)
      nmap <F9> <Plug>(ale_next_wrap)

See also: `HLint <http://hackage.haskell.org/package/hlint>`__, `Asynchronous
Lint Engine <https://github.com/w0rp/ale>`__.


.. only:: latex

   .. figure:: ide-vim.png
      :width: 12cm

      Vim, configured for Haskell.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/ide-vim.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|


VSCode + HIE
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Pros:
   - linter
   - completion
   - interactive documentation...

Cons:
   - unfree binary

Installation:
   - enable unfree packages in ``~/.config/nixpkgs/config.nix``:

   .. code-block:: nix

      allowUnfree = true;

   - import HIE in ``~/.config/nixpkgs/config.nix``:

   .. code-block:: text

      hies = (import (fetchTarball "https://github.com/domenkozar/hie-nix/tarball/master") {}).hies;

   or:

   .. code-block:: text

      all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
      hies = all-hies.selection { selector = p: { inherit (p) ghc864 ghc844; }; };

   - install vscode:

   .. code-block:: text

      $ nix-env -iA nixos.vscode

   - launch ``code`` and install the `haskell language server extension <https://github.com/alanz/vscode-hie-server>`__.


See also: `Visual Studio Code <https://code.visualstudio.com/>`__, `Haskell
IDE Engine <https://github.com/haskell/haskell-ide-engine>`__, `Nix packages
for HIE <https://github.com/domenkozar/hie-nix>`__.


.. only:: latex

   .. figure:: ide-vscode.png
      :width: 12cm

      Visual Studio Code, configured for Haskell.


.. raw:: html

    <video preload="metadata" controls>
    <source src="../../_static/ide-vscode.mp4" type="video/mp4" />
    Your browser does not support the video tag.
    </video>


|



Some other tools
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- intellij-haskell
- ghcid
- ...

