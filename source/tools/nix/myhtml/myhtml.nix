{ mkDerivation, base, lucid, stdenv }:
mkDerivation {
  pname = "myhtml";
  version = "0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base lucid ];
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
