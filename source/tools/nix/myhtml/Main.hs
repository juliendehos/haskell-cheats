{-# LANGUAGE OverloadedStrings #-}

import Lucid

main :: IO ()
main = renderToFile "index.html" $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "myhtml"
        body_ $ do
            h1_ "myhtml"
            a_ [href_ "https://www.haskell.org"] "haskell.org"

