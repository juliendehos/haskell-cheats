{ nixpkgs ? import <nixpkgs> {} }:

let

  # import the nix derivation
  myhtml = import ./default.nix { inherit nixpkgs; };

in

  # define an environment from the derivation and add supplementary packages 
  ( nixpkgs.haskell.lib.addBuildTools myhtml [
      nixpkgs.geany
    ]
  ).env

