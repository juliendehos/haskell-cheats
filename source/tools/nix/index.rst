
.. _tools-nix:

Nix
===============================================================================

:Date:
    2019-08-02

:Examples:
   `tools/nix <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/nix>`__


`Nix <https://nixos.org/nix/>`_ is a functional package manager that works well
with Haskell. Nix can configure, build and deploy Haskell projects quite
easily.

See also:
   - `User’s Guide to the Haskell Infrastructure <https://nixos.org/nixpkgs/manual/#users-guide-to-the-haskell-infrastructure>`__
   - `Nix and Haskell in production <https://github.com/Gabriel439/haskell-nix>`__



Nix packages for Haskell
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Nix provides a curated list of Hackage packages (similarly to Stack snapshots).
We can list Haskell packages in Nix:

.. code-block:: text

   $ nix-env -qaP -A nixos.haskellPackages

   nixos.haskellPackages.a50                 a50-0.5
   nixos.haskellPackages.AAI                 AAI-0.2.0.1
   nixos.haskellPackages.abacate             abacate-0.0.0.0
   ...


Nix also provides several versions of GHC:

.. code-block:: text

   $ nix-env -qaP -A nixos.haskell.compiler

   nixos.haskell.compiler.ghc7103            ghc-7.10.3
   nixos.haskell.compiler.ghc802             ghc-8.0.2
   nixos.haskell.compiler.ghc822             ghc-8.2.2
   ...


Nix environments
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Nix can run temporary environments. For example, we can run GHCI in a temporary
environment with the Random Haskell package:

.. code-block:: text

   $ nix-shell --run ghci -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.random])"

   GHCi, version 8.4.4: http://www.haskell.org/ghc/  :? for help
   Prelude> import System.Random 
   Prelude System.Random> :set -XScopedTypeVariables 
   Prelude System.Random> x::Double <- randomIO 
   Prelude System.Random> x
   0.114730187676368


We can also define a nix environment using a ``shell.nix`` file:

.. literalinclude:: shell.nix
    :caption: shell.nix
    :language: text

.. code-block:: text

   $ nix-shell --run ghci
   GHCi, version 8.6.5: http://www.haskell.org/ghc/  :? for help
   Prelude> import System.Random
   Prelude System.Random> 



Nix script files
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can use Nix as the interpreter of a script file. For example, if we write
the following executable script file:

.. literalinclude:: myrandom.hs
    :caption: myrandom.hs
    :language: haskell

then we can execute it as a classic script file:

.. code-block:: text

   $ ./myrandom.hs 

   0.9742300417428591

Haskell dependencies (here, the Random package), are handled automatically by
Nix.


Simple derivations
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

If we already have a cabal file, we can define a corresponding nix package
(also called a derivation) by writing a ``default.nix`` file: 

.. literalinclude:: myrandom/default.nix
    :caption: myrandom/default.nix
    :language: nix

Then, we can run a temporary environment:

.. code-block:: text

   $ nix-shell 

   [nix-shell]$ cabal run

   Preprocessing executable 'myrandom' for myrandom-0.1..
   Building executable 'myrandom' for myrandom-0.1..
   Running myrandom...
   0.8916583321748858

   [nix-shell]$ exit
   exit

Or build a Nix package from our Haskell project:

.. code-block:: text

   $ nix-build 

   these derivations will be built:
     /nix/store/006x56fr52r5cfqzajr9r5s8dp05q7dk-myrandom-0.1.drv
   building '/nix/store/006x56fr52r5cfqzajr9r5s8dp05q7dk-myrandom-0.1.drv'...
   ...

   $ ./result/bin/myrandom 

   0.745757011363302


Or install/remove the package:

.. code-block:: text

   $ nix-env -i -f .installing 'myrandom-0.1'
   these derivations will be built:
     /nix/store/ni354q4fkxx45jpxa28kax3lm9cks32n-myrandom-0.1.drv
   building '/nix/store/ni354q4fkxx45jpxa28kax3lm9cks32n-myrandom-0.1.drv'...
   ...
   created 5826 symlinks in user environment

   $ myrandom 
   0.21990570027912126

   $ nix-env -e myrandom
   uninstalling 'myrandom-0.1'

   $ myrandom 
   myrandom: command not found



Pin or override packages
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can fix the nix channel (package snapshot):

.. literalinclude:: myrandom/default2.nix
    :caption: myrandom/default2.nix
    :language: nix

We can customize the haskell environment by overriding packages:

.. literalinclude:: myrandom/default3.nix
    :caption: myrandom/default3.nix
    :language: text

Other example:

.. literalinclude:: shell2.nix
    :caption: shell2.nix
    :language: nix


.. code-block:: text

   $ nix-shell shell2.nix 

   [nix-shell]$ ghc-pkg list | grep scalpel
       scalpel-0.6.0
       scalpel-core-0.6.0



Cabal2nix
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Instead of using ``callCabal2nix``, we can use ``cabal2nix`` to generate a nix
file from the cabal file:

.. code-block:: text

   cabal2nix . > myhtml.nix

Then we can load this nix file (here, ``myhtml.nix``) as a classic nix package:

.. literalinclude:: myhtml/default.nix
    :caption: myhtml/default.nix
    :language: nix

Finally, we can also define a temporary environment (for developing the
project):

.. literalinclude:: myhtml/shell.nix
    :caption: myhtml/shell.nix
    :language: nix

