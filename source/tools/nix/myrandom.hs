#!/usr/bin/env nix-shell
#!nix-shell -i "runghc -Wall" -p "haskellPackages.ghcWithPackages (pkgs: [pkgs.random])"

{-# LANGUAGE ScopedTypeVariables #-}

import System.Random

main :: IO ()
main = do
   x::Double <- randomIO
   print x

