let

  # import nix packages
  pkgs = import <nixpkgs> {};

  # build a nix derivation from myrandom.cabal
  drv = pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};

in

  if pkgs.lib.inNixShell 
  then drv.env  # load an environment (for nix-shell)
  else drv      # load a derivation (for nix-build and nix-env)

