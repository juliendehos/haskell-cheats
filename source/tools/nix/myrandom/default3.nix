let

  text-src = fetchTarball {
    url = https://github.com/haskell/text/archive/1.2.3.0.tar.gz;
    sha256 = "0rh9mb023f0s56ylzxz9c3c1y09lpl6m69ap5bnpdi0dz7fm6s85";
  };

  ghc-version = "ghc864";

  config = {
    packageOverrides = pkgs: {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          ghc = pkgs.haskell.packages.${ghc-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              # import a same package but disable test and doc
              network = dontCheck (dontHaddock (doJailbreak super.network));

              # import a specific version, from hackage
              random = self.callHackage "random" "1.1" {};

              # import from a cabal project (github repository)
              text = self.callCabal2nix "text" text-src {};

            };
          };
        };
      };
    };
  };

  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz";

  pkgs = import channel { inherit config; };

  drv = pkgs.haskell.packages.ghc.callCabal2nix "myrandom" ./. {};

in if pkgs.lib.inNixShell then drv.env else drv

