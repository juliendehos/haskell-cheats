let

  # pin nix packages to release 19.03
  channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz";

  pkgs = import channel {};

  drv = pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};

in if pkgs.lib.inNixShell then drv.env else drv
