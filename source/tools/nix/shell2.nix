let

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: rec {
          scalpel = self.callHackage "scalpel" "0.6.0" {};
          scalpel-core = self.callHackage "scalpel-core" "0.6.0" {};
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    scalpel
    scalpel-core
  ]);

in pkgs.stdenv.mkDerivation {
  name = "my-haskell-env-0";
  buildInputs = [ ghc ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}

