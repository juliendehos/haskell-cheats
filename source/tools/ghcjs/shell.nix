let

  #nixpkgs-src = fetchTarball "https://github.com/nixos/nixpkgs/archive/1c92cda.tar.gz";

  ghcjs-version = "ghcjs86";

  config = { 

    allowBroken = true;

    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghcjs = pkgs.haskell.packages.${ghcjs-version}.override {
            overrides = self: super: with pkgs.haskell.lib; {

              network = dontCheck (doJailbreak super.network_2_6_3_1);
              servant-client = dontCheck (doJailbreak super.servant-client);
              servant = dontCheck (doJailbreak super.servant);
              semigroupoids = dontCheck (doJailbreak super.semigroupoids);
              http-types = dontCheck (doJailbreak super.http-types);
              comonad = dontCheck (doJailbreak super.comonad);
              memory = dontCheck (doJailbreak super.memory);
              QuickCheck = dontCheck (doJailbreak super.QuickCheck);
              tasty = dontCheck (doJailbreak super.tasty);
              tasty-golden = dontCheck (doJailbreak super.tasty-golden);
              tasty-quickcheck = dontCheck (doJailbreak super.tasty-quickcheck);
              aeson = dontCheck (doJailbreak super.aeson);
              cookie = dontCheck (doJailbreak super.cookie);
              scientific = dontCheck (doJailbreak super.scientific);
              time-compat = dontCheck (doJailbreak super.time-compat);
              uuid-types = dontCheck (doJailbreak super.uuid-types);
              streaming-commons = dontCheck (doJailbreak super.streaming-commons);

              ghcjs-dom = dontCheck (doJailbreak super.ghcjs-dom);
              ghcjs-dom-jsffi = dontCheck (doJailbreak super.ghcjs-dom-jsffi);

            };
          };

        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };
  #pkgs = import nixpkgs-src { inherit config; };

  ghcjs = pkgs.haskell.packages.ghcjs.ghcWithPackages (ps: with ps; [
    #ghcjs-dom-jsffi
    ghcjs-dom
  ]);

in pkgs.mkShell {
  buildInputs = [
    ghcjs
  ];
}

