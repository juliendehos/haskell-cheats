import GHCJS.Foreign.Callback 
import GHCJS.Marshal
import GHCJS.Types

foreign import javascript unsafe "hsmul2 = $1"
    set_hsmul2 :: Callback a -> IO ()

hsmul2 :: JSVal -> IO JSVal
hsmul2 jsv = fromJSVal jsv >>= toJSVal . maybe (0 :: Int) ((*2) . read)

main :: IO ()
main = do
    syncCallback1' hsmul2 >>= set_hsmul2
    putStrLn "module loaded"

