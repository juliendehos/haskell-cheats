import Data.JSString as S

foreign import javascript unsafe
    "console.log($1)" console_log :: S.JSString -> IO ()

main :: IO ()
main = do
    console_log (S.pack "hello")
    putStrLn "world"

