import GHCJS.Foreign.Callback 
import GHCJS.Marshal
import GHCJS.Types

foreign import javascript unsafe "hshello = $1"
    set_hshello :: Callback a -> IO ()

hello :: IO JSVal
hello = toJSVal "hello from haskell"

main :: IO ()
main = do
    syncCallback' hello >>= set_hshello
    putStrLn "module loaded"


