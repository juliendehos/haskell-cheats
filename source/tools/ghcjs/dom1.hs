import GHCJS.DOM
import GHCJS.DOM.Types
import GHCJS.DOM.Document
import GHCJS.DOM.Element
import GHCJS.DOM.Node

main :: JSM ()
main = do
    doc <- currentDocumentUnchecked

    body <- getBodyUnchecked doc
    setInnerHTML body "<h1>hello</h1>"

    text <- createTextNode doc "world"
    _ <- appendChild body text

    return ()

