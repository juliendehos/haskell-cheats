
.. _tools-ghcjs:

Ghcjs 
===============================================================================

:Date:
    2020-01-15

:Examples:
   `tools/ghcjs <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/ghcjs>`__


Ghcjs is a Haskell to Javascript compiler. It implements many features/libraries of Ghc.

See also:
    - `ghcjs <https://github.com/ghcjs/ghcjs>`_
    - `ghcjs-base <http://hackage.haskell.org/package/ghcjs-base>`_
    - `ghcjs-dom <https://github.com/ghcjs/ghcjs-dom>`_



Ghcjs usage
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ghcjs aims to be compatible with Ghc. For example, the following code can be
compiled with both Ghc and Ghcjs.

.. literalinclude:: ghcjs1.hs
   :caption: ghcjs1.hs
   :language: haskell


The compilation with Ghcjs produces Javascript code: a multiple-file version of
the application (``lib,js``, ``out.js``, ``rts.js`` and ``runmain.js``), a
single-file version (``all.js``) and a testing HTML page (``index.html``).

.. code-block:: text

    $ ghcjs ghcjs1.hs 
    [1 of 1] Compiling Main             ( ghcjs1.hs, ghcjs1.js_o )
    Linking ghcjs1.jsexe (Main)

    $ tree ghcjs1.jsexe/
    ghcjs1.jsexe/
    ├── all.js
    ├── all.js.externs
    ├── index.html
    ├── lib.js
    ├── manifest.webapp
    ├── out.frefs.js
    ├── out.frefs.json
    ├── out.js
    ├── out.stats
    ├── rts.js
    └── runmain.js

We can run the Javascript code using Node.js:

.. code-block:: text

    $ node ghcjs1.jsexe/all.js
    42

Or in a browser.

.. figure:: ghcjs1.png
    :width: 90%

    The Firefox console when opening the page ``ghcjs1.jsexe/index.html``.


Javascript Foreign Interface
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can call Javascript function from Haskell code.

.. literalinclude:: ffi1.hs
   :caption: ffi1.hs
   :language: haskell

This can be useful for manipulating the DOM.

.. literalinclude:: ffi2.hs
   :caption: ffi2.hs
   :language: haskell

.. literalinclude:: ffi2.html
   :caption: ffi2.html
   :language: html

.. figure:: ffi2.png
    :width: 90%

    ``ffi2.html``

We can also call a Haskell function from the Javascript code.

.. literalinclude:: ffi3.hs
   :caption: ffi3.hs
   :language: haskell

.. literalinclude:: ffi3.html
   :caption: ffi3.html
   :language: html

.. figure:: ffi3.png
    :width: 90%

    ``ffi3.html``


Finally, we can call a Haskell function with a parameter:

.. literalinclude:: ffi4.hs
   :caption: ffi4.hs
   :language: haskell

.. literalinclude:: ffi4.html
   :caption: ffi4.html
   :language: html


.. figure:: ffi4.png
    :width: 90%

    ``ffi4.html``


DOM interface
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Using the ``ghcjs-dom`` library, we can build a web page directly in Haskell.

.. literalinclude:: dom1.hs
   :caption: dom1.hs
   :language: haskell


.. figure:: dom1.png
    :width: 90%

    ``dom1.jsexe/index.html``

However this is quite low-level. Haskell has more practical library for
building web app (see :ref:`web`).

