main :: IO ()
main = do
    let xs = [1..1000000]
    print $ {-# SCC last_xs #-} last xs
    print $ {-# SCC last_init_xs #-} last $ init xs

