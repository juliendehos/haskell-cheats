import Criterion.Main

fibo :: Int -> Int
fibo n = if n < 2 then 1 else fibo (n-1) + fibo (n-2)

main = defaultMain
    [ bgroup "fibo"
        [ bench "1"  $ whnf fibo 1
        , bench "5"  $ whnf fibo 5
        , bench "9"  $ whnf fibo 9
        , bench "11" $ whnf fibo 11
        ]
    ]

