
Profiling
===============================================================================

:Date:
    2020-01-15

:Examples:
   `tools/profiling <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/profiling>`__


Profiling and benchmarking are very useful for analyzing the performance of a
program. It is even more important for a Haskell program, where laziness and
garbage collection can be difficult to predict.

See also:
    - `Ghc profiling tools <https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html>`_
    - `Criterion <http://www.serpentine.com/criterion/>`_


Time profiling (Ghc)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

With Ghc, we can profile the functions of a program, and even expression (using
``SCC`` annotations).

.. literalinclude:: ghc/list.hs
   :caption: ghc/list.hs
   :language: haskell

To profile this program, we have to set some profiling compilation flags. 

.. code-block:: text

    $ ghc -O -prof -fprof-auto -rtsopts list.hs
    [1 of 1] Compiling Main             ( list.hs, list.o )
    Linking list ...

Then run the program using some profiling options. 

.. code-block:: text

    $ ./list +RTS -P
    1000000
    999999

A report is generated in a ``.prof`` file.

.. code-block:: text

    $ cat list.prof
        Wed Jan 15 19:09 2020 Time and Allocation Profiling Report  (Final)

           list +RTS -P -RTS

        total time  =        0.04 secs   (37 ticks @ 1000 us, 1 processor)
        total alloc = 128,051,152 bytes  (excludes profiling overheads)

    COST CENTRE  MODULE SRC              %time %alloc  ticks     bytes

    main.xs      Main   list.hs:3:9-25    51.4   56.2     19  72000016
    last_init_xs Main   list.hs:5:38-51   35.1   43.7     13  55999944
    last_xs      Main   list.hs:4:33-39   13.5    0.0      5        16

                                                                                  individual      inherited
    COST CENTRE     MODULE                SRC                  no.     entries  %time %alloc   %time %alloc  ticks     bytes

    MAIN            MAIN                  <built-in>           115          0    0.0    0.0   100.0  100.0      0       648
     CAF            Main                  <entire-module>      229          0    0.0    0.0   100.0  100.0      0        16
      main          Main                  list.hs:(2,1)-(5,51) 230          1    0.0    0.0   100.0  100.0      0       520
       last_init_xs Main                  list.hs:5:38-51      234          1   35.1   43.7    35.1   43.7     13  55999944
       last_xs      Main                  list.hs:4:33-39      232          1   13.5    0.0    13.5    0.0      5        16
       main.xs      Main                  list.hs:3:9-25       233          1   51.4   56.2    51.4   56.2     19  72000016
     CAF            GHC.Conc.Signal       <entire-module>      224          0    0.0    0.0     0.0    0.0      0       640
     CAF            GHC.IO.Encoding       <entire-module>      214          0    0.0    0.0     0.0    0.0      0      2768
     CAF            GHC.IO.Encoding.Iconv <entire-module>      212          0    0.0    0.0     0.0    0.0      0       200
     CAF            GHC.IO.Handle.FD      <entire-module>      204          0    0.0    0.0     0.0    0.0      0     34704
     CAF            GHC.IO.Handle.Text    <entire-module>      202          0    0.0    0.0     0.0    0.0      0        88
     main           Main                  list.hs:(2,1)-(5,51) 231          0    0.0    0.0     0.0    0.0      0     11592



Memory usage (Ghc)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Let's say we want to check the memory usage of this code.

.. literalinclude:: ghc/fib.hs
   :caption: ghc/fib.hs
   :language: haskell


We just have to compile with some profiling options then run with some memory
usage options. This generates a report in a ``.hp`` file, that we can plot
using ``hp2ps``.

.. code-block:: text

    $ ghc -O -prof -fprof-auto -rtsopts fib.hs
    [1 of 1] Compiling Main             ( fib.hs, fib.o )
    Linking fib ...

    $ ./fib +RTS -h
    165580141

    $ hp2ps fib.hp


.. figure:: fib.png
    :width: 90%

    Memory usage of the ``fibo.hs`` program.


Microbenchmarks (Criterion)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Criterion is a Haskell library that can benchmark functions.  We just have to
write a program that defines and runs some benchmark groups.

.. literalinclude:: criterion/fibo.hs
   :caption: criterion/fibo.hs
   :language: haskell

We then compile and run the program.

.. code-block:: text

	$ ghc -O fibo.hs

	$ ./fibo --output fibo.html

This produces a report:

.. figure:: fibo.png
    :width: 100%

    Benchmarks report.


