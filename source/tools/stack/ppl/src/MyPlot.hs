{-# language OverloadedStrings #-}

module MyPlot where

import Data.Text (Text)

import qualified Graphics.Vega.VegaLite as VL

barPlot :: Text -> VL.VLSpec
barPlot xName = 
    let encoding = VL.encoding
            . VL.position VL.X [VL.PName xName, VL.PmType VL.Nominal, VL.PAxis [VL.AxGrid True, VL.AxTitle xName]]
            . VL.position VL.Y [VL.PName "binnedData", VL.PAggregate VL.Count, VL.PmType VL.Quantitative, VL.PAxis [VL.AxGrid False, VL.AxTitle "count"]]
    in VL.asSpec $ [VL.mark VL.Bar [VL.MOpacity 1.0, VL.MColor "#a3c6de"], encoding []] 

linePlot :: Text -> Text -> VL.VLSpec
linePlot xName yName = 
    let encoding = VL.encoding
            . VL.position VL.X [VL.PName xName, VL.PmType VL.Quantitative, VL.PAxis [VL.AxGrid True, VL.AxTitle xName]]
            . VL.position VL.Y [VL.PName yName, VL.PmType VL.Quantitative, VL.PAxis [VL.AxGrid False, VL.AxTitle yName]]
    in VL.asSpec $ [VL.mark VL.Line [VL.MColor "green"], encoding []]

density2DPlot :: Text -> Text -> (Double, Double) -> (Double, Double) -> VL.VLSpec
density2DPlot xName yName (xmin, xmax) (ymin, ymax) = 
    let encoding = VL.encoding
            . VL.position VL.X [VL.PName xName, VL.PBin [VL.MaxBins 30, VL.Extent xmin xmax], VL.PmType VL.Quantitative, VL.PAxis [VL.AxGrid True, VL.AxTitle xName]]
            . VL.position VL.Y [VL.PName yName, VL.PBin [VL.MaxBins 30, VL.Extent ymin ymax], VL.PmType VL.Quantitative, VL.PAxis [VL.AxGrid True, VL.AxTitle yName]]
            . VL.color [ VL.MAggregate VL.Count, VL.MName "col", VL.MmType VL.Quantitative, VL.MScale [VL.SScheme "blues" [0.0, 1.0]]]
    in VL.asSpec $ [VL.mark VL.Rect [], encoding []]
    
plot :: (Double, Double) -> [VL.VLSpec] -> [(Text, VL.DataValues)] -> VL.VegaLite
plot (figw,figh) layers samples =
    let description = VL.description "Plot"
        dataColumns = map (\(x,y) -> VL.dataColumn x y) samples
        dat =  foldl (.) (VL.dataFromColumns []) dataColumns
        configure = VL.configure
            . VL.configuration (VL.Axis
                                        [ VL.Domain False,
                                            VL.Grid True,
                                            VL.GridColor "#FFFFFF",
                                            VL.GridOpacity 1,
                                            VL.LabelColor "#7F7F7F",
                                            VL.LabelPadding 4,
                                            VL.TickColor "#7F7F7F",
                                            VL.TickSize 5.67 ])
            . VL.configuration (VL.SelectionStyle [ ( VL.Single, [ VL.On "dblclick" ] ) ])
            . VL.configuration (VL.View [VL.ViewStroke (Just "transparent")])
    in VL.toVegaLite [VL.width figw, VL.background "#f9f9f9", VL.height figh, configure [], description, dat [], VL.layer layers]

