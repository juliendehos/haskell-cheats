
.. _tools-stack:

Stack
===============================================================================

:Date:
    2019-08-02

:Examples:
   `tools/stack <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/stack>`__

Stack is an alternative 
`tool for building Haskell projects <https://docs.haskellstack.org/en/stable/README/>`_. 
It can complement or replace Cabal. Stack also provides 
`curated snapshots of hackage packages <https://www.stackage.org/>`_

Stack + cabal
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- if the project already has a cabal config file, e.g.:

.. literalinclude:: mymath/mymath.cabal
    :caption: mymath/mymath.cabal
    :language: haskell

- then we just need to specify the stackage snapshot in a ``stack.yaml`` file:

.. literalinclude:: mymath/stack.yaml
    :caption: mymath/stack.yaml
    :language: yaml

- we can also customize haskell packages, nix packages, etc:

.. literalinclude:: ppl/stack.yaml
    :caption: ppl/stack.yaml
    :language: yaml


Basic commands
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- build a stack project:

.. code-block:: text

   $ stack build

   mymath-0.1: unregistering (local file changes: app/mysqrt1764/Main.hs mymath.cabal src/Mymath.hs)
   Building all executables for `mymath' once. After a successful build of all of them, only specified executables will be rebuilt.
   mymath-0.1: configure (lib + exe)
   Configuring mymath-0.1...
   mymath-0.1: build (lib + exe)
   Preprocessing library for mymath-0.1..
   Building library for mymath-0.1..
   [1 of 1] Compiling Mymath           ( src/Mymath.hs, .stack-work/dist/x86_64-linux-nix/Cabal-2.4.0.1/build/Mymath.o )
   Preprocessing executable 'mysqrt1764' for mymath-0.1..
   Building executable 'mysqrt1764' for mymath-0.1..
   [1 of 1] Compiling Main             ( app/mysqrt1764/Main.hs, .stack-work/dist/x86_64-linux-nix/Cabal-2.4.0.1/build/mysqrt1764/mysqrt1764-tmp/Main.o )
   Linking .stack-work/dist/x86_64-linux-nix/Cabal-2.4.0.1/build/mysqrt1764/mysqrt1764 ...
   mymath-0.1: copy/register
   Installing library in /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/.stack-work/install/x86_64-linux-nix/lts-13.19/8.6.4/lib/x86_64-linux-ghc-8.6.4/mymath-0.1-FEsGo98Obw9HavyNfQXs3d
   Installing executable mysqrt1764 in /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/.stack-work/install/x86_64-linux-nix/lts-13.19/8.6.4/bin
   Registering library for mymath-0.1..


- run an executable:

.. code-block:: text

   $ stack build --exec mysqrt1764

   42.0


- automatic rebuild:

.. code-block:: text

   $ stack build --file-watch 

   ExitSuccess
   Type help for available commands. Press enter to force a rebuild.

   mymath-0.1: unregistering (local file changes: app/mysqrt1764/Main.hs)
   mymath-0.1: build (lib + exe)
   Preprocessing library for mymath-0.1..
   Building library for mymath-0.1..
   Preprocessing executable 'mysqrt1764' for mymath-0.1..
   Building executable 'mysqrt1764' for mymath-0.1..
   [1 of 1] Compiling Main             ( app/mysqrt1764/Main.hs, .stack-work/dist/x86_64-linux-nix/Cabal-2.4.0.1/build/mysqrt1764/mysqrt1764-tmp/Main.o )
   Linking .stack-work/dist/x86_64-linux-nix/Cabal-2.4.0.1/build/mysqrt1764/mysqrt1764 ...
   mymath-0.1: copy/register
   Installing library in /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/.stack-work/install/x86_64-linux-nix/lts-13.19/8.6.4/lib/x86_64-linux-ghc-8.6.4/mymath-0.1-FEsGo98Obw9HavyNfQXs3d
   Installing executable mysqrt1764 in /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/.stack-work/install/x86_64-linux-nix/lts-13.19/8.6.4/bin
   Registering library for mymath-0.1..
   ExitSuccess
   Type help for available commands. Press enter to force a rebuild.


- interactive session:

.. code-block:: text

   $ stack repl 

   Using main module: 1. Package `mymath' component exe:mysqrt1764 with main-is file: /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/app/mysqrt1764/Main.hs
   The following GHC options are incompatible with GHCi and have not been passed to it: -O2
   Configuring GHCi with the following packages: mymath
   GHCi, version 8.6.4: http://www.haskell.org/ghc/  :? for help
   [1 of 2] Compiling Mymath           ( /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/src/Mymath.hs, interpreted )
   [2 of 2] Compiling Main             ( /home/julien/depots/gitlab/juliendehos/useful-haskell/source/tools/stack/mymath/app/mysqrt1764/Main.hs, interpreted )
   Ok, two modules loaded.
   Loaded GHCi configuration from /run/user/1000/haskell-stack-ghci/c315aa9b/ghci-script
   *Main Mymath> mysqrt 16
   4.0
   *Main Mymath> 


Stack + hpack
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- if the project has no cabal file, we can write a ``package.yaml`` file:

.. literalinclude:: mymath-hpack/package.yaml
    :caption: mymath-hpack/package.yaml
    :language: yaml

Then stack will generate a cabal file automatically.


Other features
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Stack can also:
   - build and run tests (see :ref:`tools-testing`)
   - generate documentation (see :ref:`tools-doc`)
   - ...

