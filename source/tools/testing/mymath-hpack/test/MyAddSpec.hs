module MyAddSpec (main, spec) where

import Test.Hspec
import MyAdd

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "myadd42" $ do
        it "myadd42 20" $ myadd42 20 `shouldBe` (62::Int)
        it "myadd42 0" $ myadd42 0 == (42::Int)
