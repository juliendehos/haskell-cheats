module MySqrtSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck
import MySqrt

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "mysqrt" $ do

    it "mysqrt (n**2) == |n|" $ property $
        \n -> almostEq (mysqrt (n**2)) (abs n) 1e-6

    it "(mysqrt n)**2 == n " $ property $
        \(Positive n) -> almostEq (mysqrt n ** 2) n 1e-6

almostEq :: Double -> Double -> Double -> Bool
almostEq x y e = abs (x - y) < e

