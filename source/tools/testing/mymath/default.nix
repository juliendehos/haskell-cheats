let
  pkgs = import <nixpkgs> {};
  drv = pkgs.haskellPackages.callCabal2nix "mymath" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
