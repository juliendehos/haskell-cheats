
.. _tools-testing:

Testing
===============================================================================

:Date:
    2019-08-09

:Examples:
   `tools/testing <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/testing>`__


Haskell has many 
`tools for automatic testing <http://hackage.haskell.org/package/tasty>`_: 
unit testing, property-based testing, testing frameworks...
Here, we briefly introduce Hspec and QuickCheck. 


Hspec
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`Hspec <https://hspec.github.io/>`_ is a testing framework for defining,
discovering and running tests (unit tests and property-based tests). 

Usually, we write our modules in a `src` folder and the corresponding tests in
a `test` folder.

.. code-block:: text

   mymath
   ├── src
   │   ├── MyAdd.hs
   │   └── MySqrt.hs
   ├── test
   │   ├── MyAddSpec.hs
   │   ├── MySqrtSpec.hs
   │   └── Spec.hs
   ...

For enabling the automatic discovery of test files, we add a `Spec.hs` file
containing:

.. literalinclude:: mymath/test/Spec.hs
   :caption: mymath/test/Spec.hs
   :language: haskell

A test file contains the tests to run for the corresponding module.

.. literalinclude:: mymath/test/MyAddSpec.hs
   :caption: mymath/test/MyAddSpec.hs
   :language: haskell


Cabal
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

For running the tests with Cabal, we just have to add a `test-suite` target in
the cabal file.

.. literalinclude:: mymath/mymath.cabal
   :caption: mymath/mymath.cabal
   :language: haskell

We can enable tests and test coverage, then run the tests.

.. code-block:: text

   $ cabal configure --enable-tests --enable-coverage

   $ cabal test

   Running 1 test suites...
   Test suite spec: RUNNING...
   Test suite spec: PASS
   Test suite logged to: dist/test/mymath-0.1-spec.log

   ...


Stack
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Stack can also run the tests. With Hpack, we have to add a `tests` target in
the `package.yaml` file:

.. literalinclude:: mymath-hpack/package.yaml
   :caption: mymath-hpack/package.yaml
   :language: yaml

To run the tests:

.. code-block:: text

   $ stack test --coverage

   mymath-0.1: test (suite: mymath-test)

   Progress 1/2: mymath-0.1
   MyAdd
     myadd42
       myadd42 20
       myadd42 0
   MySqrt
     mysqrt
       mysqrt (x**2) == |x|
         +++ OK, passed 100 tests.
       (mysqrt x)**2 == x
         +++ OK, passed 100 tests.

   Finished in 0.0024 seconds
   4 examples, 0 failures

   mymath-0.1: Test suite mymath-test passed
   Completed 2 action(s).

   ...



Property-based testing
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`QuickCheck <http://hackage.haskell.org/package/QuickCheck>`_ is a very classic
tool for property-based testing with Haskell (other tools also exist, such as
Smartcheck, Hedgehog...).

Property-based testing is a powerfull tool. It consists in writing properties
that the function to test should always satisfy. QuickCheck then runs the
function on random inputs and checks that the properties are satisfied on the
outputs. 

There is a lot more to say about PBT (random generator customization, 
error case shrinking...), but here we just give a brief example:

.. literalinclude:: mymath/test/MySqrtSpec.hs
   :caption: mymath/test/MySqrtSpec.hs
   :language: haskell

