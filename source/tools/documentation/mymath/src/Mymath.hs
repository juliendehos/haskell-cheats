
-- |
-- Module      : Mymath
-- Copyright   : (c) 2019 Julien Dehos
-- License     : MIT
-- Maintainer  : dehos@univ-littoral.fr
-- Stability   : experimental
-- Portability : GHC
--
-- A module containing custom math functions:
--
-- * mysqrt
--
-- * add42
--
-- > >>> import qualified Mymath as M
-- > >>> M.add42 20
-- > 62

module Mymath where

-- | Square root of a `Double` x.
mysqrt 
    :: Double -- ^ x
    -> Double -- ^ the square root of x 
mysqrt = sqrt

-- | Add 42 to a number x.
add42 :: Num a => a -> a
add42 = (+42)

