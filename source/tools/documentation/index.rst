
.. _tools-doc:

Documentation
===============================================================================

:Date:
    2019-08-08

:Examples:
   `tools/documentation <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/documentation>`__


Haskell has a tool, `Haddock <https://www.haskell.org/haddock/>`_, for
generating documentation. Haddock is similar to Javadoc, Doxygen, etc: we first
write specially-formatted comments in the source code then we run the haddock
program to build the documentation in HTML, latex, etc (see the `haddock
documentation <https://haskell-haddock.readthedocs.io/en/latest/>`_)


Writing documentation
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haddock understands source code comments. We can use ``-- |`` to document
following code and ``-- ^`` to document previous code. We can use use
markdown-like syntax for special formatting.

For example, the following code

.. literalinclude:: mymath/src/Mymath.hs
    :caption: mymath/src/Mymath.hs
    :language: haskell

gives the following HTML documentation

.. figure:: mymath.png
   :width: 80%


Generating documentation with Haddock
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can call the haddock program directly. 

.. code-block:: text

   haddock --html -o doc src/Mymath.hs 

Here, the main documentation file is ``doc/index.html``


Generating documentation with Cabal
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can call haddock using cabal.

.. code-block:: text

   cabal haddock

Here, the main documentation file is ``dist/doc/html/mymath/index.html``


Generating documentation with Stack
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

We can call haddock using stack.

.. code-block:: text

   stack haddock

Here, the main documentation file is 
``.stack-work/install/x86_64-linux-nix/lts-13.19/8.6.4/doc/mymath-0.1/index.html``


Literate programming
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell natively supports literate programming, see the
`literate programming wiki page <https://wiki.haskell.org/Literate_programming>`_.

