module Module1 where

welcome name = "Welcome " ++ name ++ "!"

welcome2 name = let w = welcome name in w ++ "\n" ++ w