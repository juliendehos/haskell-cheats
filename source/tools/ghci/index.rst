
Ghci
===============================================================================

:Date:
    2019-03-11

:Examples:
   `tools/ghci <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/tools/ghci>`__

Ghci is an interactive environment provided with Ghc. It is mainly a
REPL, that can Read a Haskell expression, Evaluate it, Print the result
and Loop for another expression. Ghci can also import modules and files,
inspect code, debug...

See also: `Ghci User's Guide
<https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/ghci.html>`__



REPL
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To launch Ghci, we run the ``ghci`` command. Then, every Haskell
expressions we enter are evaluated and the resulting value (if any) is printed.
To quit, we can enter ``:quit`` or hit C-d.

.. code-block:: text

    $ ghci
    GHCi, version 8.4.4: http://www.haskell.org/ghc/  :? for help

    Prelude> 21*2
    42

    Prelude> mul2 = (*2)

    Prelude> n = mul2 4

    Prelude> n
    8

    :quit

In Ghci, we can redefine a variable (this is impossible in a Haskell source
file). Previous expressions are not affected by the redefinition.

.. code-block:: text

    Prelude> x = 2      -- defines x

    Prelude> y = x      -- defines y, using x

    Prelude> x = 3      -- redefines x

    Prelude> y          -- y is not affected by the redefinition of x
    2


Ghci automatically binds the last resulting value to the ``it`` variable.

.. code-block:: text

    Prelude> 2+3
    5

    Prelude> it*2
    10

Ghci can read multi-line expressions, between ``:}`` and ``:}``.

.. code-block:: text

    Prelude> :{
    Prelude| do
    Prelude|     print 13
    Prelude|     print 37
    Prelude| :}
    13
    37

Inspect code
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ghci can show the type of an expression, using the ``:type`` command (or ``:t``).

.. code-block:: text

    Prelude> :t head
    head :: [a] -> a

The ``:info`` command (or ``:i``) displays information about a name.

.. code-block:: text

    Prelude> :i head
    head :: [a] -> a 	-- Defined in ‘GHC.List’

``:info`` works on types,

.. code-block:: text

    Prelude> :i Int
    data Int = GHC.Types.I# GHC.Prim.Int# 	-- Defined in ‘GHC.Types’
    instance Eq Int -- Defined in ‘GHC.Classes’
    instance Ord Int -- Defined in ‘GHC.Classes’
    instance Show Int -- Defined in ‘GHC.Show’
    instance Read Int -- Defined in ‘GHC.Read’
    instance Enum Int -- Defined in ‘GHC.Enum’
    instance Num Int -- Defined in ‘GHC.Num’
    instance Real Int -- Defined in ‘GHC.Real’
    instance Bounded Int -- Defined in ‘GHC.Enum’
    instance Integral Int -- Defined in ‘GHC.Real’


and on type-classes.

.. code-block:: text

    Prelude> :i Num
    class Num a where
      (+) :: a -> a -> a
      (-) :: a -> a -> a
      (*) :: a -> a -> a
      negate :: a -> a
      abs :: a -> a
      signum :: a -> a
      fromInteger :: Integer -> a
      {-# MINIMAL (+), (*), abs, signum, fromInteger, (negate | (-)) #-}
      	-- Defined in ‘GHC.Num’
    instance Num Word -- Defined in ‘GHC.Num’
    instance Num Integer -- Defined in ‘GHC.Num’
    instance Num Int -- Defined in ‘GHC.Num’
    instance Num Float -- Defined in ‘GHC.Float’
    instance Num Double -- Defined in ‘GHC.Float’


Import modules and files
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ghci can import Haskell modules.

.. code-block:: text

    Prelude> import Data.List

    Prelude Data.List> isInfixOf "come" "welcome"
    True


The ``:load`` command (or ``:l``) can load local source files (use
``:reload`` or ``:r`` to update).

.. literalinclude:: Module1.hs
    :caption: Module1.hs
    :language: haskell

.. code-block:: text

    Prelude> :l Module1.hs 
    [1 of 1] Compiling Module1          ( Module1.hs, interpreted )
    Ok, one module loaded.

    *Module1> welcome "toto"
    "Welcome toto!"


Debugger
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ghci includes a simple debugger. First, we load the file to debug and set breakpoints.

.. code-block:: text

    Prelude> :load Module1.hs 
    [1 of 1] Compiling Module1          ( Module1.hs, interpreted )
    Ok, one module loaded.

    *Module1> :break welcome
    Breakpoint 0 activated at Module1.hs:3:16-40

    *Module1> :break welcome2
    Breakpoint 1 activated at Module1.hs:5:41-54

    *Module1> :show breaks 
    [0] Module1 Module1.hs:3:16-40
    [1] Module1 Module1.hs:5:41-54

    *Module1> :delete 1

    *Module1> :show breaks 
    [0] Module1 Module1.hs:3:16-40


We can evaluate an expression until a breakpoint is reached, then inspect
values, source code...

.. code-block:: text

    *Module1> putStrLn $ welcome2 "toto"
    Stopped in Module1.welcome, Module1.hs:3:16-40
    _result :: [Char] = _
    name :: [Char] = _

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :force name
    name = "toto"

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :list
    2  
    3  welcome name = "Welcome " ++ name ++ "!"
    4  

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :continue
    Welcome toto!
    Welcome toto!


Unfortunately, Ghci has no "stack trace". But it has a tracing feature.

.. code-block:: text

    *Module1> :trace putStrLn $ welcome2 "toto"
    Stopped in Module1.welcome, Module1.hs:3:16-40
    _result :: [Char] = _
    name :: [Char] = _

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :history 
    -1  : welcome2:w (Module1.hs:5:25-36)
    -2  : welcome2 (Module1.hs:5:41-54)
    <end of history>

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :list
    2  
    3  welcome name = "Welcome " ++ name ++ "!"
    4  

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :back
    Logged breakpoint at Module1.hs:5:25-36
    _result :: [Char]
    name :: [Char]

    [-1: Module1.hs:5:25-36] [-1: Module1.hs:5:25-36] *Module1> :list
    4  
    5  welcome2 name = let w = welcome name in w ++ "\n" ++ w

    [-1: Module1.hs:5:25-36] [-1: Module1.hs:5:25-36] *Module1> :forward 
    Stopped at Module1.hs:3:16-40
    _result :: [Char]
    name :: [Char]

    [Module1.hs:3:16-40] [Module1.hs:3:16-40] *Module1> :list 
    2  
    3  welcome name = "Welcome " ++ name ++ "!"
    4  

