import Data.Vector as V

createTab3 :: Vector Int
createTab3 = V.replicate 3 0

incMid3 :: Vector Int -> Vector Int
incMid3 t = 
    let m = t ! 1
    in t // [(1, m+1)]

main :: IO ()
main = print $ incMid3 createTab3

