import Data.Array as A

createTab3 :: Array Int Int
createTab3 = listArray (0,2) (repeat 0)

incMid3 :: Array Int Int -> Array Int Int
incMid3 t = 
    let m = t ! 1
    in t // [(1, m+1)]

main :: IO ()
main = print $ incMid3 createTab3

