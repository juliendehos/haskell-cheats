import Data.Array as A

type Array22 = Array (Int,Int) Int

createTab22 :: Array22
createTab22 = listArray ((0,0), (1,1)) (repeat 0)

setDiag22 :: Array22 -> Array22
setDiag22 t = t // [((0,0),1), ((1,1),1)]

main :: IO ()
main = print $ setDiag22 createTab22

