createTab3 :: [Int]
createTab3 = replicate 3 0

incMid3 :: [Int] -> [Int]
incMid3 t = 
    let (t0, m:t1) = splitAt 1 t
    in t0 ++ [m+1] ++ t1

main :: IO ()
main = print $ incMid3 createTab3

