
Vector, Array
===============================================================================

:Date:
    2020-05-13

:Examples:
   `computing/vector <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/computing/vector>`__


List
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Lists are very common in Haskell. The `Data.List
<https://hackage.haskell.org/package/base/docs/Data-List.html>`_ module
provides many functions for handling lists. However, lists are not designed for
high-performance computating.

.. literalinclude:: examples/list1.hs
   :caption: examples/list1.hs
   :language: haskell

.. code-block::

    [0,1,0]
    

Vector
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The `vector <https://hackage.haskell.org/package/vector>`_ library implements
effient 1D arrays.


.. literalinclude:: examples/vector1.hs
   :caption: examples/vector1.hs
   :language: haskell

.. code-block::

    [0,1,0]


Array
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The `array <https://hackage.haskell.org/package/array>`_ library implements
multi-dimensional arrays.


.. literalinclude:: examples/array1.hs
   :caption: examples/array1.hs
   :language: haskell

.. code-block::

    array (0,2) [(0,0),(1,1),(2,0)]

With 2D arrays:

.. literalinclude:: examples/array2.hs
   :caption: examples/array2.hs
   :language: haskell

.. code-block::

    array ((0,0),(1,1)) [((0,0),1),((0,1),0),((1,0),0),((1,1),1)]


