import System.Random

data Color = Red | White | Blue deriving (Bounded, Enum, Show)

instance Random Color where

    randomR (a, b) g =
        let (x, g') = randomR (fromEnum a, fromEnum b) g
        in (toEnum x, g')

    random g = randomR (minBound, maxBound) g

main :: IO ()
main = do
    g <- newStdGen
    print $ take 5 (randoms g :: [Color])

