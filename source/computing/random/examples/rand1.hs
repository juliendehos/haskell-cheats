import System.Random

main :: IO ()
main = do
    g0 <- getStdGen
    let (x1, g1) = randomR (1, 10) g0
    let (x2, _g2) = randomR (1, 10) g1
    print (x1::Int)
    print (x2::Int)

