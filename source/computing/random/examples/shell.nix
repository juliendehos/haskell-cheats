with import <nixpkgs> {};

mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      mersenne-random-pure64
      random
      random-fu
    ]))
  ];
}

