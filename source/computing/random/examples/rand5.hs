import Control.Monad.State 
import Data.Random
import Data.RVar
import System.Random.Mersenne.Pure64

myRandVar :: State PureMT Int
myRandVar = sampleRVar (uniform 1 10)

myCompute :: State PureMT Int
myCompute = (*2) <$> myRandVar

main :: IO ()
main = do
    g <- newPureMT
    print $ evalState (replicateM 5 myRandVar) g
    print $ evalState (replicateM 5 myCompute) g

