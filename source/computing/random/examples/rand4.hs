import System.Random
import Control.Monad.State

randMul2 :: State StdGen Int
randMul2 = do
    g <- get
    let (x, g') = randomR (1, 10) g
    put g'
    return x

randMul2' :: State StdGen Int
randMul2' = (*2) <$> state (randomR (1, 10))

main :: IO ()
main = do
    g0 <- getStdGen
    print $ evalState (replicateM 5 randMul2) g0
    print $ evalState (replicateM 5 randMul2') g0

