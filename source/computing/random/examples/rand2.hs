import System.Random

main :: IO ()
main = do
    x1 <- randomRIO (1,10) 
    x2 <- randomRIO (1,10)
    print (x1::Int)
    print (x2::Int)

