
Random numbers
===============================================================================

:Date:
    2020-05-10

:Examples:
   `computing/random <https://gitlab.com/juliendehos/haskell-cheats/tree/master/source/computing/random>`__


Haskell has several libs for generating and handling pseudo-random numbers:
``random``, ``mersenne-random-pure64``, ``random-fu``...


System.Random
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The `random <https://hackage.haskell.org/package/random>`_ lib is very simple
to use. In a very classic way, we create a generator (or get the system
generator) and use it for generating pseudo-random numbers:

.. literalinclude:: examples/rand1.hs
   :caption: examples/rand1.hs
   :language: haskell

We can also generate a number using the system generator, directly, inside the
``IO`` monad: 

.. literalinclude:: examples/rand2.hs
   :caption: examples/rand2.hs
   :language: haskell

The ``random`` lib has other useful functions, for sampling an interval,
generating an infinite list of numbers... 


Sample a datatype
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A data type which instantiates ``Random`` can be sampled conveniently:

.. literalinclude:: examples/rand3.hs
   :caption: examples/rand3.hs
   :language: haskell

See also: `Random numbers in Haskell <https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/randoms>`_.


Random numbers and State monad
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

To handle the generator, it is quite common to use a `state monad
<https://hackage.haskell.org/package/mtl/docs/Control-Monad-State-Lazy.html>`_

.. literalinclude:: examples/rand4.hs
   :caption: examples/rand4.hs
   :language: haskell


Random variable, Mersenne-Twister 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Haskell has libraries implementing random variables (`rvar
<http://hackage.haskell.org/package/rvar>`_), random distributions (`random-fu
<http://hackage.haskell.org/package/random-fu>`_), the Mersenne-Twister
generator (`mersenne-random-pure64
<http://hackage.haskell.org/package/mersenne-random-pure64>`_)...

.. literalinclude:: examples/rand5.hs
   :caption: examples/rand5.hs
   :language: haskell


