let

  config = {
    allowBroken = true;
    packageOverrides = pkgs: {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: with pkgs.haskell.lib; {
          vinyl = dontCheck (dontHaddock (doJailbreak super.vinyl));
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in with pkgs; mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      Chart
      Chart-cairo
      Chart-diagrams
      diagrams-rasterific
      Frames
      lens
      foldl
      random
      statistics
      vinyl
    ]))
  ];
}

