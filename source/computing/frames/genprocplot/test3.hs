-- https://hackage.haskell.org/package/pipes-4.3.13/docs/Pipes-Tutorial.html

import Pipes
import qualified Pipes.Prelude as P

input :: Producer String IO ()
input = P.stdinLn >-> P.takeWhile (/= "quit")

name :: ListT IO String
name = do
    firstName <- Select input
    lastName  <- Select input
    return (firstName ++ " " ++ lastName)

main = runEffect $ every name >-> P.stdoutLn

