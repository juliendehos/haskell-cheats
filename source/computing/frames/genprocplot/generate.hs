import Control.Monad
import Data.List
import System.Random

noise :: Double
noise = 0.01

sinus :: Double -> Double -> Double
sinus x1 x2 = 
    let theta = - pi * 30 / 180
        t = x1 * cos theta - x2 * sin theta
        omega = 6
    in 0.5 * (1 + sin (omega*t))

gendata :: Int -> String -> IO ()
gendata n fp = do
    dataset <- forM [1..n] $ \_ -> do
        x1 <- randomRIO (-1, 1)
        x2 <- randomRIO (-1, 1)
        dy <- randomRIO (-noise, noise)
        return $ intercalate "," $ map show [x1, x2, dy + sinus x1 x2]
    writeFile fp $ unlines $ "x1,x2,y" : dataset

main :: IO ()
main = gendata 1000 "data.csv"

