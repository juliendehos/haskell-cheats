{-# OPTIONS_GHC -Wall #-}

import Pipes
import qualified Pipes.Prelude as P

triple :: Monad m => a -> Producer a m ()
triple x = do
    yield x
    yield x
    yield x

loop :: Producer String IO ()
loop = for P.stdinLn triple

main = runEffect $ for loop (lift . putStrLn)

