{-# LANGUAGE DataKinds, FlexibleContexts, QuasiQuotes, TemplateHaskell #-}

import qualified Control.Foldl as L
import Data.Vinyl (rcast)
import Data.Vinyl.Curry (runcurryX)
import Frames

tableTypes "Row" "cities.csv"

loadRows :: IO (Frame Row)
loadRows = inCoreAoS (readTable "cities.csv")

type City = (:->) "city" String
type Population = (:->) "population" Int

fmtRow :: Record '[City, Population] -> (String, Int)
fmtRow = runcurryX (\c p -> (c,p))

-- allRows :: IO [(String, Int)]
-- allRows = L.premap (fmtRow . rcast) <$> loadRows

main :: IO ()
main = do
    loadRows >>= mapM_ print
    -- allRows >>= mapM_ print

