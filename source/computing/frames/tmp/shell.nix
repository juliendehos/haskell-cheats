
let

  #text-src = fetchTarball {
  #  url = https://github.com/haskell/text/archive/1.2.3.0.tar.gz;
  #  sha256 = "0rh9mb023f0s56ylzxz9c3c1y09lpl6m69ap5bnpdi0dz7fm6s85";
  #};

  config = {
    allowBroken = true;

    packageOverrides = pkgs: {
      haskellPackages = pkgs.haskellPackages.override {
            overrides = self: super: with pkgs.haskell.lib; {

              vinyl = dontCheck (dontHaddock (doJailbreak super.vinyl));

              #network = dontCheck (dontHaddock (doJailbreak super.network));
              #random = self.callHackage "random" "1.1" {};
              #text = self.callCabal2nix "text" text-src {};

        };
      };
    };
  };

  #channel = fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz";

  pkgs = import <nixpkgs> { inherit config; };

in with pkgs; mkShell {
  buildInputs = [
    (haskellPackages.ghcWithPackages (ps: with ps; [
      Chart
      Chart-cairo
      Chart-diagrams
      diagrams-rasterific
      Frames
      lens
      foldl
      #Rasterific
      statistics
      vinyl
    ]))
  ];
}

