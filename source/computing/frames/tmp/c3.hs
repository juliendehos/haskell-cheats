{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wall #-}

import Frames
import Frames.TH (rowGen, RowGen(..))
import Pipes (Effect, Producer, (>->))

import qualified Pipes.Prelude as P

tableTypes' (rowGen "c1.csv") { rowTypeName = "Row" , separator = ";" }

rowStream :: MonadSafe m => Producer Row m ()
rowStream = readTableOpt rowParser "c1.csv"

getCities :: MonadSafe m => Effect m [(Text, Int)]
getCities = P.toListM $ rowStream
    >-> P.map (\r -> (rgetField @City r, rgetField @Population r))

main :: IO ()
main = runSafeEffect getCities >>= print

