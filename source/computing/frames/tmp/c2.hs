{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}
{-# OPTIONS_GHC -Wall #-}

import           Frames
import           Frames.CSV
import           Pipes         (Effect, Producer, (>->))
import qualified Pipes.Prelude as P

declareColumn "city" ''Text
declareColumn "population" ''Int

type Row = Record '["city" :-> Text, "population" :-> Int]

myParser :: ParserOptions
myParser = defaultParser { columnSeparator = ";" }

myStreamM :: MonadSafe m => Producer Row m ()
myStreamM = readTableOpt myParser "c1.csv"

getCities :: MonadSafe m => Effect m [Text]
getCities = P.toListM $ myStreamM >-> P.map (rgetField @City)

main :: IO ()
main = runSafeEffect getCities >>= print

