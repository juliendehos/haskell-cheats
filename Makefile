SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = "My Haskell Cheat Sheet"
SOURCEDIR     = source
BUILDDIR      = build

gzip: html
	find build/html \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.svg' \) -print0 | xargs -0 gzip -9 -k -f

serve: html
	python -m http.server -d build/html/

watch:
	sphinx-autobuild source build

pdf: latex
	make -C build/latex

clean:
	rm -rf $(BUILDDIR)

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: # Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

