with import <nixpkgs> {};

let

  mypython = (let

    port-for = python3.pkgs.buildPythonPackage rec {
      pname = "port-for";
      version = "0.3.1";
      src = python3.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "073gb767blwhdg0xw61rwckrr735gjqkigi99js4v5f256xq8smi";
      };
      doCheck = false;
    };

    livereload = python3.pkgs.buildPythonPackage rec {
      pname = "livereload";
      version = "2.6.1";
      src = python3.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "0rhggz185bxc3zjnfpmhcvibyzi86i624za1lfh7x7ajsxw4y9c9";
      };
      doCheck = false;
      propagatedBuildInputs = with python3Packages; [
        port-for
        six
        tornado
      ];
    };

    sphinx-autobuild = python3.pkgs.buildPythonPackage rec {
      pname = "sphinx-autobuild";
      version = "0.7.1";
      src = python3.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "0kn753dyh3b1s0h77lbk704niyqc7bamvq6v3s1f6rj6i20qyf36";
      };
      doCheck = false;
      propagatedBuildInputs = with python3Packages; [
        argh
        port-for
        livereload
        pyyaml
        watchdog
      ];
    };

  in python3.withPackages (ps: [
    ps.pygments
    ps.sphinx
    ps.sphinx_rtd_theme
    sphinx-autobuild
  ]));

in

pkgs.mkShell {

  buildInputs = [
    gnumake
    gzip
    mypython
    (texlive.combine {
      inherit (texlive)
      scheme-medium
      capt-of
      fncychap
      framed
      needspace
      tabulary
      titlesec
      upquote
      varwidth
      wrapfig
    ;})

  ];

  shellHook = ''
    unset SOURCE_DATE_EPOCH;
  '';

}

